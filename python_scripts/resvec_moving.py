'''
Copyright (C) 2019-2022 Eindhoven University of Technology.

This code is free software, you can redistribute it and/or modify it under
the terms of the GNU General Public License; either version 3.0 of the
License, or (at your option) any later version. See LICENSE for details.

This code is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
        The Plasimo Team (https://plasimo.phys.tue.nl)
'''
from scipy.io import mmread
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import pyplot
import matplotlib.colors as colors
from matplotlib import rc
#import matplotlib.ticker as tick
import matplotlib.ticker

font = {'family': 'normal',
        'weight': 'normal',
        'size': 15}
matplotlib.rc('font', **font)

'''
2D Plots of the residual
'''
folder="resvec_moving_nopc/"

'''
Load in the 2D plot data, number at the end is the number of iterations
'''
input_files_2D=[
"relres_moving_LMR_100",
"relres_moving_LMR_125",
"relres_moving_LMR_150",
"relres_moving_LMR_175",
"relres_moving_LMR_200",
"relres_moving_LMR_225",
"relres_moving_LMR_75",
"relres_moving_LMR_50",
"relres_moving_LMR_25",
"relres_moving_LMR_5",
"relres_moving_rubicgstab_100",
"relres_moving_rubicgstab_125",
"relres_moving_rubicgstab_150",
"relres_moving_rubicgstab_175",
"relres_moving_rubicgstab_200",
"relres_moving_rubicgstab_225",
"relres_moving_rubicgstab_75",
"relres_moving_rubicgstab_50",
"relres_moving_rubicgstab_25",
"relres_moving_rubicgstab_5",
]


class FormatScalarFormatter(matplotlib.ticker.ScalarFormatter):
    #https://stackoverflow.com/questions/45815396/how-to-change-the-the-number-of-digits-of-the-mantissa-using-offset-notation-in
    def __init__(self, fformat="%1.1f", offset=True, mathText=True):
        self.fformat = fformat
        matplotlib.ticker.ScalarFormatter.__init__(self,useOffset=offset,
                                                        useMathText=mathText)
    def _set_format(self):
        self.format = self.fformat
        if self._useMathText:
            self.format = '$%s$' % matplotlib.ticker._mathdefault(self.format)

X=mmread(folder+"relres_moving_X.mtx")
Y=mmread(folder+"relres_moving_Y.mtx")
for input_file in input_files_2D:
    Z=mmread(folder+input_file+".mtx")

    #Set up the plot
    f = plt.figure()
    ax = plt.gca()

    #pcm = plt.contourf(X, Y, Z, cmap='jet',extend='both',levels=100)
    #pcm = plt.contourf(X, Y,np.log10(np.abs(Z)+1e-16), cmap='jet',extend='both',levels=100,vmin=-6,vmax=0)
    #pcm = plt.contourf(X, Y,np.log10(np.abs(Z)), cmap='jet',extend='both',levels=np.linspace(-6,0,100),vmin=-6,vmax=0)
    pcm = plt.contourf(X, Y,np.abs(Z), cmap='jet',extend='both',levels=np.logspace(-6,0,100),norm=matplotlib.colors.LogNorm())
    #norm=matplotlib.colors.Normalize(vmin=-0.01, vmax=0.01)
    #fmt = FormatScalarFormatter("%.1f")
    #cbar = f.colorbar(pcm,extend='both',format=fmt)
    cbar = f.colorbar(pcm,extend='both',ticks=[1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,1e0])
    cbar.ax.set_yticklabels(['$<10^{-6}$','$10^{-5}$','$10^{-4}$', '$10^{-3}$','$10^{-2}$','$10^{-1}$', '$>1$'])
    cbar.ax.set_ylabel('True relative residual', usetex=False)

    #cbar.ax.yaxis.set_major_formatter(tick.FormatStrFormatter('%.2f'))

    #https://stackoverflow.com/questions/25983218/scientific-notation-colorbar-in-matplotlib
    #cbar.formatter.set_powerlimits((0, 0))
    #https://stackoverflow.com/questions/43324152/python-matplotlib-colorbar-scientific-notation-base
    #cbar.update_ticks()
    #cbar.set_clim(-2.0, 2.0)
    #plt.clim(-2.0,2.0)

    plt.xlabel("x", usetex=False)
    plt.ylabel("y", usetex=False)
    #f.savefig(filename+".pdf", bbox_inches='tight')
    f.savefig(folder+input_file+".png", bbox_inches='tight')
    plt.close()

'''
1D plots
'''
params = {'text.latex.preamble': [r'\usepackage{amsmath}']}
pyplot.rcParams.update(params)
r_LMR=np.array(mmread(folder+"resvec_moving_LMR.mtx")[0])
r_RuBiCGStab=np.array(mmread(folder+"resvec_moving_rubicgstab.mtx")[0])

#Set up the plot
f = plt.figure()
ax = plt.gca()

plt.plot(r_LMR,label="$\|\|\\bf{r}_{\mathrm{LMR}}\|\|/\|\|\\bf{b}\|\|$")
#Note: RuBiCGStab does 2MV/iteration, resvec gets 1 extra element per MV.
plt.plot(r_RuBiCGStab[::2],label="$\|\|\\bf{r}_{\mathrm{BiCGStab}}\|\|/\|\|\\bf{b}\|\|$")
plt.xlabel('Iterations')
plt.ylabel('$\|\|\\bf{r}\|\|/\|\|\\bf{b}\|\|$')
plt.yscale('log')
plt.autoscale(enable=True, axis='x', tight=True)
ax.legend(fontsize='small',frameon=False)
f.savefig(folder+"lmr_rubicgstab_residuals"+".pdf", bbox_inches='tight')
plt.close()
