% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clc
clear all
%close all
fig_no=0;
%{
Observations:

%}
rng(0,'twister')

axis_min=1e-50
axis_max=1

M=100;
lambda=1
A=lambda*eye(M)+lambda*diag(-ones(M-1,1),-1);
%A=diag(rand(M,1),0)+diag(-ones(M-1,1),-1);

% V=rand(M,M);
% L=diag(10.^((rand(M,1)-0.5)*10));
% A=V*L/V;

x=zeros(M,1);
b=zeros(M,1);
b(1)=1;

% problem=Input1D;
% problem.phi_w=1;
% problem.phi_e=0;
% problem.x_w=0;
% problem.x_e=1;
% problem.N_x=M;
%
% dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
% e=1;
% Pe=1e5;
% Da=1e-5;
%
% L=1;
% u=Pe/L;
% sl=-Da * (u + e / L) / L;
%
% %problem.flux_fn=@(u,e,dx) flux.CD(u,e,dx);
% problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);
% problem.u_fn=@(x) u;
% problem.e_fn=@(x) e;
% problem.s_fn=@(x) 1;
% problem.sl_fn=@(x) sl;
%
% [A,b]=problem.discretize();

x=zeros(size(b));

%Compute with LMR to check that indeed all omega=0.5 for the bidiagonal
%problem
[x_lmr,omegas_lmr,r_lmr,rs]=lmr_fn(A,b,x,1.0*M+1);
%{
Note for maxiter>M, we can no longer guarantee that omega=0.5.
%}

fig_no=fig_no+1;
figure(fig_no)
%contourf(rs,10)
imagesc(abs(rs))
colorbar
colormap jet
ylabel('r-index []')
xlabel('Iteration []')
title('LMR residual')
set(gca,'ColorScale','log')
caxis([axis_min,axis_max])
mmwrite('r_lmr.mtx',rs)

tspan=[0;cumsum(omegas_lmr)];
mmwrite('tspan.mtx',tspan)
try
%Compute an accurate solution to the ODE using ode45
y0=b;
[t_ode45,r_ode45]=ode45(@(t,y) -A*y,tspan,y0);
%ODE45's timesteps are appending rows
r_ode45=transpose(r_ode45);
fig_no=fig_no+1;
figure(fig_no)
%contourf(r_ode45,10);
imagesc(abs(r_ode45));
colorbar
colormap jet
ylabel('r-index []')
xlabel('timestep []')
title('ODE45 solution')
set(gca,'ColorScale','log')
caxis([axis_min,axis_max])
catch
end

%Compute a less accurate solution to the ODE using forward euler
%with fixed stepsize dt=0.5
[r_euler]=forward_euler(A,y0,tspan);

fig_no=fig_no+1;
figure(fig_no)
%contourf(r_euler,10);
imagesc(abs(r_euler));
colorbar
colormap jet
ylabel('r-index []')
xlabel('timestep []')
title('Forward Euler solution')
set(gca,'ColorScale','log')
caxis([axis_min,axis_max])

%Compute the exact solution to the ODE
[r_exact]=exact_solve_ode(A,y0,tspan);
fig_no=fig_no+1;
figure(fig_no)
%contourf(r_exact,10);
imagesc(abs(r_exact));
colorbar
colormap jet
ylabel('r-index []')
xlabel('timestep []')
title('Exact solution')
set(gca,'ColorScale','log')
caxis([axis_min,axis_max])
mmwrite('r_exact.mtx',r_exact)

%Compute with bicgstab
[x_bicgstab,r_bicgstab,rs_bicgstab]=bicgstab_fn(A,b,x,M);

fig_no=fig_no+1;
figure(fig_no)
%contourf(rs,10)
imagesc(abs(rs_bicgstab))
colorbar
colormap jet
ylabel('r-index []')
xlabel('Iteration []')
title('BiCGStab residual')
set(gca,'ColorScale','log')
%caxis([max(rs_bicgstab),min(rs_bicgstab)])
caxis([axis_min,axis_max])
%rs_bicgstab

fig_no=fig_no+1;
figure(fig_no)
plot(tspan(1:end-1),diff(tspan))
xlabel('t')
ylabel('omega')
function [y]=exact_solve_ode(A,y0,tspan)
%{
Exact solution to y'=-A*y at times given in tspan
%}
    y=nan(length(y0),length(tspan));
    y(:,1)=y0;
    for ix_tspan=1:length(tspan)-1
        t=tspan(ix_tspan+1);
        y(:,ix_tspan+1)=(expm(-A*t))*y0;
    end
end

function [x,omegas,r,rs]=lmr_fn(A,b,x,maxit)
r=b-A*x;
omegas=[]';
rs=nan(length(b),maxit);
rs(:,1)=r;
iter=1;
while(iter<maxit)
    iter=iter+1;
    %nnz(r)
    Ar=A*r;
    omega=dot(Ar,r)/(Ar'*Ar);
    x=x+omega*r;
    r=r-omega*Ar;
    omegas(end+1)=omega;
    rs(:,iter)=r;
end
omegas=transpose(omegas);
end

function [y]=forward_euler(A,y0,tspan)
    %{
    Takes forward steps to approximate y'=-A*y
    %}
    y=nan(length(y0),length(tspan));
    y(:,1)=y0;
    for ix_tspan=1:length(tspan)-1
        dt=tspan(ix_tspan+1)-tspan(ix_tspan);
        y(:,ix_tspan+1)=-dt*A*y(:,ix_tspan)+y(:,ix_tspan);
    end

end

function [x,r,rs]=bicgstab_fn(A,b,x,maxit)
r=b-A*x;
omegas=[]';
rs=nan(length(b),maxit);
rs(:,1)=r;
iter=0;
while(iter<maxit)
    iter=iter+1;
    options.keep_best=false;
    options.random_rtilde=true;
    options.L=1;
    x=rubicgstabl(A,b,1e-12,iter,[],[],zeros(size(b)),options);
    %x=gmres(A,b,[],1e-12,iter,[],[],zeros(size(b)));
    r=b-A*x;
    rs(:,iter)=r;
end
end
