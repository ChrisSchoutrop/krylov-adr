% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function [x,flag,relres,iter,resvec] = rubicgstabl(A,b,tol,maxit,M1,M2,x0,options)
%{
RU-BiCGstab(L) Reliable Update - BiConjugate Gradients Stabilized Method (L
(L).
   x = rubicgstabl(A,b) attempts to solve the system of linear equations
   A*x=b for x. The N-by-N coefficient matrix A must be square and the
   right hand side column vector b must have length N.

   x = rubicgstabl(A,b,tol) specifies the tolerance of the method. If tol is
   [] then BICGSTAB uses the default, 1e-6.

   x = rubicgstabl(A,b,tol,maxit) specifies the maximum number of iterations.
   If maxit is [] then rubicgstabl uses the default, min(N,20).

   x = rubicgstabl(A,b,tol,maxit,M) and x = rubicgstabl(A,b,tol,maxit,M1,M2)
   use preconditioner M or M=M1*M2 and effectively solve the system
   A*inv(M)*x = b for x. If M is [] then a preconditioner is not applied.

   x = rubicgstabl(A,b,tol,maxit,M1,M2,x0) specifies the initial guess.  If
   x0 is [] then rubicgstabl uses the default, an all zero vector.

   x = rubicgstabl(A,b,tol,maxit,M1,M2,x0,options) add an additional struct
   containing options:
   'L', integer specifying the order of the stabilizing polynomial. By
   default L=2.
   'norm', defines the type of norm to use.
   'reliable_update', boolean defining whether or not to use the reliable
   updating strategy.
   'random_rtilde', boolean value, when set to false the BiCGStab default
   choice of rtilde=r0 will be used.
   'keep_best', boolean value, by default rubicgstabl stores the solution
   with best residual thus far.

   [x,flag] = rubicgstabl(A,b,...) also returns a convergence flag:
    0 rubicgstabl converged to the desired tolerance tol within maxit iterations.
    1 rubicgstabl iterated maxit times but did not converge.
    2 preconditioner M was ill-conditioned (Not implemented).
    3 rubicgstabl stagnated (two consecutive iterates were the same) (Not implemented).
    4 one of the scalar quantities calculated during rubicgstabl became
      too small or too large to continue computing.

   [x,flag,relres] = rubicgstabl(A,b,...) also returns the relative residual
   norm(b-A*x)/norm(b). If flag is 0, then relres <= tol.

   [x,flag,relres,iter] = rubicgstabl(A,b,...) also returns the iteration
   number at which x was computed: 0 <= iter <= maxit.

   [x,flag,relres,iter,resvec] = rubicgstabl(A,b,...) also returns a vector
   of the residual norms after each MV (Matrix-Vector product).
%}

warning('off','MATLAB:rankDeficientMatrix');

% Check for an acceptable number of input arguments
if nargin < 2
    error('NotEnoughInputs');
end

N=length(b);

% Assign default values to unspecified parameters
if nargin < 3 || isempty(tol)
    tol = 1e-6;
end
if tol < eps
    warning('tooSmallTolerance');
    tol = eps;
elseif tol >= 1
    warning('tooBigTolerance');
    tol = 1-eps;
end
if nargin < 4 || isempty(maxit)
    maxit = min(N,20);
end
maxit = max(maxit, 0);

%Check if a preconditioner was defined
if ((nargin >= 5) && ~isempty(M1))
    existM1 = 1;
    %Check if M1 is a function
    if isa(M1, 'function_handle')
        functionM1=true;
        m1fun=M1;
    else
        functionM1=false;
    end
else
    existM1 = 0;
end
if ((nargin >= 6) && ~isempty(M2))
    existM2 = 1;
else
    existM2 = 0;
end

%Check if an initial guess was given
if ((nargin >= 7) && ~isempty(x0))==false
    x0=zeros(size(b));
end

%Default options:
L=2;
use_norm=2;
use_reliable_update=true;
use_random_rtilde=true;
use_keep_best_solution=true;

%Check additional options are given as input
if (nargin >= 8)
    if isfield(options,'L')
        L=options.L;
    end
    if isfield(options,'norm')
        use_norm=options.norm;
    end
    if isfield(options,'reliable_update')
        use_reliable_update=options.reliable_update;
    end
    if isfield(options,'random_rtilde')
        use_random_rtilde=options.random_rtilde;
    end
    if isfield(options,'keep_best')
        use_keep_best_solution=options.keep_best;
    end
end

if L>N
    L=N;
elseif isempty(L)
    L=2;
end
k=0;

%Other vectors and scalars initialization
rho0 = 1.0;
alpha = 1.0;
omega = 1.0;
rHat=zeros(N, L + 1);
uHat=zeros(N, L + 1);

resvec=[];
flag=0;

rHat(:,1) = b-A*x0;
x=zeros(size(x0)); %This will contain the updates to x0.

%rShadow is arbritary, but must not be orthogonal to r0.
if use_random_rtilde
    rShadow=rand(size(rHat(:,1)));
else
    rShadow=rHat(:,1);
end
%Random avoids a lot of problems if rHat is very sparse for example.

x_prime = x;
x(:)=0;
b_prime = rHat(:,1);

bicg_convergence = false;

zeta0 = norm(b,use_norm);
%%zeta = zeta0;    %TODO: Should this be norm(r0,use_norm)?
zeta=norm(rHat(:,1),use_norm);

resvec(end+1)=zeta;

Mx = zeta;
Mr = zeta;

%Criterion for when to apply the group-wise update, conform ref 3.
delta = 0.01;
compute_res = false;
update_app = false;

%Keep track of the solution with the lowest residual
x_min=x_prime+x;
zeta_min=zeta;

while(zeta > tol * zeta0 && k < maxit)

    rho0 =rho0* -omega;
    for j = 1:L
        rho1=rShadow'*rHat(:,j);
        if isfinite(rho1)==false || rho0==0.0
            %Algorithm cannot continue after either of these two occurs
            %We cannot continue computing, return the best solution found.
            x=x+x_prime;
            if existM1 && existM2
                zeta = norm(b-A*((M2\(M1\x)+x0)),use_norm);
            elseif existM1
                if functionM1==false
                    zeta = norm(b-A*((M1\x)+x0),use_norm);
                else
                    zeta = norm(b-A*m1fun(x)+x0,use_norm);
                end
            elseif existM2
                zeta = norm(b-A*((M2\x)+x0),use_norm);
            end
            if(use_keep_best_solution && (zeta>zeta_min || ~isfinite(zeta)))
                %x_min is a better solution than x, return x_min
                x = x_min;
                zeta = zeta_min;
            end

            relres=zeta/zeta0;
            resvec(end+1)=zeta;
            flag=4;
            iter=k;

            %Convert back to the non-preconditioned x
            if existM1 && existM2
                x = (M2\(M1\x));
            elseif existM1
                if functionM1==false
                    x = (M1\x);
                else
                    x=m1fun(x);
                end
            elseif existM2
                x = (M2\x);
            end
            x=x+x0;

            if relres<tol
                flag=0;
            end

            return
        end
        beta = alpha * (rho1 / rho0);
        rho0 = rho1;
        % Update search directions
        uHat(:,1:j) = rHat(:,1:j) - beta * uHat(:,1:j);
        %uHat(:,j + 1) = mat * precond.solve(uHat.col(j));
        %uHat(:,j + 1) = A * uHat(:,j);
        if existM1 && existM2
            uHat(:,j + 1) = A*(M2\(M1\uHat(:,j)));
        elseif existM1
            if functionM1==false
                uHat(:,j + 1) = A*(M1\uHat(:,j));
            else
                uHat(:,j + 1)=A*m1fun(uHat(:,j));
            end
        elseif existM2
            uHat(:,j + 1) = A*(M2\uHat(:,j));
        else
            uHat(:,j + 1) = A*uHat(:,j);
        end
        resvec(end+1)=zeta;
        alpha = rho1 / (rShadow'*(uHat(:,j + 1)));
        % Update residuals
        rHat(:,1:j) =rHat(:,1:j)- alpha * uHat(:,2:(2+(j-1)));
        %rHat.col(j + 1) = mat * precond.solve(rHat.col(j));
        %rHat(:,j + 1) = A * rHat(:,j);
        if existM1 && existM2
            rHat(:,j + 1) = A*(M2\(M1\rHat(:,j)));
        elseif existM1
            if functionM1==false
                rHat(:,j + 1) = A*(M1\rHat(:,j));
            else
                rHat(:,j + 1) = A*m1fun(rHat(:,j));
            end
        elseif existM2
            rHat(:,j + 1) = A*(M2\rHat(:,j));
        else
            rHat(:,j + 1) = A * rHat(:,j);
        end

        %Complete BiCG iteration by updating x
        x =x+ alpha * uHat(:,1);

        %Check for early exit
        zeta = norm(rHat(:,1),use_norm);
        resvec(end+1)=zeta;
        if(zeta<tol*zeta0)
            %{
			Convergence was achieved during BiCG step.
			Without this check BiCGStab(L) fails for trivial matrices, such as when the preconditioner already is
			the inverse, or the input matrix is identity.
            %}
            bicg_convergence=true;
            break;
        elseif(use_keep_best_solution && zeta<zeta_min)
            %We found an x with lower residual, keep this one.
            x_min=x+x_prime;
            zeta_min=zeta;
        end
    end
    if(bicg_convergence==false)
        %{
		The polynomial/minimize residual step.

		QR Householder method for argmin is more stable than (modified) Gram-Schmidt, in the sense that there is
		less loss of orthogonality. It is more accurate than solving the normal equations, since the normal equations
		scale with condition number squared.
        %}
        gamma=rHat(:,(end-L+1):end)\rHat(:,1);
        %[Q,R,p] = qr(rHat(:,(end-L+1):end),0);
        %gamma(p,:) = R\(Q\rHat(:,1));
        x=x+rHat(:,1:L)*gamma;
        rHat(:,1)=rHat(:,1)-rHat(:,(end-L+1):end)*gamma;
        uHat(:,1)=uHat(:,1)-uHat(:,(end-L+1):end)*gamma;
        omega=gamma(L-1+1);
        zeta=norm(rHat(:,1),use_norm);
    end
    if(use_keep_best_solution && zeta<zeta_min)
        %We found an x with lower residual, keep this one.
        x_min=x+x_prime;
        zeta_min=zeta;
    end
    k=k+1;

    %{
	Reliable update part

	The recursively computed residual can deviate from the actual residual after several iterations. However,
	computing the residual from the definition costs extra MVs and should not be done at each iteration. The reliable
    update strategy computes the true residual from the definition: r=b-A*x at strategic intervals. Furthermore a
	"group wise update" strategy is used to combine updates, which improves accuracy.
    %}
    %Maximum norm of residuals since last update of x.
    Mx = max(Mx, zeta);
    %Maximum norm of residuals since last computation of the true residual.
    Mr = max(Mr,zeta);

    if (zeta < delta * zeta0 && zeta0 <= Mx)
        update_app = true;
    end

    if (update_app || (zeta < delta * Mr && zeta0 <= Mr))
        compute_res = true;
    end

    if (bicg_convergence)
        update_app = true;
        compute_res = true;
        bicg_convergence = false;
    end

    if (use_reliable_update && compute_res)
        %rHat.col(0) = b_prime - mat * x; //Fokkema paper pseudocode
        %Fokkema paper Fortan code L250-254
        %rHat.col(0) = b_prime - mat * precond.solve(x);
        %rHat(:,1) = b_prime - A * x;
        if existM1 && existM2
            rHat(:,1) = b_prime-A*(M2\(M1\x));
        elseif existM1
            if functionM1==false
                rHat(:,1) = b_prime-A*(M1\x);
            else
                rHat(:,1) = b_prime-A*m1fun(x);
            end
        elseif existM2
            rHat(:,1) = b_prime-A*(M2\x);
        else
            rHat(:,1) = b_prime - A * x;
        end
        zeta = norm(rHat(:,1),use_norm);

        resvec(end+1)=zeta;

        Mr = zeta;

        if (update_app)
            %After the group wise update, the original problem is translated
            %to a shifted one.
            x_prime =x_prime+ x;
            x(:)=0;
            b_prime = rHat(:,1);
            %zeta0=norm(b_prime);
            Mx = zeta;
        end
    end
    if (use_keep_best_solution && zeta<zeta_min)
        %We found an x with lower residual, keep this one.
        x_min=x+x_prime;
        zeta_min=zeta;
    end

    compute_res = false;
    update_app = false;

end

%Convert internal variable to the true solution vector x
x=x+x_prime;
if existM1 && existM2
    zeta = norm(b-A*((M2\(M1\x)+x0)),use_norm);
elseif existM1
    if functionM1==false
        zeta = norm(b-A*((M1\x)+x0),use_norm);
    else
        zeta = norm(b-A*(m1fun(x)+x0),use_norm);
    end
elseif existM2
    zeta = norm(b-A*((M2\x)+x0),use_norm);
end
if(use_keep_best_solution && (zeta>zeta_min || ~isfinite(zeta)))
    %x_min is a better solution than x, return x_min
    x = x_min;
    zeta = zeta_min;
end

resvec(end+1)=zeta;

if zeta0~=0
    relres = zeta / zeta0;
else
    relres=0;
end

iter = k;
if existM1 && existM2
    x = (M2\(M1\x));
elseif existM1
    if functionM1==false
        x = (M1\x);
    else
        x=m1fun(x);
    end
elseif existM2
    x = (M2\x);
else
    x = x;
end
x=x+x0;
if iter==maxit && relres>tol
    flag=1;
end


return;






end
















