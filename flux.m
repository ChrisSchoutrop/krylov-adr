% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef flux
    %{
        Numerical flux is given by
        F_{j+1/2}=
        \alpha_{j+1/2}\varphi_j
        +\beta_{j+1/2}\varphi_{j+1}
        +\Delta x(
            \gamma_{j+1/2}s_j
            +\delta_{j+1/2}s_{j+1}
            )

        Here the coefficients \alpha,\beta,\gamma,\delta are computed.
        Coefficients obtained out from ACFD4.pdf
        Note: every sign is a +!
    %}
    methods (Static)
        function out=UW(u,e,ds)
            %Upwind scheme
            out=zeros(4,1);

            %Advection term
            if (u > 0)
                out(1) = u;
            else
                out(2) = u;
            end

            %Diffusion term
            out(1) =out(1)+ e / ds;
            out(2) =out(2)- e / ds;
            return;
        end
        function out=CD(u,e,ds)
            %Central difference scheme
            out=zeros(4,1);

            %Advection term
            out(1) = 0.5 * u;
            out(2) = 0.5 * u;

            %Diffusion term
            out(1) =out(1)+ e / ds;
            out(2) =out(2)- e / ds;
            return;
        end
        function out=HF(u,e,ds)
            %Homogeneous flux (exponential) scheme
            out=zeros(4,1);

            P = df.P(u, e, ds);
            out(1) = e * df.B(-P) / ds;
            out(2) = -e * df.B(P) / ds;
            return;
        end
        function out=CF(u,e,ds)
            %Complete flux scheme
            out=zeros(4,1);

            P = df.P(u, e, ds);
            out(1) = e * df.B(-P) / ds;
            out(2) = -e * df.B(P) / ds;
            out(3) = 1 * (df.C(-P));
            out(4) = -1 * (df.C(P));
            return;
        end
    end
end
