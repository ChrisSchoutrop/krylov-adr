% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
%close all
clc

plot_id=0;

%{
Setup
%}
problem=Input2D;
N=1e2;

Pe=1e-5
Da=1e-5

dx=1./(N-1);

L=dx;
u=Pe/L;
e=1.0;
sl=+Da * (u + e / L) / L;

u_y=u/sqrt(2);
u_x=u_y;

N_x=N;
N_y=N;

problem.phi_w_fn=@(y) 1.0;
problem.phi_e_fn=@(y) 0.0;
problem.phi_n_fn=@(x) 1.0;
problem.phi_s_fn=@(x) 0.0;

problem.x_w=0;
problem.x_e=1;
problem.y_s=0;
problem.y_n=1;

problem.N_x=N_x;
problem.N_y=N_y;

problem.u_x_fn=@(x,y) u_x;
problem.u_y_fn=@(x,y) u_y;
problem.e_fn=@(x,y) e;
problem.s_fn=@(x,y) 0.0;
problem.sl_fn=@(x,y) sl;

%{
Solve the problem
%}
%problem.flux_fn=@(u,e,ds) flux.UW(u,e,ds);
%[A_UW,b_UW]=problem.discretize();
%disp(['condest(A_UW): ',num2str(condest(A_UW),3)])
%problem.flux_fn=@(u,e,ds) flux.CD(u,e,ds);
%[A_CD,b_CD]=problem.discretize();
%disp(['condest(A_CD): ',num2str(condest(A_CD),3)])
problem.flux_fn=@(u,e,ds) flux.HF(u,e,ds);
[A_HF,b_HF]=problem.discretize();
%disp(['condest(A_HF): ',num2str(condest(A_HF),3)])
phi_ones=A_HF\b_HF;
phi=A_HF\b_HF;
phi=problem.assemble_phi(phi);

phi_plot=zeros(problem.N_x,problem.N_y);
x=linspace(problem.x_w,problem.x_e,problem.N_x);
y=linspace(problem.x_w,problem.x_e,problem.N_y);
[X,Y]=meshgrid(x,y);

for i=0:problem.N_x-1
    for j=0:problem.N_y-1
        ix_C =  (i + 0) + problem.N_x * (j + 0)+1;
        phi_plot(i+1,j+1)=phi(ix_C);
    end
end
plot_id=plot_id+1;
figure(plot_id)

contourf(X,Y,phi_plot')
xlabel('x')
ylabel('y')
colormap jet
colorbar

return
%{
Compute the eigenvalues
%}
h=1./(N_x-1);
ev_exact=NaN((N_x-1)*(N_x-1),1);
ev_CD_exact=NaN((N_x-1)*(N_x-1),1);
ev_UW_exact=NaN((N_x-1)*(N_x-1),1);
ev_HF_exact=NaN((N_x-1)*(N_x-1),1);
for ix_x=1:N_x-2
    for ix_y=1:N_y-2
        ev_exact((ix_x-1)*N_x+(ix_y-1)+1)=norm([u_x,u_y],2).^2/(4.*e)+e*(ix_x.^2+ix_y.^2).*pi.^2;

        P1=u_x.*h./e;
        P2=u_y.*h./e;

        ev_CD_exact((ix_x-1)*N_x+(ix_y-1)+1)=2.*e.*(2-sqrt(1-(P1./2).^2).*cos(pi.*ix_x.*h)-sqrt(1-(P2./2).^2).*cos(pi.*ix_y.*h))./h.^2;
        ev_UW_exact((ix_x-1)*N_x+(ix_y-1)+1)=2.*e.*(2+(P1+P2)./2-sqrt(1+P1).*cos(pi.*ix_x.*h)-sqrt(1+P2).*cos(pi.*ix_x.*h))./h.^2;
        ev_HF_exact((ix_x-1)*N_x+(ix_y-1)+1)=2.*e.*(0.5.*P1.*coth(0.5.*P1)+0.5.*P2.*coth(P2./2)-(cos(pi.*ix_x.*h))./(df.sinhc(P1./2))-(cos(pi.*ix_y.*h))./(df.sinhc(P2./2)))./h.^2;
    end
end
%The discretized version has moved a factor h.^2 into the rhs
ev_CD_exact=(ev_CD_exact+sl)*h.^2;
ev_UW_exact=(ev_UW_exact+sl)*h.^2;
ev_HF_exact=(ev_HF_exact+sl)*h.^2;
if N<50
    [V,ev_UW_matlab]=eig(full(A_UW));
    ev_UW_matlab=diag(ev_UW_matlab);
    disp(['cond(V) UW: ',num2str(cond(V),3)])
    [V,ev_CD_matlab]=eig(full(A_CD));
    ev_CD_matlab=diag(ev_CD_matlab);
    disp(['cond(V) CD: ',num2str(cond(V),3)])
    [V,ev_HF_matlab]=eig(full(A_HF));
    ev_HF_matlab=diag(ev_HF_matlab);
    disp(['cond(V) HF: ',num2str(cond(V),3)])
    %{
Filter out the (N_x*N_y)-((N_x-2)*(N_y-2)) eigenvalues that are 1 due to
%the BC.
It is trivial to show that these do not affect any other eigenvalues.
Sketch: det(A-lambda*eye), the BC causes some rows to contain the value 1,
the rest of the row is zero. We can use cofactor expansion of the determinant
to factor out +/-(1-lambda).^((N_x*N_y)-((N_x-2)*(N_y-2))).
    %}
    N_BC=(N_x*N_y)-((N_x-2)*(N_y-2));
    [~,idx]=sort(abs(ev_UW_matlab(:)-1));
    ev_UW_matlab(idx(1:N_BC))=[];
    [~,idx]=sort(abs(ev_CD_matlab(:)-1));
    ev_CD_matlab(idx(1:N_BC))=[];
    [~,idx]=sort(abs(ev_HF_matlab(:)-1));
    ev_HF_matlab(idx(1:N_BC))=[];
else
    ev_UW_matlab=NaN;
    ev_CD_matlab=NaN;
    ev_HF_matlab=NaN;
end



plot_id=plot_id+1;
figure(plot_id)
hold on
scatter(real(ev_exact),imag(ev_exact),'go','displayname',"Exact",'linewidth',1.5)
scatter(real(ev_CD_exact),imag(ev_CD_exact),'rx','displayname',"Exact CD",'linewidth',1.5)
scatter(real(ev_CD_matlab),imag(ev_CD_matlab),'bs','displayname',"Matlab CD",'linewidth',1.5)
xlabel('Re')
ylabel('Im')
title('CD')
set(gca, 'Linewidth', 1.5)
box on
legend('-dynamiclegend')

plot_id=plot_id+1;
figure(plot_id)
hold on
scatter(real(ev_exact),imag(ev_exact),'go','displayname',"Exact",'linewidth',1.5)
scatter(real(ev_UW_exact),imag(ev_UW_exact),'rx','displayname',"Exact UW",'linewidth',1.5)
scatter(real(ev_UW_matlab),imag(ev_UW_matlab),'bs','displayname',"Matlab UW",'linewidth',1.5)
xlabel('Re')
ylabel('Im')
title('UW')
set(gca, 'Linewidth', 1.5)
box on
legend('-dynamiclegend')

plot_id=plot_id+1;
figure(plot_id)
hold on
%scatter(real(ev_exact),imag(ev_exact),'go','displayname',"Exact",'linewidth',1.5)
scatter(real(ev_HF_exact),imag(ev_HF_exact),'rx','displayname',"Exact HF",'linewidth',1.5)
scatter(real(ev_HF_matlab),imag(ev_HF_matlab),'bs','displayname',"Matlab HF",'linewidth',1.5)
xlabel('Re')
ylabel('Im')
title('HF')
set(gca, 'Linewidth', 1.5)
box on
legend('-dynamiclegend')

%{
Solve using Krylov methods
%}
schemes=["UW","CD","HF"];
for schemes_ix=1:length(schemes)
    disp(['Residual for scheme: ',schemes(schemes_ix)])
    switch schemes(schemes_ix)
        case "UW"
            A=A_UW;
            b=b_UW;
        case "CD"
            A=A_CD;
            b=b_CD;
        case "HF"
            A=A_HF;
            b=b_HF;
    end

    %No preconditioner
    M1=[];
    M2=[];

    %Initial guess
    x0=zeros(size(b));

    %Other settings
    tol=1e-12;
    maxit=2000;

    plot_id=plot_id+1;
    figure(plot_id)
    hold on
    x0=zeros(size(b));
    normb=norm(b);
    [x,flag,relres,iter,resvec] = bicgstab(A,b,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres BiCGStab: ',num2str(res)])
    plot([1:1:length(resvec)],resvec/normb,'displayname','BiCGStab','linewidth',1.5)

    [x,flag,relres,iter,resvec] = bicgstabl(A,b,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres BiCGStab(2): ',num2str(res)])
    plot([1:1:length(resvec)],resvec/normb,'displayname','BiCGStab(2)','linewidth',1.5)

    [x,flag,relres,iter,resvec] = idrs(A,b,4,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres IDR(4): ',num2str(res)])
    plot([1:1:length(resvec)],resvec/normb,'displayname','IDR(4)','linewidth',1.5)

    [x,flag,relres,iter,resvec] = gmresG(A,b,[],tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres GMRES(Inf): ',num2str(res)])
    plot([1:length(resvec)],resvec/normb,'displayname','GMRES(Inf)','linewidth',1.5)

    [x,flag,relres,iter,resvec] = gmres(A,b,10,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres GMRES(10): ',num2str(res)])
    plot([1:length(resvec)],resvec/normb,'displayname','GMRES(10)','linewidth',1.5)

    [x,flag,relres,iter,resvec] = rubicgstabl(A,b,2,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres RU-BiCGStab(2): ',num2str(res)])
    plot([1:1:length(resvec)],resvec/normb,'displayname',"RU-BiCGStab(2)",'linewidth',1.5)

    set(gca, 'YScale', 'log')
    set(gca, 'Linewidth', 1.5)
    box on
    xlabel('MV')
    ylabel('Residual')
    title(schemes(schemes_ix))
    legend()
    axis tight

end
