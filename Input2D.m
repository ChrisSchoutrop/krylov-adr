% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef Input2D
    %{
        +X direction: west -> east
        +Y direction: south-> north
        +Z direction: down -> up
        See Fig. 1 in "Discretization and Parallel Iterative Schemes for Advection-Diffusion-Reaction Problems"
    %}
    properties
        %Boundary condition functions
        phi_w_fn; %phi_w(y)
        phi_e_fn; %phi_e(y)
        phi_n_fn; %phi_n(x)
        phi_s_fn; %phi_s(x)

        %Are the boundary conditions Dirichlet? (if they're not, they're Neumann)
        %Note these are functions
        phi_w_dirichlet_fn=@(y) true;
        phi_e_dirichlet_fn=@(y) true;
        phi_n_dirichlet_fn=@(x) true;
        phi_s_dirichlet_fn=@(x) true;

        %Coordinates of the boundaries
        x_w;
        x_e;
        y_s;
        y_n;

        %Cross-flux coefficient, beta=0=not included, beta=1=fully included
        CFbeta=0.0;
        CFalpha=1.0;

        %Number of gridpoints
        N_x;
        N_y;

        %Flux approximation function
        flux_fn %@(u,e,ds) flux.CF(u,e,ds);

        %x,y-Components advection(x,y):
        u_x_fn; %u_x(x,y)
        u_y_fn; %u_y(x,y)

        %Diffusion (x,y)
        e_fn; %e(x,y)

        %Constant source (x,y)
        s_fn; %s(x,y)

        %Linear source(x,y)
        sl_fn; %sl(x,y)
    end
    methods
        function [A,b]=discretize(obj)
            %{
            Discretize such that only the unknowns are computed.
            %}
            %Grid spacing
            dx = (obj.x_e - obj.x_w) / (obj.N_x - 1);
            dy = (obj.y_n - obj.y_s) / (obj.N_y - 1);

            %Prepare A and b
            SA=SparseA;
            b=zeros((obj.N_x-2)*(obj.N_y-2),1);

            %Loop over the non-boundary cells of the grid
            for i=0:obj.N_x-3
                for j=0:obj.N_y-3

                    %Position of the nodal point
                    x = obj.x_w + (i+1) * dx;
                    y = obj.y_s + (j+1) * dy;

                    if(obj.phi_w_dirichlet_fn(y) || obj.phi_e_dirichlet_fn(y)...
                     ||obj.phi_n_dirichlet_fn(x) || obj.phi_s_dirichlet_fn(x))==false
                        error('Neumann boundary conditions not implemented, use discretize_ones() instead.')
                    end

                    %Indices of the 5-point stencil
                    ix_N =  (i + 0) + (obj.N_x-2) * (j + 1)+1;
                    ix_W =  (i - 1) + (obj.N_x-2) * (j + 0)+1;
                    ix_C =  (i + 0) + (obj.N_x-2) * (j + 0)+1;
                    ix_E =  (i + 1) + (obj.N_x-2) * (j + 0)+1;
                    ix_S =  (i + 0) + (obj.N_x-2) * (j - 1)+1;

                    %NOTE: For efficient assembly it would probably be faster to first compute all fluxes in the entire domain
                    %Instead of considering each cell individually, currently many flux calculations are not recycled.
                    F_CN  = obj.flux_fn(obj.u_y_fn(x, y + dy / 2), obj.e_fn(x, y + dy / 2), dy);
                    F_WC  = obj.flux_fn(obj.u_x_fn(x - dx / 2, y), obj.e_fn(x - dx / 2, y), dx);
                    F_CE  = obj.flux_fn(obj.u_x_fn(x + dx / 2, y), obj.e_fn(x + dx / 2, y), dx);
                    F_SC  = obj.flux_fn(obj.u_y_fn(x, y - dy / 2), obj.e_fn(x, y - dy / 2), dy);

                    %North flux
                    %SA.add(ix_C, ix_S) += 0;
                    SA.add(ix_C, ix_C,+ dx * F_CN(1));
                    if j==obj.N_y-3
                        %North Boundary
                        b(ix_C)=b(ix_C)-(+ dx * F_CN(2))*obj.phi_n_fn(x);
                    else
                        SA.add(ix_C, ix_N,+ dx * F_CN(2));
                    end
                    b(ix_C) =b(ix_C) - dy * dx * (F_CN(3) * obj.CFalpha * obj.s_fn(x, y) + F_CN(4) * obj.CFalpha * obj.s_fn(x, y + dy));

                    %East flux
                    %SA.add(ix_C, ix_W) += 0;
                    SA.add(ix_C, ix_C,+ dy * F_CE(1));

                    if i==obj.N_x-3
                        %East Boundary
                        b(ix_C)=b(ix_C)-(+ dy * F_CE(2))*obj.phi_e_fn(y);
                    else
                        SA.add(ix_C, ix_E,+ dy * F_CE(2));
                    end
                    b(ix_C) =b(ix_C)- dx * dy * (F_CE(3) * obj.CFalpha * obj.s_fn(x, y) + F_CE(4) * obj.CFalpha * obj.s_fn(x + dx, y));

                    %South flux
                    if j==0
                        %South Boundary
                        b(ix_C)=b(ix_C)-(- dx * F_SC(1))*obj.phi_s_fn(x);
                    else
                        SA.add(ix_C, ix_S,- dx * F_SC(1));
                    end
                    SA.add(ix_C, ix_C,- dx * F_SC(2));
                    %SA.add(ix_C, ix_N) -= 0;
                    b(ix_C) =b(ix_C)+ dy * dx * (F_SC(3) * obj.CFalpha * obj.s_fn(x, y - dy) + F_SC(4) * obj.CFalpha * obj.s_fn(x, y));

                    %West flux
                    if i==0
                        %West Boundary
                        b(ix_C)=b(ix_C)-(- dy * F_WC(1))*obj.phi_w_fn(y);
                    else
                        SA.add(ix_C, ix_W,- dy * F_WC(1));
                    end
                    SA.add(ix_C, ix_C,- dy * F_WC(2));
                    %SA.add(ix_C, ix_E) -= 0;
                    b(ix_C) =b(ix_C)+ dx * dy * (F_WC(3) * obj.CFalpha * obj.s_fn(x - dx, y) + F_WC(4) * obj.CFalpha * obj.s_fn(x, y));

                    %Source terms
                    %Linear source
                    SA.add(ix_C, ix_C,+ dx * dy * obj.sl_fn(x, y));
                    %Other source
                    b(ix_C) =b(ix_C)+ dx * dy * obj.s_fn(x, y);

                    if (obj.CFbeta ~= 0.0)
                        error('CFCF Not implemented, use discretize_ones() instead')
                    end
                end
            end

            A=SA.assemble();
        end
        function phi=assemble_phi(obj,phi_in)
            %{
            Assembles the full vector phi that contains the boundary
            conditions as well, given the solution obtained from A\b
            obtained from discretize().
            Note that discretize_ones() yields the solution vector
            directly, but has different matrix properties, and the
            enforcement of boundary conditions depends on the accuracy of
            the linear solver.
            %}
            phi=NaN(obj.N_x*obj.N_y,1);

            %Grid spacing
            dx = (obj.x_e - obj.x_w) / (obj.N_x - 1);
            dy = (obj.y_n - obj.y_s) / (obj.N_y - 1);


            %Loop over the all cells of the grid
            for i=0:obj.N_x-1
                for j=0:obj.N_y-1
                    %Position of the nodal point
                    x = obj.x_w + (i) * dx;
                    y = obj.y_s + (j) * dy;

                    if(obj.phi_w_dirichlet_fn(y) || obj.phi_e_dirichlet_fn(y)...
                     ||obj.phi_n_dirichlet_fn(x) || obj.phi_s_dirichlet_fn(x))==false
                        error('Neumann boundary conditions not implemented, use discretize_ones() instead.')
                    end

                    %Index on the grid involving all cells
                    ix_C =  (i + 0) + (obj.N_x) * (j + 0)+1;
                    %Index on the grid involving only non-boundary cells
                    ix_C_phi =  (i + 0-1) + (obj.N_x-2) * (j + 0-1)+1;
                    if i==0
                        %West boundary
                        phi(ix_C)=obj.phi_w_fn(y);
                    elseif i==obj.N_y-1
                        %East boundary
                        phi(ix_C)=obj.phi_e_fn(y);
                    elseif j==0
                        %South boundary
                        phi(ix_C)=obj.phi_s_fn(x);
                    elseif j==obj.N_x-1
                        %North boundary
                        phi(ix_C)=obj.phi_n_fn(x);
                    else
                        phi(ix_C)=phi_in(ix_C_phi);
                    end

                end
            end
        end
        function [A,b]=discretize_ones(obj)
            %{
            Discretize in a non-fiddly way. by just adding a row that
            contains a single 1 to deal with the boundary conditions.
            %}
            %Grid spacing
            dx = (obj.x_e - obj.x_w) / (obj.N_x - 1);
            dy = (obj.y_n - obj.y_s) / (obj.N_y - 1);

            %Total number of cells
            N=obj.N_x*obj.N_y;

            %Prepare A and b
            SA=SparseA;
            b=zeros(N,1);

            %Loop over the grid
            for i=0:obj.N_x-1
                for j=0:obj.N_y-1

                    %Indices of the 5-point stencil
                    ix_N =  (i + 0) + obj.N_x * (j + 1)+1;
                    ix_W =  (i - 1) + obj.N_x * (j + 0)+1;
                    ix_C =  (i + 0) + obj.N_x * (j + 0)+1;
                    ix_E =  (i + 1) + obj.N_x * (j + 0)+1;
                    ix_S =  (i + 0) + obj.N_x * (j - 1)+1;

                    if(obj.CFbeta~=0.0)
                        %Indices of the cross-fluxes (9-point stencil)
                        ix_NW = (i - 1) + obj.N_x * (j + 1)+1;
                        ix_NE = (i + 1) + obj.N_x * (j + 1)+1;
                        ix_SW = (i - 1) + obj.N_x * (j - 1)+1;
                        ix_SE = (i + 1) + obj.N_x * (j - 1)+1;
                    end

                    %Position of the nodal point
                    x = obj.x_w + i * dx;
                    y = obj.y_s + j * dy;

                    %Work out the boundary conditions
                    %Work out the boundary conditions
                    if (i == 0)
                        %West boundary
                        if (obj.phi_w_dirichlet_fn(y))
                            SA.add(ix_C, ix_C,1);
                            b(ix_C) = obj.phi_w_fn(y);
                        else
                            SA.add(ix_C, ix_E,+ 1);
                            SA.add(ix_C, ix_C,- 1);
                            b(ix_C) = dx * obj.phi_w_fn(y);
                        end
                    elseif (i == obj.N_x - 1)
                        %East boundary
                        if (obj.phi_e_dirichlet_fn(y))
                            SA.add(ix_C, ix_C,+ 1);
                            b(ix_C) = obj.phi_e_fn(y);
                        else
                            SA.add(ix_C, ix_C,+ 1);
                            SA.add(ix_C, ix_W,- 1);
                            b(ix_C) = dx * obj.phi_e_fn(y);
                        end
                    elseif (j == 0)
                        %South boundary
                        if (obj.phi_s_dirichlet_fn(x))
                            SA.add(ix_C, ix_C,+ 1);
                            b(ix_C) = obj.phi_s_fn(x);
                        else
                            SA.add(ix_C, ix_N,+ 1);
                            SA.add(ix_C, ix_C,- 1);
                            b(ix_C) = dy * obj.phi_s_fn(x);
                        end
                    elseif (j == obj.N_y - 1)
                        %North boundary
                        if (obj.phi_n_dirichlet_fn(x))
                            SA.add(ix_C, ix_C,+ 1);
                            b(ix_C) = obj.phi_n_fn(x);
                        else
                            SA.add(ix_C, ix_C,+ 1);
                            SA.add(ix_C, ix_S,- 1);
                            b(ix_C) = dy * obj.phi_n_fn(x);
                        end
                    else
                        %NOTE: For efficient assembly it would probably be faster to first compute all fluxes in the entire domain
                        %Instead of considering each cell individually, currently many flux calculations are not recycled.
                        F_CN  = obj.flux_fn(obj.u_y_fn(x, y + dy / 2), obj.e_fn(x, y + dy / 2), dy);
                        F_WC  = obj.flux_fn(obj.u_x_fn(x - dx / 2, y), obj.e_fn(x - dx / 2, y), dx);
                        F_CE  = obj.flux_fn(obj.u_x_fn(x + dx / 2, y), obj.e_fn(x + dx / 2, y), dx);
                        F_SC  = obj.flux_fn(obj.u_y_fn(x, y - dy / 2), obj.e_fn(x, y - dy / 2), dy);

                        if (obj.CFbeta ~= 0.0)
                            %Components of the flux needed for crossflux
                            F_NWN = obj.flux_fn(obj.u_x_fn(x - dx / 2, y + dy), obj.e_fn(x - dx / 2, y + dy), dx);
                            F_NNE = obj.flux_fn(obj.u_x_fn(x + dx / 2, y + dy), obj.e_fn(x + dx / 2, y + dy), dx);
                            F_WNW = obj.flux_fn(obj.u_y_fn(x - dx, y + dy / 2), obj.e_fn(x - dx, y + dy / 2), dy);
                            F_ENE = obj.flux_fn(obj.u_y_fn(x + dx, y + dy / 2), obj.e_fn(x + dx, y + dy / 2), dy);
                            F_SWW = obj.flux_fn(obj.u_y_fn(x - dx, y - dy / 2), obj.e_fn(x - dx, y - dy / 2), dy);
                            F_SEE = obj.flux_fn(obj.u_y_fn(x + dx, y - dy / 2), obj.e_fn(x + dx, y - dy / 2), dy);
                            F_SWS = obj.flux_fn(obj.u_x_fn(x - dx / 2, y - dy), obj.e_fn(x - dx / 2, y - dy), dx);
                            F_SSE = obj.flux_fn(obj.u_x_fn(x + dx / 2, y - dy), obj.e_fn(x + dx / 2, y - dy), dx);
                        end
                        %Not at a boundary
                        %CF Scheme without the effects of cross flux:

                        %North flux
                        %SA.add(ix_C, ix_S) += 0;
                        SA.add(ix_C, ix_C,+ dx * F_CN(1));
                        SA.add(ix_C, ix_N,+ dx * F_CN(2));
                        b(ix_C) =b(ix_C) - dy * dx * (F_CN(3) * obj.CFalpha * obj.s_fn(x, y) + F_CN(4) * obj.CFalpha * obj.s_fn(x, y + dy));
                        %East flux
                        %SA.add(ix_C, ix_W) += 0;
                        SA.add(ix_C, ix_C,+ dy * F_CE(1));
                        SA.add(ix_C, ix_E,+ dy * F_CE(2));
                        b(ix_C) =b(ix_C)- dx * dy * (F_CE(3) * obj.CFalpha * obj.s_fn(x, y) + F_CE(4) * obj.CFalpha * obj.s_fn(x + dx, y));
                        %South flux
                        SA.add(ix_C, ix_S,- dx * F_SC(1));
                        SA.add(ix_C, ix_C,- dx * F_SC(2));
                        %SA.add(ix_C, ix_N) -= 0;
                        b(ix_C) =b(ix_C)+ dy * dx * (F_SC(3) * obj.CFalpha * obj.s_fn(x, y - dy) + F_SC(4) * obj.CFalpha * obj.s_fn(x, y));
                        %West flux
                        SA.add(ix_C, ix_W,- dy * F_WC(1));
                        SA.add(ix_C, ix_C,- dy * F_WC(2));
                        %SA.add(ix_C, ix_E) -= 0;
                        b(ix_C) =b(ix_C)+ dx * dy * (F_WC(3) * obj.CFalpha * obj.s_fn(x - dx, y) + F_WC(4) * obj.CFalpha * obj.s_fn(x, y));
                        %Linear source
                        SA.add(ix_C, ix_C,+ dx * dy * obj.sl_fn(x, y));
                        %Other source
                        b(ix_C) =b(ix_C)+ dx * dy * obj.s_fn(x, y);


                        if (obj.CFbeta == 0.0)
                            %If obj.CFbeta==0.0 there is no cross flux, we can skip the rest.
                            continue;
                        end

                        %Cross flux in F_CE
                        %x_{i,j} part
                        SA.add(ix_C, ix_C+ (-obj.CFbeta * dx * dy * F_CE(3)) * F_CN(1) / dy);
                        SA.add(ix_C, ix_N+ (-obj.CFbeta * dx * dy * F_CE(3)) * F_CN(2) / dy);
                        SA.add(ix_C, ix_S- (-obj.CFbeta * dx * dy * F_CE(3)) * F_SC(1) / dy);
                        SA.add(ix_C, ix_C- (-obj.CFbeta * dx * dy * F_CE(3)) * F_SC(2) / dy);
                        %x_{i+1,j} part
                        SA.add(ix_C, ix_E,+  (-obj.CFbeta * dx * dy * F_CE(4)) * F_ENE(1) / dy)
                        SA.add(ix_C, ix_NE,+ (-obj.CFbeta * dx * dy * F_CE(4)) * F_ENE(2) / dy)
                        SA.add(ix_C, ix_SE,- (-obj.CFbeta * dx * dy * F_CE(4)) * F_SEE(1) / dy)
                        SA.add(ix_C, ix_E,-  (-obj.CFbeta * dx * dy * F_CE(4)) * F_SEE(2) / dy)

                        %Cross flux in F_WC
                        %x_{i-1,j} part
                        SA.add(ix_C, ix_W ,+  (obj.CFbeta * dx * dy * F_WC(3)) * F_WNW(1) / dy);
                        SA.add(ix_C, ix_NW,+ (obj.CFbeta * dx * dy * F_WC(3)) * F_WNW(2) / dy);
                        SA.add(ix_C, ix_SW,- (obj.CFbeta * dx * dy * F_WC(3)) * F_SWW(1) / dy);
                        SA.add(ix_C, ix_W ,-  (obj.CFbeta * dx * dy * F_WC(3)) * F_SWW(2) / dy);
                        %x_{i,j} part
                        SA.add(ix_C, ix_C,+ (obj.CFbeta * dx * dy * F_WC(4)) * F_CN(1) / dy);
                        SA.add(ix_C, ix_N,+ (obj.CFbeta * dx * dy * F_WC(4)) * F_CN(2) / dy);
                        SA.add(ix_C, ix_S,- (obj.CFbeta * dx * dy * F_WC(4)) * F_SC(1) / dy);
                        SA.add(ix_C, ix_C,- (obj.CFbeta * dx * dy * F_WC(4)) * F_SC(2) / dy);

                        %Cross flux in F_CN
                        %TODO: Double check all cross flux parts after completing
                        %TODO: Make convergence test to check if all schemes are implemented properly
                        %TODO: Documentation in code
                        %TODO: Documentation in paper (especially the notation)
                        %x_{i,j} part
                        SA.add(ix_C, ix_C,+ (-obj.CFbeta * dx * dy * F_CN(3)) * F_CE(1) / dx);
                        SA.add(ix_C, ix_E,+ (-obj.CFbeta * dx * dy * F_CN(3)) * F_CE(2) / dx);
                        SA.add(ix_C, ix_W,- (-obj.CFbeta * dx * dy * F_CN(3)) * F_WC(1) / dx);
                        SA.add(ix_C, ix_C,- (-obj.CFbeta * dx * dy * F_CN(3)) * F_WC(2) / dx);
                        %x_{i,j+1} part
                        SA.add(ix_C, ix_N ,+ (-obj.CFbeta * dx * dy * F_CN(4)) * F_NNE(1) / dx);
                        SA.add(ix_C, ix_NE,+ (-obj.CFbeta * dx * dy * F_CN(4)) * F_NNE(2) / dx);
                        SA.add(ix_C, ix_NW,- (-obj.CFbeta * dx * dy * F_CN(4)) * F_NWN(1) / dx);
                        SA.add(ix_C, ix_N ,- (-obj.CFbeta * dx * dy * F_CN(4)) * F_NWN(2) / dx);

                        %Cross flux in F_SC
                        %x_{i,j-1} part
                        SA.add(ix_C, ix_S ,+ (obj.CFbeta * dx * dy * F_SC(3)) * F_SSE(1) / dx);
                        SA.add(ix_C, ix_SE,+ (obj.CFbeta * dx * dy * F_SC(3)) * F_SSE(2) / dx);
                        SA.add(ix_C, ix_SW,- (obj.CFbeta * dx * dy * F_SC(3)) * F_SWS(1) / dx);
                        SA.add(ix_C, ix_S ,- (obj.CFbeta * dx * dy * F_SC(3)) * F_SWS(2) / dx);
                        %x_{i,j} part
                        SA.add(ix_C, ix_C,+ (obj.CFbeta * dx * dy * F_SC(4)) * F_CE(1) / dx);
                        SA.add(ix_C, ix_E,+ (obj.CFbeta * dx * dy * F_SC(4)) * F_CE(2) / dx);
                        SA.add(ix_C, ix_W,- (obj.CFbeta * dx * dy * F_SC(4)) * F_WC(1) / dx);
                        SA.add(ix_C, ix_C,- (obj.CFbeta * dx * dy * F_SC(4)) * F_WC(2) / dx);
                    end
                end
            end
            A=SA.assemble();
        end
        function obj=set_CFbeta(obj,beta)
            obj.CFbeta = beta;
            obj.CFalpha = (1 + obj.CFbeta) / 2;
        end
        function obj=set_crossflux(obj,enabled)
            %Enable or disable the crossflux terms in the CF scheme
            %CF with crossflux is referred to as CFCF.
            if (enabled)
                obj.set_CFbeta(1.0);
            else
                obj.set_CFbeta(0.0);
            end
        end
    end
end
