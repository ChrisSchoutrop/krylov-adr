% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function result=problem_2D(N,Pe,e,Da,scheme,solvers)

    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input2D;

    %Fixed input
    problem.phi_w_fn=@(y) 1;
    problem.phi_e_fn=@(y) 1;
    problem.phi_s_fn=@(x) 1;
    problem.phi_n_fn=@(x) 1;
    problem.x_w=0;
    problem.x_e=1;
    problem.y_s=0;
    problem.y_n=1;

    %Variable input
    problem.N_x=N;
    problem.N_y=N;

    %Work out the other parameters from the input
    dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
    u=Pe/dx;
    sl=Da * (u + e / dx) / dx;
    s=1.0;

    %Set the flux approximation based on the scheme used
    switch scheme
        case "UW"
            problem.flux_fn=@(u,e,dx) flux.UW(u,e,dx);
        case "CD"
            problem.flux_fn=@(u,e,dx) flux.CD(u,e,dx);
        case "HF"
            problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);
        case "CF"
            problem.flux_fn=@(u,e,dx) flux.CF(u,e,dx);
        case "CFCF"
            %problem.flux_fn=@(u,e,dx) flux.CF(u,e,dx);
            %problem.set_crossflux(true)
            error('CFCF scheme not defined for 1D')
    end
    %Advection
    problem.u_x_fn=@(x,y) u/sqrt(2);
    problem.u_y_fn=@(x,y) u/sqrt(2);
    %Diffusion
    problem.e_fn=@(x,y) e;
    %Constant source
    problem.s_fn=@(x,y) s;
    %Linear source
    problem.sl_fn=@(x,y) sl;

    [A,b]=problem.discretize();

    result=solve_problem(A,b,N,u,e,s,sl,scheme,solvers);
end
