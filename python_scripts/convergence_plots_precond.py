'''
Copyright (C) 2019-2022 Eindhoven University of Technology.

This code is free software, you can redistribute it and/or modify it under
the terms of the GNU General Public License; either version 3.0 of the
License, or (at your option) any later version. See LICENSE for details.

This code is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
        The Plasimo Team (https://plasimo.phys.tue.nl)
'''
from scipy.io import loadmat
from numpy import size
#from matplotlib.pyplot import plot,figure,xlabel,ylabel,show,clf,xlim,legend
import matplotlib.pyplot as plt
import matplotlib

folder = "N256/"
files = "Pe= 100000  Da= 100000  Figure:  1.fig", "Pe= 100000  Da= 100000  Figure:  2.fig", "Pe= 100000  Da= 100000  Figure:  3.fig", "Pe= 100000  Da= 1e-05  Figure:  1.fig", "Pe= 100000  Da= 1e-05  Figure:  2.fig", "Pe= 100000  Da= 1e-05  Figure:  3.fig", "Pe= 100000  Da= 1  Figure:  1.fig", "Pe= 100000  Da= 1  Figure:  2.fig", "Pe= 100000  Da= 1  Figure:  3.fig", "Pe= 1  Da= 100000  Figure:  1.fig", "Pe= 1  Da= 100000  Figure:  2.fig", "Pe= 1  Da= 100000  Figure:  3.fig", "Pe= 1  Da= 1e-05  Figure:  1.fig", "Pe= 1  Da= 1e-05  Figure:  2.fig", "Pe= 1  Da= 1e-05  Figure:  3.fig", "Pe= 1  Da= 1  Figure:  1.fig", "Pe= 1  Da= 1  Figure:  2.fig", "Pe= 1  Da= 1  Figure:  3.fig", "Pe= 1e-05  Da= 100000  Figure:  1.fig", "Pe= 1e-05  Da= 100000  Figure:  2.fig", "Pe= 1e-05  Da= 100000  Figure:  3.fig", "Pe= 1e-05  Da= 1e-05  Figure:  1.fig", "Pe= 1e-05  Da= 1e-05  Figure:  2.fig", "Pe= 1e-05  Da= 1e-05  Figure:  3.fig", "Pe= 1e-05  Da= 1  Figure:  1.fig", "Pe= 1e-05  Da= 1  Figure:  2.fig", "Pe= 1e-05  Da= 1  Figure:  3.fig",

x = loadmat(folder+files[0])
print(x["hgS_070000"])

font = {'family': 'normal',
		'weight': 'normal',
		'size': 15}
matplotlib.rc('font', **font)

def plotFig(filename):
	f = plt.figure()
	ax = plt.gca()
	d = loadmat(filename, squeeze_me=True, struct_as_record=False)
	matfig = d['hgS_070000']
	childs = matfig.children
	ax1 = [c for c in childs if c.type == 'axes']
	f=plt.figure()
	plt.clf()
	print(ax1[0].children[0].properties.__dict__)
	#exit()
	leg_entries=[]
	#plt.hold(True)
	counter = 0
	for line in ax1[0].children:
		if line.type == 'graph2d.lineseries':
			# if hasattr(line.properties,'Marker'):
			#     mark = "%s" % line.properties.Marker
			#     mark = mark[0]
			# else:
			#     mark = '.'
			# if hasattr(line.properties,'LineStyle'):
			#     linestyle = "%s" % line.properties.LineStyle
			# else:
			#     linestyle = '-'
			# if hasattr(line.properties,'Color'):
			#     r,g,b =  line.properties.Color
			# else:
			#     r = 0
			#     g = 0
			#     b = 1
			# if hasattr(line.properties,'MarkerSize'):
			#     marker_size = line.properties.MarkerSize
			# else:
			#     marker_size = 1
			x = line.properties.XData
			y = line.properties.YData
			leg_entries.append(line.properties.DisplayName)
			#plt.plot(x,y,marker=mark,linestyle=linestyle,markersize=marker_size)
			plt.plot(x, y)
		elif line.type == 'text':
			if counter < 1:
				plt.xlabel("%s" % line.properties.String)
				counter += 1
			elif counter < 2:
				#plt.ylabel("%s" % line.properties.String)
				plt.ylabel("Relative residual")
				counter += 1
	plt.xlim(ax1[0].properties.XLim)
	if leg_entries:
		for ix,leg_entry in enumerate(leg_entries):
			if leg_entry=="none":
				leg_entries[ix]="No preconditioner"
			elif leg_entry=="jacobi":
				leg_entries[ix]="Jacobi"
			elif leg_entry=="ilu0":
				leg_entries[ix]="ILU(0)"
			elif leg_entry=="multigrid4":
				leg_entries[ix]="Multigrid"
		plt.legend(leg_entries,fontsize='small', frameon=False)
	plt.xscale('linear')
	plt.yscale('log')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	plt.xlim((0,None))
	plt.autoscale(enable=True, axis='y', tight=False)
	#ax.legend(fontsize='small', frameon=False)
	f.savefig(filename.replace(" ","").replace(".fig","").replace(":","")+".pdf", bbox_inches='tight')

for file in files:
	plotFig(folder+file)