% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function result=solve_problem(A,b,N,u,e,s,sl,scheme,solver_vec)
%{
    Solves the problem generated by problem_1D, problem_2D, problem_3D.
    Results are:
    1   solver algorithm used
    2   true residual
    3   estimated residual
    4   number of MV
    5   flag returned by the solver
    6   u,  advection
    7   sl, linear source
    8   e, Diffusion coefficient
    9   Time required to solve the linear system
    10  N, number of gridpoints in one direction
    11  maximum number of MV
    12  tolerance set
    13  S used in the solver
    14  L used in the solver
    15  timestamp when the solving started
    16  discretization scheme used
    17  condition number of A
    18  s, constant source
%}
tolerance=1e-12;
max_mv=10000;

result=cell(length(solver_vec),17);


%Initial guess
x0=zeros(size(b));

start_setup_time=posixtime(datetime('now'));

%No preconditioner
M1=[];
M2=[];

setup_time=posixtime(datetime('now'))-start_setup_time;

% if length(A)>1e5
%     disp('Computing cond')
%     cond_A=condest(A);
% else
%     cond_A=condest(A);
% end
cond_A=-1;

norm_b=norm(b);
for ix_solver=1:length(solver_vec)
    solver=solver_vec(ix_solver);

    %S is only applicable for idr(S), it is defined in the IDR(S) section
    S=1;

    if length(A)>1e5
        disp(['Solver ',num2str(ix_solver),'/',num2str(length(solver_vec))])
    end


    start_time_solver=posixtime(datetime('now'));

    if contains(solver,"_L1_")
        L=1;
    else
        L=2;
    end
    options.L=L;

    if contains(solver,"_bicgstab_")
        mv_per_iter=2;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=bicgstab(A,b,tolerance,max_iters,M1,M2,x0);
        L=1;
        len_relres=length(relres);
    elseif contains(solver,"_bicgstabl_")
        %For L=2 this performs 4 MV/iter
        mv_per_iter=4;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=bicgstabl(A,b,tolerance,max_iters,M1,M2,x0);
        L=2;
        len_relres=length(relres);
    elseif contains(solver,"_idrs_")
        %For S=4 this performs 4 MV/iter
        mv_per_iter=4;
        max_iters=max_mv/mv_per_iter;
        S=4;
        [x,flag,relres,iter]=idrs(A,b,S,tolerance,max_mv,M1,M2,x0);
        len_relres=length(relres);
    elseif contains(solver,"_gmres_")
        %1 MV/iter
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=gmres(A,b,[],tolerance,max_iters,M1,M2,x0);
        iter=iter(2);
        len_relres=length(relres);
    elseif contains(solver,"_gmres1_")
        %1 MV/iter
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=gmres(A,b,1,tolerance,max_iters,M1,M2,x0);
        iter=iter(2);
        len_relres=length(relres);
    elseif contains(solver,"_gmres10_")
        %1 MV/iter
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=gmres(A,b,10,tolerance,max_iters,M1,M2,x0);
        iter=iter(2);
        len_relres=length(relres);
    elseif contains(solver,"_gmres50_")
        %1 MV/iter
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=gmres(A,b,50,tolerance,max_iters,M1,M2,x0);
        iter=iter(2);
        len_relres=length(relres);
    elseif contains(solver,"_gmres100_")
        %1 MV/iter
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=gmres(A,b,100,tolerance,max_iters,M1,M2,x0);
        iter=iter(2);
        len_relres=length(relres);
    elseif contains(solver,"_gmres250_")
        %1 MV/iter
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=gmres(A,b,250,tolerance,max_iters,M1,M2,x0);
        iter=iter(2);
        len_relres=length(relres);
    elseif contains(solver,"_rubicgstabl_")
        %length(relres)=MV
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=rubicgstabl(A,b,tolerance,max_iters/(2*L),M1,M2,x0);
        len_relres=length(relres);
    elseif contains(solver,"_rubicgstablv1_")
        %length(relres)=MV
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        options.reliable_update=true;
        options.random_rtilde=false;
        options.keep_best=true;
        [x,flag,relres,iter]=rubicgstabl(A,b,tolerance,max_iters/(2*L),M1,M2,x0,options);
        len_relres=length(relres);
    elseif contains(solver,"_rubicgstablv2_")
        %length(relres)=MV
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        options.reliable_update=false;
        options.random_rtilde=true;
        options.keep_best=true;
        [x,flag,relres,iter]=rubicgstabl(A,b,tolerance,max_iters/(2*L),M1,M2,x0,options);
        len_relres=length(relres);
    elseif contains(solver,"_rubicgstablv3_")
        %length(relres)=MV
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        options.reliable_update=true;
        options.random_rtilde=true;
        options.keep_best=false;
        [x,flag,relres,iter]=rubicgstabl(A,b,tolerance,max_iters/(2*L),M1,M2,x0,options);
        len_relres=length(relres);
    elseif contains(solver,"_rubicgstablv4_")
        %length(relres)=MV
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        options.reliable_update=false;
        options.random_rtilde=true;
        options.keep_best=false;
        [x,flag,relres,iter]=rubicgstabl(A,b,tolerance,max_iters/(2*L),M1,M2,x0,options);
        len_relres=length(relres);
    elseif contains(solver,"_rubicgstablv5_")
        %length(relres)=MV
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        options.reliable_update=false;
        options.random_rtilde=false;
        options.keep_best=true;
        [x,flag,relres,iter]=rubicgstabl(A,b,tolerance,max_iters/(2*L),M1,M2,x0,options);
        len_relres=length(relres);
    elseif contains(solver,"_rubicgstablv6_")
        %length(relres)=MV
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        options.reliable_update=true;
        options.random_rtilde=false;
        options.keep_best=false;
        [x,flag,relres,iter]=rubicgstabl(A,b,tolerance,max_iters/(2*L),M1,M2,x0,options);
        len_relres=length(relres);
    elseif contains(solver,"_rubicgstablv7_")
        %length(relres)=MV
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        options.reliable_update=false;
        options.random_rtilde=false;
        options.keep_best=false;
        [x,flag,relres,iter]=rubicgstabl(A,b,tolerance,max_iters/(2*L),M1,M2,x0,options);
        len_relres=length(relres);
    elseif contains(solver,"_lmr_")
        %length(relres)=MV
        mv_per_iter=1;
        max_iters=max_mv/mv_per_iter;
        [x,flag,relres,iter]=lmr(A,b,tolerance,max_iters,M1,M2,x0);
        len_relres=length(relres);
    end

    %Save the results to the result cells
    result{ix_solver,1} = solver;
    result{ix_solver,2} = num2str(norm(b-A*x)/norm_b);
    result{ix_solver,3} = num2str(relres);
    result{ix_solver,4} = num2str(ceil(iter/mv_per_iter));
    result{ix_solver,5} = num2str(flag);
    result{ix_solver,6} = num2str(u);
    result{ix_solver,7} = num2str(sl);
    result{ix_solver,8} = num2str(e);
    result{ix_solver,9} = num2str((posixtime(datetime('now'))-start_time_solver)+setup_time);
    result{ix_solver,10}= num2str(N);
    result{ix_solver,11}= num2str(max_mv);
    result{ix_solver,12}= num2str(tolerance);
    result{ix_solver,13}= num2str(S);
    result{ix_solver,14}= num2str(L);
    result{ix_solver,15}= num2str(floor(start_time_solver));
    result{ix_solver,16}= scheme;
    result{ix_solver,17}= num2str(cond_A);
    result{ix_solver,18}= num2str(s);
    result{ix_solver,19}= len_relres;
end

end
