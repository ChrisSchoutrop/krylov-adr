% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
close all
clc

problem=Input1D;
problem.phi_w=1;
problem.phi_e=1;

problem.x_w=0;
problem.x_e=1;
problem.N_x=1000;

Pe=1e6;
Da=1e6;

%{
Observation: for the CD scheme if Re/Im<1 BiCGStab seems to become
problematic. The specific size of the eigenvalues seems irrelevant, only
the ratio of the real to imaginary part.
%}

dx=(problem.x_e-problem.x_w)/(problem.N_x-1);

L=dx;
u=Pe/L;
e=1.0;
s=1.0;
sl=-Da * (u + e / L) / L;

problem.flux_fn=@(u,e,dx) flux.UW(u,e,dx);
problem.u_fn=@(x) u;
problem.e_fn=@(x) e;
problem.s_fn=@(x) s;
problem.sl_fn=@(x) sl;

%{
    Discretization, we only need to solve the non-boundary cells
%}
[A,b]=problem.discretize();
phi=A\b;
phi=[problem.phi_w;phi;problem.phi_e];

figure(1)
clf
%plot(linspace(problem.x_w,problem.x_e,problem.N_x),phi,'linewidth',1.5);
xlabel('x')
ylabel('phi')
set(gca, 'Linewidth', 1.5)
box on
axis tight

figure(2)
clf
hold on
normb=norm(b);
x0=ones(size(b));

%No preconditioner
M1=[];
M2=[];

%{
Solve the systems
%}
[x,flag,relres,iter,resvec] = bicgstab(A,b,1e-12,2000,M1,M2,x0);
disp(['Relres BiCGStab: ',num2str(relres)])
plot([1:1:length(resvec)],resvec/normb,'displayname','BiCGStab','linewidth',1.5)

[x,flag,relres,iter,resvec] = bicgstabl(A,b,1e-12,2000,M1,M2,x0);
disp(['Relres BiCGStab(2): ',num2str(relres)])
% iter
% length(resvec)
plot([1:1:length(resvec)],resvec/normb,'displayname','BiCGStab(2)','linewidth',1.5)

[x,flag,relres,iter,resvec] = idrs(A,b,4,1e-12,2000,M1,M2,x0);
disp(['Relres IDR(4): ',num2str(relres)])
plot([1:1:length(resvec)],resvec/normb,'displayname','IDR(4)','linewidth',1.5)

[x,flag,relres,iter,resvec] = gmres(A,b,[],1e-12,2000,M1,M2,x0);
disp(['Relres GMRES: ',num2str(relres)])
plot([1:length(resvec)],resvec/normb,'displayname','GMRES','linewidth',1.5)

[x,flag,relres,iter,resvec] = rubicgstabl(A,b,1e-12,2000,M1,M2,x0);
disp(['Relres RU-BiCGStab(2): ',num2str(relres)])
plot([1:4:4*length(resvec)],resvec/normb,'displayname',"RU-BiCGStab(2)",'linewidth',1.5)

set(gca, 'YScale', 'log')
set(gca, 'Linewidth', 1.5)
box on
xlabel('MV')
ylabel('Residual')
legend()
axis tight

%{
Eigenvalues
%}
if length(A)>1000
    return
end

figure(3)
clf
Av=A(2:end-1,2:end-1);
ev=eig(full(Av));
scatter(real(ev),imag(ev),'rx','Linewidth', 1.5,'displayname','Matlab');
hold on
xlabel('Re')
ylabel('Im')

[Q,N]=schur(full(Av),'complex');
ev2=diag(N);
scatter(real(ev2),imag(ev2),'bo','Linewidth', 1.5,'displayname','Schur');
xlabel('Re')
ylabel('Im')

[AA,BB,Q,Z]=qz(full(Av),eye(length(Av)));
ev3=diag(AA);
scatter(real(ev3),imag(ev3),'ks','Linewidth', 1.5,'displayname','QZ');
xlabel('Re')
ylabel('Im')

sigma=full(Av(10,9));
delta=full(Av(10,10));
tau=full(Av(10,11));
[D,X,Y]=eig_toep(sigma,delta,tau,length(Av));

ev4=diag(D);
scatter(real(ev4),imag(ev4),'gd','Linewidth', 1.5,'displayname','Toeplitz');
xlabel('Re')
ylabel('Im')
set(gca, 'Linewidth', 1.5)
box on
axis tight
legend()

%{
Check if the eigenvalue calculation is done accurately. It seems that if V
is badly conditioned, the eigenvalues computed from eig may not be
representative of the true eigenvalues.
This should be near 0
%}
disp('Relative error in the eigenvalue decompositions:')
normAv=norm(full(Av));
[V,L]=eig(full(Av));
disp('Matlab eigensystem')
norm(Av*V-V*L)./normAv
disp('Matlab eigensystem 2')
norm(Av-V*L/V)./normAv
disp('Toeplitz right eigensystem')
norm(Av*X-X*D)./normAv
disp('Toeplitz right eigensystem 2')
norm(Av-X*D/X)./normAv
disp('Toeplitz left eigensystem')
norm(Y'*Av-D*Y')./normAv
disp('Toeplitz left eigensystem 2')
norm(Av-(Y')\D*Y')./normAv
