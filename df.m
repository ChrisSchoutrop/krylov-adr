% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef df
    %{
    Functions used in the discretization schemes
    df: Discretization Functions

    Arguments:
    u   Advection velocity component (Scalar)
    e   Diffusion coefficient
    ds  gridspacing
    %}
    methods (Static)
        function out=B(z)
            %{
	        Generating function for the Bernoulli numbers
	        B(z)=z/(exp(z)-1)
            %}
            if(abs(z)>1e-3)
                out=z/expm1(z);
                return
            else
                %Use series expansion to avoid division by 0
                B0 = +1.0;
                B1 = -1.0 / 2;
                B2 = +1.0 / 6;
                B4 = -1.0 / 30;
                B6 = +1.0 / 42;
                C0 = B0 / 1;
                C1 = B1 / 1;
                C2 = B2 / 2;
                C4 = B4 / 24;
                C6 = B6 / 720;

                zz = z * z;
                out= C0 + C1 * z + (C2 + (C4 + C6 * zz) * zz) * zz;
                return
            end
        end
        function out=C(z)
            %{
                Function C(z)
                C(Z)=(exp(z/2)-1-z/2)/(z(exp(z)-1))
                z->inf, C~1/(2z sinh(z/2))
                z->-inf,C~ 0.5+1/z
            %}
            expm1z = expm1(z);

            if (isinf(expm1z))
                %For double precision exp(z) is inf for z>709.
                %C(Z) tends to 1/(2z sinh(z/2))
                %This is exponentially close to 1/(2z exp(z/2))
                %This can probably be set to 0.0 for any practical application
                out=1 / (2 * z * sinh(z / 2));
                return
            else

                if (abs(z) > 1.0e-3)
                    out=(expm1(z / 2.0) - z / 2.0) / (z * expm1z);
                    return
                else
                    %Use series expansion to avoid division by 0
                    %C(Z)~1/8-z/24+z^2/384+z^3/1440
                    %The cubic term is well below machine epsilon if
                    %|z|<1e-7
                    %Horner form of 1/8-z/24+z^2/384+z^3/1440
                    %return 1 / 8.0 + z * (-(1 / 24.0) + (1 / 384.0 + z / 1440.0) * z);
                    C0 = 1 / 8.0;
                    C1 = -1 / 24.0;
                    C2 = 1 / 384.0;
                    C3 = 1 / 1440.0;
                    C4 = -1 / 15360.0;
                    out=C0 + z * (C1 + z * (C2 + z * (C3 - z * C4)));
                    return
                    %return 1/8.0+z*(-(1/24.0)+z*(1/384.0+(1/1440.0-z/15360.0)*z));

                end
            end
        end
        function out=W(z)
            %{
	        W function
	        W(z)=(exp(z)-1-z)/(z*(exp(z)-1))
            %}
            if (abs(z) > 1e-7)
                exmp1z = expm1(z);
                if (isinf(exmp1z))
                    %Use asymptotic value for W to avoid
                    %inf/inf
                    %W(z)=1/z-1/expm1(z)
                    if (z > 0)
                        out= 0.0 + 1.0 / z;
                        return
                    else
                        out= 1.0 + 1.0 / z;
                        return
                    end
                else
                    %No weird exceptions occured, just apply formula
                    %for W(z)
                    out= (exmp1z - z) / (z * exmp1z);
                    return
                end
            else
                out= z * (z * z / 720.0 - 1.0 / 12.0) + 0.5;
                return
            end
        end
        function out=sgn(z)
            if (z >= 0.0)
                out= 1.0;
                return
            else
                out= -1.0;
                return
            end
        end
        function out=A(u,e)
            out=u/e;
            return
        end
        function out=P(u,e,ds)
            out=ds*df.A(u,e);
            return
        end
        function out=sinhc(z)
            if(abs(z)>1e-3)
                out=sinh(z)/z;
                return
            else
                z2=z*z;
                out=1.0 + z2 * (1.0 / 6.0 + z2 * (1.0 / 120.0 + z2 / 5040.0));
                return
            end
        end
    end
end
