N_inner_points =
   256
    "Pe="    "1e-05"    "Da="    "1e-05"
Relres BiCGStab: 9.8289e-13
    "Setup time"    "none"    "0.001606"
    "Solve time"    "none"    "300.9937"
    "Wall  time"    "none"    "300.9953"
Relres BiCGStab: 9.9959e-13
    "Setup time"    "jacobi"    "0.83819"
    "Solve time"    "jacobi"    "402.5365"
    "Wall  time"    "jacobi"    "403.3747"
Relres BiCGStab: 9.4553e-13
    "Setup time"    "ilu0"    "1.1454"
    "Solve time"    "ilu0"    "339.7594"
    "Wall  time"    "ilu0"    "340.9047"
Level 0: The problem is of dimension 16777216 with 117047296 nonzeros.
Level 1: The problem is of dimension 2097152 with 14581760 nonzeros.
Level 2: The problem is of dimension 262144 with 1810432 nonzeros.
Level 3: The problem is of dimension 32768 with 223232 nonzeros.
Level 4: The problem is of dimension 4096 with 27136 nonzeros.
Relres BiCGStab: 1.8585e-13
    "Setup time"    "multigrid4"    "1064.0589"
    "Solve time"    "multigrid4"    "988.597"
    "Wall  time"    "multigrid4"    "2052.656"
========================================================
    "Pe="    "1e-05"    "Da="    "1"
Relres BiCGStab: 9.8103e-13
    "Setup time"    "none"    "0.00067711"
    "Solve time"    "none"    "12.4066"
    "Wall  time"    "none"    "12.4072"
Relres BiCGStab: 9.8103e-13
    "Setup time"    "jacobi"    "0.84162"
    "Solve time"    "jacobi"    "18.5187"
    "Wall  time"    "jacobi"    "19.3603"
Relres BiCGStab: 6.1143e-13
    "Setup time"    "ilu0"    "1.1616"
    "Solve time"    "ilu0"    "11.6296"
    "Wall  time"    "ilu0"    "12.7912"
Level 0: The problem is of dimension 16777216 with 117047296 nonzeros.
Level 1: The problem is of dimension 2097152 with 14581760 nonzeros.
Level 2: The problem is of dimension 262144 with 1810432 nonzeros.
Level 3: The problem is of dimension 32768 with 223232 nonzeros.
Level 4: The problem is of dimension 4096 with 27136 nonzeros.
Relres BiCGStab: 7.9886e-13
    "Setup time"    "multigrid4"    "1055.1089"
    "Solve time"    "multigrid4"    "31.9166"
    "Wall  time"    "multigrid4"    "1087.0255"
========================================================
    "Pe="    "1e-05"    "Da="    "100000"
Relres BiCGStab: 2.1644e-15
    "Setup time"    "none"    "0.00017405"
    "Solve time"    "none"    "0.87756"
    "Wall  time"    "none"    "0.87774"
Relres BiCGStab: 2.1627e-15
    "Setup time"    "jacobi"    "0.82468"
    "Solve time"    "jacobi"    "1.1717"
    "Wall  time"    "jacobi"    "1.9963"
Relres BiCGStab: 2.4107e-16
    "Setup time"    "ilu0"    "1.172"
    "Solve time"    "ilu0"    "1.5054"
    "Wall  time"    "ilu0"    "2.6774"
Level 0: The problem is of dimension 16777216 with 117047296 nonzeros.
Level 1: The problem is of dimension 2097152 with 14581760 nonzeros.
Level 2: The problem is of dimension 262144 with 1810432 nonzeros.
Level 3: The problem is of dimension 32768 with 223232 nonzeros.
Level 4: The problem is of dimension 4096 with 27136 nonzeros.
Relres BiCGStab: 6.0422e-17
    "Setup time"    "multigrid4"    "1060.1201"
    "Solve time"    "multigrid4"    "2.8903"
    "Wall  time"    "multigrid4"    "1063.0103"
========================================================
    "Pe="    "1"    "Da="    "1e-05"
Relres BiCGStab: 0.0013787
    "Setup time"    "none"    "0.00018311"
    "Solve time"    "none"    "389.3211"
    "Wall  time"    "none"    "389.3213"
Relres BiCGStab: 2.4667e-06
    "Setup time"    "jacobi"    "0.85145"
    "Solve time"    "jacobi"    "549.0032"
    "Wall  time"    "jacobi"    "549.8546"
Relres BiCGStab: 1.3073e-07
    "Setup time"    "ilu0"    "1.1807"
    "Solve time"    "ilu0"    "219.6008"
    "Wall  time"    "ilu0"    "220.7815"
Level 0: The problem is of dimension 16777216 with 117047296 nonzeros.
Level 1: The problem is of dimension 2097152 with 14581760 nonzeros.
Level 2: The problem is of dimension 262144 with 1810432 nonzeros.
Level 3: The problem is of dimension 32768 with 223232 nonzeros.
Level 4: The problem is of dimension 4096 with 27136 nonzeros.
Relres BiCGStab: 3.1962e-09
    "Setup time"    "multigrid4"    "1066.7532"
    "Solve time"    "multigrid4"    "676.1995"
    "Wall  time"    "multigrid4"    "1742.9527"
========================================================
    "Pe="    "1"    "Da="    "1"
Relres BiCGStab: 8.7029e-13
    "Setup time"    "none"    "0.00040579"
    "Solve time"    "none"    "15.6945"
    "Wall  time"    "none"    "15.6949"
Relres BiCGStab: 5.7945e-13
    "Setup time"    "jacobi"    "0.84002"
    "Solve time"    "jacobi"    "23.0149"
    "Wall  time"    "jacobi"    "23.855"
Relres BiCGStab: 9.3624e-13
    "Setup time"    "ilu0"    "1.1811"
    "Solve time"    "ilu0"    "11.5938"
    "Wall  time"    "ilu0"    "12.7749"
Level 0: The problem is of dimension 16777216 with 117047296 nonzeros.
Level 1: The problem is of dimension 2097152 with 14581760 nonzeros.
Level 2: The problem is of dimension 262144 with 1810432 nonzeros.
Level 3: The problem is of dimension 32768 with 223232 nonzeros.
Level 4: The problem is of dimension 4096 with 27136 nonzeros.
Relres BiCGStab: 2.1343e-13
    "Setup time"    "multigrid4"    "1067.9724"
    "Solve time"    "multigrid4"    "35.3106"
    "Wall  time"    "multigrid4"    "1103.283"
========================================================
    "Pe="    "1"    "Da="    "100000"
Relres BiCGStab: 3.2639e-15
    "Setup time"    "none"    "0.00046897"
    "Solve time"    "none"    "0.88663"
    "Wall  time"    "none"    "0.8871"
Relres BiCGStab: 3.2644e-15
    "Setup time"    "jacobi"    "0.85752"
    "Solve time"    "jacobi"    "1.188"
    "Wall  time"    "jacobi"    "2.0456"
Relres BiCGStab: 1.6644e-16
    "Setup time"    "ilu0"    "1.1863"
    "Solve time"    "ilu0"    "1.54"
    "Wall  time"    "ilu0"    "2.7263"
Level 0: The problem is of dimension 16777216 with 117047296 nonzeros.
Level 1: The problem is of dimension 2097152 with 14581760 nonzeros.
Level 2: The problem is of dimension 262144 with 1810432 nonzeros.
Level 3: The problem is of dimension 32768 with 223232 nonzeros.
Level 4: The problem is of dimension 4096 with 27136 nonzeros.
Relres BiCGStab: 1.7237e-19
    "Setup time"    "multigrid4"    "1066.9093"
    "Solve time"    "multigrid4"    "2.9189"
    "Wall  time"    "multigrid4"    "1069.8283"
========================================================
    "Pe="    "100000"    "Da="    "1e-05"
Relres BiCGStab: 0.32452
    "Setup time"    "none"    "0.00016189"
    "Solve time"    "none"    "328.4852"
    "Wall  time"    "none"    "328.4853"
Relres BiCGStab: 0.34374
    "Setup time"    "jacobi"    "0.81211"
    "Solve time"    "jacobi"    "547.252"
    "Wall  time"    "jacobi"    "548.0641"
Relres BiCGStab: 2.3246e-15
    "Setup time"    "ilu0"    "0.68417"
    "Solve time"    "ilu0"    "0.7486"
    "Wall  time"    "ilu0"    "1.4328"
Level 0: The problem is of dimension 16777216 with 66912256 nonzeros.
Level 1: The problem is of dimension 2097152 with 8339456 nonzeros.
Level 2: The problem is of dimension 262144 with 1036288 nonzeros.
Level 3: The problem is of dimension 32768 with 128000 nonzeros.
Level 4: The problem is of dimension 4096 with 15616 nonzeros.
Relres BiCGStab: 3.1428e-16
    "Setup time"    "multigrid4"    "1063.4226"
    "Solve time"    "multigrid4"    "2.0014"
    "Wall  time"    "multigrid4"    "1065.424"
========================================================
    "Pe="    "100000"    "Da="    "1"
Relres BiCGStab: 0.33629
    "Setup time"    "none"    "0.00016212"
    "Solve time"    "none"    "325.7292"
    "Wall  time"    "none"    "325.7294"
Relres BiCGStab: 0.25652
    "Setup time"    "jacobi"    "0.83754"
    "Solve time"    "jacobi"    "514.0308"
    "Wall  time"    "jacobi"    "514.8684"
Relres BiCGStab: 3.0279e-15
    "Setup time"    "ilu0"    "0.68122"
    "Solve time"    "ilu0"    "0.69793"
    "Wall  time"    "ilu0"    "1.3792"
Level 0: The problem is of dimension 16777216 with 66912256 nonzeros.
Level 1: The problem is of dimension 2097152 with 8339456 nonzeros.
Level 2: The problem is of dimension 262144 with 1036288 nonzeros.
Level 3: The problem is of dimension 32768 with 128000 nonzeros.
Level 4: The problem is of dimension 4096 with 15616 nonzeros.
Relres BiCGStab: 3.1717e-16
    "Setup time"    "multigrid4"    "1050.696"
    "Solve time"    "multigrid4"    "2.0208"
    "Wall  time"    "multigrid4"    "1052.7168"
========================================================
    "Pe="    "100000"    "Da="    "100000"
Relres BiCGStab: 7.8627e-13
    "Setup time"    "none"    "0.00016308"
    "Solve time"    "none"    "12.0379"
    "Wall  time"    "none"    "12.038"
Relres BiCGStab: 2.5378e-13
    "Setup time"    "jacobi"    "0.8185"
    "Solve time"    "jacobi"    "23.6059"
    "Wall  time"    "jacobi"    "24.4244"
Relres BiCGStab: 2.363e-16
    "Setup time"    "ilu0"    "0.67901"
    "Solve time"    "ilu0"    "0.69698"
    "Wall  time"    "ilu0"    "1.376"
Level 0: The problem is of dimension 16777216 with 66912256 nonzeros.
Level 1: The problem is of dimension 2097152 with 8339456 nonzeros.
Level 2: The problem is of dimension 262144 with 1036288 nonzeros.
Level 3: The problem is of dimension 32768 with 128000 nonzeros.
Level 4: The problem is of dimension 4096 with 15616 nonzeros.
Relres BiCGStab: 2.3303e-16
    "Setup time"    "multigrid4"    "1070.3476"
    "Solve time"    "multigrid4"    "2.0411"
    "Wall  time"    "multigrid4"    "1072.3887"
========================================================