% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function result=problem_1D(N,Pe,e,Da,scheme,solvers)

    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input1D;

    %Fixed input
    problem.phi_w=1;
    problem.phi_e=1;
    problem.x_w=0;
    problem.x_e=1;

    %Variable input
    problem.N_x=N;

    %Work out the other parameters from the input
    %dx=(problem.x_e-problem.x_w)/(problem.N_x-1);

    %L=dx
    L=(problem.x_e-problem.x_w);
    u=Pe/L;
    sl=Da * (u + e / L) / L;
    s=1.0;

    %Set the flux approximation based on the scheme used
    switch scheme
        case "UW"
            problem.flux_fn=@(u,e,dx) flux.UW(u,e,dx);
        case "CD"
            problem.flux_fn=@(u,e,dx) flux.CD(u,e,dx);
        case "HF"
            problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);
        case "CF"
            problem.flux_fn=@(u,e,dx) flux.CF(u,e,dx);
        case "CFCF"
            error('CFCF scheme not defined for 1D')
    end
    %Advection
    problem.u_fn=@(x) u;
    %Diffusion
    problem.e_fn=@(x) e;
    %Constant source
    problem.s_fn=@(x) s;
    %Linear source
    problem.sl_fn=@(x) sl;

    [A,b]=problem.discretize();

    result=solve_problem(A,b,N,u,e,s,sl,scheme,solvers);
end
