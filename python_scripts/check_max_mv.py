'''
Copyright (C) 2019-2022 Eindhoven University of Technology.

This code is free software, you can redistribute it and/or modify it under
the terms of the GNU General Public License; either version 3.0 of the
License, or (at your option) any later version. See LICENSE for details.

This code is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
        The Plasimo Team (https://plasimo.phys.tue.nl)
'''
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import math
import scipy.interpolate
import plotter
from multiprocessing import Pool, cpu_count

plot_index = {
	"algorithm": 0,
	"true_residual": 1,
	"estimated_residual": 2,
	"iterations": 3,
	"return_flag": 4,
	"u": 5,
	"beta": 6,
	"e": 7,
	"time": 8,
	"N": 9,
	"max_iterations": 10,
	"tolerance": 11,
	"S": 12,
	"L": 13,
	"timestamp": 14,
	"scheme": 15,
	"cond":16,
	"s":17,
}

data = np.loadtxt('paper32_geen_precond2.log', delimiter='\t', dtype=object)
data[:, 1:-5] = data[:, 1:-5].astype(float)
print(data)

max_MV=0
max_ix=0
for ix,line in enumerate(data):
    MV=line[plot_index["iterations"]]
    if line[plot_index["algorithm"]]=="matlab_idrs_eye":
        MV=MV*4
    else:
        MV=MV*2
    if line[plot_index["iterations"]]==line[plot_index["max_iterations"]]:
        print("Maximum iterations reached on line: "+str(ix))
    if line[plot_index["return_flag"]]==1:
        if "rubicgstabl" not in line[plot_index["algorithm"]]:
            #Rubicgstabl can also return 1 if the final residual isn't perfect
            #but it has not hit maxit (this happens in case of residual gap)
            print("Solver returned flag 1 (iterated maxit times, but no convergence) on line: "+str(ix))
    if MV>max_MV:
        max_MV=MV
        max_ix=ix
print("Maximum MV found in line: "+str(max_ix)+" namely:" +str(int(max_MV)))
