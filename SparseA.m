% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef SparseA < handle
    %{
    Attempts to create a COO-like format for more easily filling in sparse
    matrices
    %}
    properties
        i=[];
        j=[];
        v=[];
        capacity=0
        entries=0
    end
    methods
        function obj=zero(obj)
            obj.i=[];
            obj.j=[];
            obj.v=[];
            obj.capacity=0;
            obj.entries=0;
            return
        end
        function obj=add(obj,ix_i,ix_j,value)
            %Benchmark has shown that this is the fastest way to append in
            %2019a. Appending is approximately constant time per element.
            obj.i(end+1)=ix_i;
            obj.j(end+1)=ix_j;
            obj.v(end+1)=value;
            return
        end
        function A=assemble(obj)
            %{
            Assemble a sparse matrix from the triplets i,j,v
            %}
            obj.i(isnan(obj.i))=[];
            obj.j(isnan(obj.j))=[];
            obj.v(isnan(obj.v))=[];
            A=sparse(obj.i,obj.j,obj.v);
            return
        end
        function obj=add_p(obj,ix_i,ix_j,value)
            %{
            Adds to a preallocated array
            %}
            obj.entries=obj.entries+1;
            if obj.entries<obj.capacity
                obj.i(obj.entries)=ix_i;
                obj.j(obj.entries)=ix_j;
                obj.v(obj.entries)=value;
            else
                obj.add(ix_i,ix_j,value);
            end
        end
        function obj=preallocate(obj,N)
            %{
            preallocates N triplets
            %}
            obj.zero();
            obj.i=NaN(N,1);
            obj.j=NaN(N,1);
            obj.v=NaN(N,1);
            obj.capacity=N;
            obj.entries=0;
        end
    end

end
