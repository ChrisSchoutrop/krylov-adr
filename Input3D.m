% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef Input3D
    %{
        +X direction: west -> east
        +Y direction: south-> north
        +Z direction: down -> up
        See Fig. 1 in "Discretization and Parallel Iterative Schemes for Advection-Diffusion-Reaction Problems"
    %}
    properties
        %Boundary condition functions
        phi_w_fn; %(y,z)
        phi_e_fn; %(y,z)
        phi_s_fn; %(x,z)
        phi_n_fn; %(x,z)
        phi_d_fn; %(x,y)
        phi_u_fn; %(x,y)

        %Are the boundary conditions Dirichlet? (if they're not, they're Neumann)
        %Note these are functions
        phi_w_dirichlet_fn= @(y,z) true;
        phi_e_dirichlet_fn= @(y,z) true;
        phi_n_dirichlet_fn= @(x,z) true;
        phi_s_dirichlet_fn= @(x,z) true;
        phi_d_dirichlet_fn= @(x,y) true;
        phi_u_dirichlet_fn= @(x,y) true;

        %Coordinates of the boundaries
        x_w;
        x_e;
        y_s;
        y_n;
        z_d;
        z_u;

        %Number of gridpoints
        N_x;
        N_y;
        N_z;

        %Cross-flux coefficient, beta=0=not included, beta=1=fully included
        CFbeta=0.0;
        CFalpha=1.0;

        %Flux approximation function
        flux_fn %@(u,e,ds) flux.CF(u,e,ds);

        %x,y,z-Components advection:
        u_x_fn; %u_x(x,y,z)
        u_y_fn; %u_y(x,y,z)
        u_z_fn; %u_z(x,y,z)

        %Diffusion
        e_fn; %e(x,y,z)

        %Constant source
        s_fn; %s(x,y,z)

        %Linear source
        sl_fn; %sl(x,y,z)
    end
    methods
        function [A,b]=discretize(obj)
            %{
        Discretize such that only the unknowns are computed.
            %}
            %{
            Discretize in a non-fiddly way. by just adding a row that
            contains a single 1 to deal with the boundary conditions.
            %}
            %Grid spacing
            dx = (obj.x_e - obj.x_w) / (obj.N_x - 1);
            dy = (obj.y_n - obj.y_s) / (obj.N_y - 1);
            dz = (obj.z_u - obj.z_d) / (obj.N_z - 1);

            %Prepare A and b
            SA=SparseA;
            SA.preallocate(13*(obj.N_x-2)*(obj.N_y-2)*(obj.N_z-2));
            b=zeros((obj.N_x-2)*(obj.N_y-2)*(obj.N_z-2),1);

            for i=0:obj.N_x-3
                for j=0:obj.N_y-3
                    for k=0:obj.N_z-3

                        %Position of the nodal point
                        x = obj.x_w + i * dx;
                        y = obj.y_s + j * dy;
                        z = obj.z_d + k * dz;

                        if( obj.phi_w_dirichlet_fn(y,z)||obj.phi_e_dirichlet_fn(y,z)||...
                            obj.phi_n_dirichlet_fn(x,z)||obj.phi_s_dirichlet_fn(x,z)||...
                            obj.phi_d_dirichlet_fn(x,y)||obj.phi_u_dirichlet_fn(x,y))==false
                            error('Neumann boundary conditions not implemented, use discretize_ones() instead.')
                        end

                        %Indices of neighbouring cells
                        ix_UC  = (i + 0) + (obj.N_x-2) * (j + 0) + (obj.N_x-2) * (obj.N_y-2) * (k + 1) + 1;
                        ix_DC  = (i + 0) + (obj.N_x-2) * (j + 0) + (obj.N_x-2) * (obj.N_y-2) * (k - 1) + 1;
                        ix_C   = (i + 0) + (obj.N_x-2) * (j + 0) + (obj.N_x-2) * (obj.N_y-2) * (k + 0) + 1;
                        ix_N   = (i + 0) + (obj.N_x-2) * (j + 1) + (obj.N_x-2) * (obj.N_y-2) * (k + 0) + 1;
                        ix_E   = (i + 1) + (obj.N_x-2) * (j + 0) + (obj.N_x-2) * (obj.N_y-2) * (k + 0) + 1;
                        ix_S   = (i + 0) + (obj.N_x-2) * (j - 1) + (obj.N_x-2) * (obj.N_y-2) * (k + 0) + 1;
                        ix_W   = (i - 1) + (obj.N_x-2) * (j + 0) + (obj.N_x-2) * (obj.N_y-2) * (k + 0) + 1;

                        F_CN  = obj.flux_fn(obj.u_y_fn(x + 00, y + dy / 2, z + 00 ), obj.e_fn(x + 00, y + dy / 2, z + 00 ), dy);
                        F_SC  = obj.flux_fn(obj.u_y_fn(x + 00, y - dy / 2, z + 00 ), obj.e_fn(x + 00, y + dy / 2, z + 00 ), dy);
                        F_WC  = obj.flux_fn(obj.u_x_fn(x - dx / 2, y + 00, z + 00 ), obj.e_fn(x - dx / 2, y + 00, z + 00 ), dx);
                        F_CE  = obj.flux_fn(obj.u_x_fn(x + dx / 2, y + 00, z + 00 ), obj.e_fn(x + dx / 2, y + 00, z + 00 ), dx);
                        F_DCC = obj.flux_fn(obj.u_z_fn(x + 00, y + 00, z - dz / 2), obj.e_fn(x + 00, y + 00, z - dz / 2), dz);
                        F_CUC = obj.flux_fn(obj.u_z_fn(x + 00, y + 00, z + dz / 2), obj.e_fn(x + 00, y + 00, z + dz / 2), dz);

                        %Non-cross fluxes:
                        %North flux
                        %SA.add_p(ix_C, ix_S) += 0;
                        SA.add_p(ix_C, ix_C,dx * dz * F_CN(1));
                        if j==obj.N_y-3
                            %North boundary
                            b(ix_C)=b(ix_C)-(dx * dz * F_CN(2))*obj.phi_n_fn(x,z);
                        else
                            SA.add_p(ix_C, ix_N,dx * dz * F_CN(2));
                        end
                        b(ix_C) =b(ix_C)- dy * dx * dz * (F_CN(3) * obj.CFalpha * obj.s_fn(x, y, z) + F_CN(4) * obj.CFalpha * obj.s_fn(x, y + dy, z));
                        %East flux

                        %SA.add_p(ix_C, ix_W) += 0;
                        SA.add_p(ix_C, ix_C,dy * dz * F_CE(1));
                        if i==obj.N_x-3
                            %East boundary
                            b(ix_C)=b(ix_C)-(dy * dz * F_CE(2))*obj.phi_e_fn(y,z);
                        else
                            SA.add_p(ix_C, ix_E,dy * dz * F_CE(2));
                        end
                        b(ix_C) =b(ix_C)- dx * dy * dz * (F_CE(3) * obj.CFalpha * obj.s_fn(x, y, z) + F_CE(4) * obj.CFalpha * obj.s_fn(x + dx, y, z));

                        %South flux
                        if j==0
                            %South boundary
                            b(ix_C)=b(ix_C)-(- dx * dz * F_SC(1))*obj.phi_s_fn(x,z);
                        else
                            SA.add_p(ix_C, ix_S,- dx * dz * F_SC(1));
                        end
                        SA.add_p(ix_C, ix_C,- dx * dz * F_SC(2));
                        %SA.add_p(ix_C, ix_N) -= 0;
                        b(ix_C) =b(ix_C)+ dy * dx * dz * (F_SC(3) * obj.CFalpha * obj.s_fn(x, y - dy, z) + F_SC(4) * obj.CFalpha * obj.s_fn(x, y, z));

                        %West flux
                        if i==0
                            %West boundary
                            b(ix_C)=b(ix_C)-(- dy * dz * F_WC(1))*obj.phi_w_fn(y,z);
                        else
                            SA.add_p(ix_C, ix_W,- dy * dz * F_WC(1));
                        end
                        SA.add_p(ix_C, ix_C,- dy * dz * F_WC(2));
                        %SA.add_p(ix_C, ix_E) -= 0;
                        b(ix_C) =b(ix_C)+ dx * dy * dz * (F_WC(3) * obj.CFalpha * obj.s_fn(x - dx, y, z) + F_WC(4) * obj.CFalpha * obj.s_fn(x, y, z));

                        %Up flux
                        %SA.add_p(ix_C, ix_DC) += 0;
                        SA.add_p(ix_C, ix_C , + dx * dy * F_CUC(1));
                        if k==obj.N_z-3
                            %Up boundary
                            b(ix_C)=b(ix_C)-(+ dx * dy * F_CUC(2))*obj.phi_u_fn(x,y);
                        else
                            SA.add_p(ix_C, ix_UC, + dx * dy * F_CUC(2));
                        end
                        b(ix_C) =b(ix_C)- dz * dx * dy * (F_CUC(3) * obj.CFalpha * obj.s_fn(x, y, z) + F_CUC(4) * obj.CFalpha * obj.s_fn(x, y, z + dz));

                        %Down flux
                        if k==0
                            %Down boundary
                            b(ix_C)=b(ix_C)-(- dx * dy * F_DCC(1))*obj.phi_d_fn(x,y);
                        else
                            SA.add_p(ix_C, ix_DC,- dx * dy * F_DCC(1));
                        end
                        SA.add_p(ix_C, ix_C,- dx * dy * F_DCC(2));
                        %SA.add_p(ix_C, ix_UC) -= 0;
                        b(ix_C) =b(ix_C)+ dz * dx * dy * (F_DCC(3) * obj.CFalpha * obj.s_fn(x, y, z - dz) + F_DCC(4) * obj.CFalpha * obj.s_fn(x, y, z));

                        %Sources
                        %Linear source
                        SA.add_p(ix_C, ix_C,dx * dy * dz * obj.sl_fn(x, y, z));
                        %Other source
                        b(ix_C) =b(ix_C)+ dx * dy * dz * obj.s_fn(x, y, z);

                        if (obj.CFbeta ~= 0.0)
                            error('CFCF Not implemented, use discretize_ones() instead')
                        end

                    end
                end
            end
            A=SA.assemble();
        end
        function phi=assemble_phi(obj,phi_in)
            %{
            Assembles the full vector phi that contains the boundary
            conditions as well, given the solution obtained from A\b
            obtained from discretize().
            Note that discretize_ones() yields the solution vector
            directly, but has different matrix properties, and the
            enforcement of boundary conditions depends on the accuracy of
            the linear solver.
            %}
            phi=NaN(obj.N_x*obj.N_y*obj.N_z,1);

            %Grid spacing
            dx = (obj.x_e - obj.x_w) / (obj.N_x - 1);
            dy = (obj.y_n - obj.y_s) / (obj.N_y - 1);
            dz = (obj.z_u - obj.z_d) / (obj.N_z - 1);

            %Loop over the all cells of the grid
            for i=0:obj.N_x-1
                for j=0:obj.N_y-1
                    for k=0:obj.N_z-1
                        %Position of the nodal point
                        x = obj.x_w + i * dx;
                        y = obj.y_s + j * dy;
                        z = obj.z_d + k * dz;
                        if( obj.phi_w_dirichlet_fn(y,z)||obj.phi_e_dirichlet_fn(y,z)||...
                            obj.phi_n_dirichlet_fn(x,z)||obj.phi_s_dirichlet_fn(x,z)||...
                            obj.phi_d_dirichlet_fn(x,y)||obj.phi_u_dirichlet_fn(x,y))==false
                            error('Neumann boundary conditions not implemented, use discretize_ones() instead.')
                        end
                        %Index on the grid involving all cells
                        ix_C   = (i + 0) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k + 0) + 1;
                        %Index on the grid involving only non-boundary cells
                        ix_C_phi = (i + 0-1) + (obj.N_x-2) * (j + 0-1) + (obj.N_x-2) * (obj.N_y-2) * (k + 0-1) + 1;

                        if i==0
                            %West boundary
                            phi(ix_C)=obj.phi_w_fn(y,z);
                        elseif i==obj.N_y-1
                            %East boundary
                            phi(ix_C)=obj.phi_e_fn(y,z);
                        elseif j==0
                            %South boundary
                            phi(ix_C)=obj.phi_s_fn(x,z);
                        elseif j==obj.N_x-1
                            %North boundary
                            phi(ix_C)=obj.phi_n_fn(x,z);
                        elseif k==0
                            %Down boundary
                            phi(ix_C)=obj.phi_d_fn(x,y);
                        elseif k==obj.N_z-1
                            %Up boundary
                            phi(ix_C)=obj.phi_u_fn(x,y);
                        else
                            phi(ix_C)=phi_in(ix_C_phi);
                        end

                    end
                end
            end

        end
        function [A,b]=discretize_ones(obj)
            %{
            Discretize in a non-fiddly way. by just adding a row that
            contains a single 1 to deal with the boundary conditions.
            %}
            dx = (obj.x_e - obj.x_w) / (obj.N_x - 1);
            dy = (obj.y_n - obj.y_s) / (obj.N_y - 1);
            dz = (obj.z_u - obj.z_d) / (obj.N_z - 1);

            %Prepare A and b
            %Total number of cells
            N=obj.N_x*obj.N_y*obj.N_z;

            %Prepare A and b
            SA=SparseA;
            b=zeros(N,1);
            for i=0:obj.N_x-1
                for j=0:obj.N_y-1
                    for k=0:obj.N_z-1

                        %Indices of the cells
                        ix_UC  = (i + 0) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k + 1) + 1;
                        ix_DC  = (i + 0) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k - 1) + 1;
                        ix_C   = (i + 0) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k + 0) + 1;
                        ix_N   = (i + 0) + obj.N_x * (j + 1) + obj.N_x * obj.N_y * (k + 0) + 1;
                        ix_E   = (i + 1) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k + 0) + 1;
                        ix_S   = (i + 0) + obj.N_x * (j - 1) + obj.N_x * obj.N_y * (k + 0) + 1;
                        ix_W   = (i - 1) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k + 0) + 1;

                        if (obj.CFbeta ~= 0.0)
                            %Needed for the crossfluxes
                            ix_UNW = (i - 1) + obj.N_x * (j + 1) + obj.N_x * obj.N_y * (k + 1) + 1; %#ok<*NASGU>
                            ix_UN  = (i + 0) + obj.N_x * (j + 1) + obj.N_x * obj.N_y * (k + 1) + 1;
                            ix_UNE = (i + 1) + obj.N_x * (j + 1) + obj.N_x * obj.N_y * (k + 1) + 1;
                            ix_UW  = (i - 1) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k + 1) + 1;
                            ix_UE  = (i + 1) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k + 1) + 1;
                            ix_USW = (i - 1) + obj.N_x * (j - 1) + obj.N_x * obj.N_y * (k + 1) + 1;
                            ix_US  = (i + 0) + obj.N_x * (j - 1) + obj.N_x * obj.N_y * (k + 1) + 1;
                            ix_USE = (i + 1) + obj.N_x * (j - 1) + obj.N_x * obj.N_y * (k + 1) + 1;
                            ix_NW  = (i - 1) + obj.N_x * (j + 1) + obj.N_x * obj.N_y * (k + 0) + 1;
                            ix_NE  = (i + 1) + obj.N_x * (j + 1) + obj.N_x * obj.N_y * (k + 0) + 1;
                            ix_SW  = (i - 1) + obj.N_x * (j - 1) + obj.N_x * obj.N_y * (k + 0) + 1;
                            ix_SE  = (i + 1) + obj.N_x * (j - 1) + obj.N_x * obj.N_y * (k + 0) + 1;
                            ix_DNW = (i - 1) + obj.N_x * (j + 1) + obj.N_x * obj.N_y * (k - 1) + 1;
                            ix_DN  = (i + 0) + obj.N_x * (j + 1) + obj.N_x * obj.N_y * (k - 1) + 1;
                            ix_DNE = (i + 1) + obj.N_x * (j + 1) + obj.N_x * obj.N_y * (k - 1) + 1;
                            ix_DW  = (i - 1) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k - 1) + 1;
                            ix_DE  = (i + 1) + obj.N_x * (j + 0) + obj.N_x * obj.N_y * (k - 1) + 1;
                            ix_DSW = (i - 1) + obj.N_x * (j - 1) + obj.N_x * obj.N_y * (k - 1) + 1;
                            ix_DS  = (i + 0) + obj.N_x * (j - 1) + obj.N_x * obj.N_y * (k - 1) + 1;
                            ix_DSE = (i + 1) + obj.N_x * (j - 1) + obj.N_x * obj.N_y * (k - 1) + 1;
                        end

                        %Position of the nodal point
                        x = obj.x_w + i * dx;
                        y = obj.y_s + j * dy;
                        z = obj.z_d + k * dz;

                        %Work out the Dirichlet boundary conditions
                        if (i == 0)
                            %West boundary
                            if (obj.phi_w_dirichlet_fn(y, z))
                                SA.add(ix_C, ix_C ,1);
                                b(ix_C) = obj.phi_w_fn(y, z);
                            else
                                SA.add(ix_C, ix_E,+ 1);
                                SA.add(ix_C, ix_C,- 1);
                                b(ix_C) = dx * obj.phi_w_fn(y, z);
                            end
                        elseif (i == obj.N_x - 1)
                            %East boundary
                            if (obj.phi_e_dirichlet_fn(y, z))
                                SA.add(ix_C, ix_C,1);
                                b(ix_C) = obj.phi_e_fn(y, z);
                            else
                                SA.add(ix_C, ix_C,+ 1);
                                SA.add(ix_C, ix_W,- 1);
                                b(ix_C) = dx * obj.phi_e_fn(y, z);
                            end
                        elseif (j == 0)
                            %South boundary
                            if (obj.phi_s_dirichlet_fn(x, z))
                                SA.add(ix_C, ix_C,1);
                                b(ix_C) = obj.phi_s_fn(x, z);
                            else
                                SA.add(ix_C, ix_N,+1);
                                SA.add(ix_C, ix_C,-1);
                                b(ix_C) = dy * obj.phi_s_fn(x, z);
                            end
                        elseif (j == obj.N_y - 1)
                            %North boundary
                            if (obj.phi_n_dirichlet_fn(x, z))
                                SA.add(ix_C, ix_C,1);
                                b(ix_C) = obj.phi_n_fn(x, z);
                            else
                                SA.add(ix_C, ix_C+ 1);
                                SA.add(ix_C, ix_S- 1);
                                b(ix_C) = dy * obj.phi_n_fn(x, z);
                            end
                        elseif (k == 0)
                            %Down boundary
                            if (obj.phi_d_dirichlet_fn(x, y))
                                SA.add(ix_C, ix_C,1);
                                b(ix_C) = obj.phi_d_fn(x, y);
                            else
                                SA.add(ix_C, ix_UC+ 1);
                                SA.add(ix_C, ix_C- 1);
                                b(ix_C) = dz * obj.phi_d_fn(x, y);
                            end
                        elseif (k == obj.N_z - 1)
                            %Up boundary
                            if (obj.phi_u_dirichlet_fn(x, y))
                                SA.add(ix_C, ix_C,1);
                                b(ix_C) = obj.phi_u_fn(x, y);
                            else
                                SA.add(ix_C, ix_C,1);
                                SA.add(ix_C, ix_DC,-1);
                                b(ix_C) = dz * obj.phi_u_fn(x, y);
                            end
                        else
                            if (obj.CFbeta ~= 0.0)
                                %Needed for the crossfluxes
                                %Up layer,  going to +y
                                F_UWUNW = obj.flux_fn(obj.u_y_fn(x - dx, y + dy / 2, z + dz), obj.e_fn(x - dx, y + dy / 2, z + dz), dy);
                                F_UCUN  = obj.flux_fn(obj.u_y_fn(x + 00, y + dy / 2, z + dz), obj.e_fn(x + 00, y + dy / 2, z + dz), dy);
                                F_UEUNE = obj.flux_fn(obj.u_y_fn(x + dx, y + dy / 2, z + dz), obj.e_fn(x + dx, y + dy / 2, z + dz), dy);
                                F_USWUW = obj.flux_fn(obj.u_y_fn(x - dx, y - dy / 2, z + dz), obj.e_fn(x - dx, y - dy / 2, z + dz), dy);
                                F_USUC  = obj.flux_fn(obj.u_y_fn(x + 00, y - dy / 2, z + dz), obj.e_fn(x + 00, y - dy / 2, z + dz), dy);
                                F_USEUE = obj.flux_fn(obj.u_y_fn(x + dx, y - dy / 2, z + dz), obj.e_fn(x + dx, y - dy / 2, z + dz), dy);

                                %Up layer,  going to +x
                                F_UNWUN = obj.flux_fn(obj.u_x_fn(x - dx / 2, y + dy, z + dz), obj.e_fn(x - dx / 2, y + dy, z + dz), dx);
                                F_UNUNE = obj.flux_fn(obj.u_x_fn(x + dx / 2, y + dy, z + dz), obj.e_fn(x + dx / 2, y + dy, z + dz), dx);
                                F_UWUC  = obj.flux_fn(obj.u_x_fn(x - dx / 2, y + 00, z + dz), obj.e_fn(x - dx / 2, y + 00, z + dz), dx);
                                F_UCUE  = obj.flux_fn(obj.u_x_fn(x + dx / 2, y + 00, z + dz), obj.e_fn(x + dx / 2, y + 00, z + dz), dx);
                                F_USWUS = obj.flux_fn(obj.u_x_fn(x - dx / 2, y - dy, z + dz), obj.e_fn(x - dx / 2, y - dy, z + dz), dx);
                                F_USUSE = obj.flux_fn(obj.u_x_fn(x + dx / 2, y - dy, z + dz), obj.e_fn(x + dx / 2, y - dy, z + dz), dx);

                                %Mid layer, going to +y
                                F_WNW = obj.flux_fn(obj.u_y_fn(x - dx, y + dy / 2, z + 00 ), obj.e_fn(x - dx, y + dy / 2, z + 00 ), dy);
                                F_ENE = obj.flux_fn(obj.u_y_fn(x + dx, y + dy / 2, z + 00 ), obj.e_fn(x + dx, y + dy / 2, z + 00 ), dy);
                                F_SWW = obj.flux_fn(obj.u_y_fn(x - dx, y - dy / 2, z + 00 ), obj.e_fn(x - dx, y + dy / 2, z + 00 ), dy);
                                F_SEE = obj.flux_fn(obj.u_y_fn(x + dx, y - dy / 2, z + 00 ), obj.e_fn(x + dx, y + dy / 2, z + 00 ), dy);

                                %Mid layer, going to +x
                                F_NWN = obj.flux_fn(obj.u_x_fn(x - dx / 2, y + dy, z + 00 ), obj.e_fn(x - dx / 2, y + dy, z + 00 ), dx);
                                F_NNE = obj.flux_fn(obj.u_x_fn(x + dx / 2, y + dy, z + 00 ), obj.e_fn(x + dx / 2, y + dy, z + 00 ), dx);
                                F_SWS = obj.flux_fn(obj.u_x_fn(x - dx / 2, y - dy, z + 00 ), obj.e_fn(x - dx / 2, y - dy, z + 00 ), dx);
                                F_SSE = obj.flux_fn(obj.u_x_fn(x + dx / 2, y - dy, z + 00 ), obj.e_fn(x + dx / 2, y - dy, z + 00 ), dx);

                                %Bot layer, going to +y
                                F_DWDNW = obj.flux_fn(obj.u_y_fn(x - dx, y + dy / 2, z - dz), obj.e_fn(x - dx, y + dy / 2, z - dz), dy);
                                F_DCDN  = obj.flux_fn(obj.u_y_fn(x + 00, y + dy / 2, z - dz), obj.e_fn(x + 00, y + dy / 2, z - dz), dy);
                                F_DEDNE = obj.flux_fn(obj.u_y_fn(x + dx, y + dy / 2, z - dz), obj.e_fn(x + dx, y + dy / 2, z - dz), dy);
                                F_DSWDW = obj.flux_fn(obj.u_y_fn(x - dx, y - dy / 2, z - dz), obj.e_fn(x - dx, y + dy / 2, z - dz), dy);
                                F_DSDC  = obj.flux_fn(obj.u_y_fn(x + 00, y - dy / 2, z - dz), obj.e_fn(x + 00, y + dy / 2, z - dz), dy);
                                F_DSEDE = obj.flux_fn(obj.u_y_fn(x + dx, y - dy / 2, z - dz), obj.e_fn(x + dx, y + dy / 2, z - dz), dy);

                                %Bot layer, going to +x
                                F_DNWDN = obj.flux_fn(obj.u_x_fn(x - dx / 2, y + dy, z - dz), obj.e_fn(x - dx / 2, y + dy, z - dz), dx);
                                F_DNDNE = obj.flux_fn(obj.u_x_fn(x + dx / 2, y + dy, z - dz), obj.e_fn(x + dx / 2, y + dy, z - dz), dx);
                                F_DWDC  = obj.flux_fn(obj.u_x_fn(x - dx / 2, y + 00, z - dz), obj.e_fn(x - dx / 2, y + 00, z - dz), dx);
                                F_DCDE  = obj.flux_fn(obj.u_x_fn(x + dx / 2, y + 00, z - dz), obj.e_fn(x + dx / 2, y + 00, z - dz), dx);
                                F_DSWDS = obj.flux_fn(obj.u_x_fn(x - dx / 2, y - dy, z - dz), obj.e_fn(x - dx / 2, y - dy, z - dz), dx);
                                F_DSDSE = obj.flux_fn(obj.u_x_fn(x + dx / 2, y - dy, z - dz), obj.e_fn(x + dx / 2, y - dy, z - dz), dx);

                                %From bot, going +z
                                F_DNWNW = obj.flux_fn(obj.u_z_fn(x - dx, y + dy, z - dz / 2), obj.e_fn(x - dx, y + dy, z - dz / 2), dz);
                                F_DNN   = obj.flux_fn(obj.u_z_fn(x + 00, y + dy, z - dz / 2), obj.e_fn(x + 00, y + dy, z - dz / 2), dz);
                                F_DNENE = obj.flux_fn(obj.u_z_fn(x + dx, y + dy, z - dz / 2), obj.e_fn(x + dx, y + dy, z - dz / 2), dz);
                                F_DWW   = obj.flux_fn(obj.u_z_fn(x - dx, y + 00, z - dz / 2), obj.e_fn(x - dx, y + 00, z - dz / 2), dz);
                                F_DEE   = obj.flux_fn(obj.u_z_fn(x + dx, y + 00, z - dz / 2), obj.e_fn(x + dx, y + 00, z - dz / 2), dz);
                                F_DSWSW = obj.flux_fn(obj.u_z_fn(x - dx, y - dy, z - dz / 2), obj.e_fn(x - dx, y - dy, z - dz / 2), dz);
                                F_DSS   = obj.flux_fn(obj.u_z_fn(x + 00, y - dy, z - dz / 2), obj.e_fn(x + 00, y - dy, z - dz / 2), dz);
                                F_DSESE = obj.flux_fn(obj.u_z_fn(x + dx, y - dy, z - dz / 2), obj.e_fn(x + dx, y - dy, z - dz / 2), dz);

                                %From mid, going +z
                                F_NWUNW = obj.flux_fn(obj.u_z_fn(x - dx, y + dy, z + dz / 2), obj.e_fn(x - dx, y + dy, z + dz / 2), dz);
                                F_NUN   = obj.flux_fn(obj.u_z_fn(x + 00, y + dy, z + dz / 2), obj.e_fn(x + 00, y + dy, z + dz / 2), dz);
                                F_NEUNE = obj.flux_fn(obj.u_z_fn(x + dx, y + dy, z + dz / 2), obj.e_fn(x + dx, y + dy, z + dz / 2), dz);
                                F_WUW   = obj.flux_fn(obj.u_z_fn(x - dx, y + 00, z + dz / 2), obj.e_fn(x - dx, y + 00, z + dz / 2), dz);
                                F_EUE   = obj.flux_fn(obj.u_z_fn(x + dx, y + 00, z + dz / 2), obj.e_fn(x + dx, y + 00, z + dz / 2), dz);
                                F_SWUSW = obj.flux_fn(obj.u_z_fn(x - dx, y - dy, z + dz / 2), obj.e_fn(x - dx, y - dy, z + dz / 2), dz);
                                F_SUS   = obj.flux_fn(obj.u_z_fn(x + 00, y - dy, z + dz / 2), obj.e_fn(x + 00, y - dy, z + dz / 2), dz);
                                F_SEUSE = obj.flux_fn(obj.u_z_fn(x + dx, y - dy, z + dz / 2), obj.e_fn(x + dx, y - dy, z + dz / 2), dz);
                            end

                            F_CN  = obj.flux_fn(obj.u_y_fn(x + 00, y + dy / 2, z + 00 ), obj.e_fn(x + 00, y + dy / 2, z + 00 ), dy);
                            F_SC  = obj.flux_fn(obj.u_y_fn(x + 00, y - dy / 2, z + 00 ), obj.e_fn(x + 00, y + dy / 2, z + 00 ), dy);
                            F_WC  = obj.flux_fn(obj.u_x_fn(x - dx / 2, y + 00, z + 00 ), obj.e_fn(x - dx / 2, y + 00, z + 00 ), dx);
                            F_CE  = obj.flux_fn(obj.u_x_fn(x + dx / 2, y + 00, z + 00 ), obj.e_fn(x + dx / 2, y + 00, z + 00 ), dx);
                            F_DCC = obj.flux_fn(obj.u_z_fn(x + 00, y + 00, z - dz / 2), obj.e_fn(x + 00, y + 00, z - dz / 2), dz);
                            F_CUC = obj.flux_fn(obj.u_z_fn(x + 00, y + 00, z + dz / 2), obj.e_fn(x + 00, y + 00, z + dz / 2), dz);

                            %Non-cross fluxes:
                            %North flux
                            %SA.add(ix_C, ix_S) += 0;
                            SA.add(ix_C, ix_C,dx * dz * F_CN(1));
                            SA.add(ix_C, ix_N,dx * dz * F_CN(2));
                            b(ix_C) =b(ix_C)- dy * dx * dz * (F_CN(3) * obj.CFalpha * obj.s_fn(x, y, z) + F_CN(4) * obj.CFalpha * obj.s_fn(x, y + dy, z));
                            %East flux
                            %SA.add(ix_C, ix_W) += 0;
                            SA.add(ix_C, ix_C,dy * dz * F_CE(1));
                            SA.add(ix_C, ix_E,dy * dz * F_CE(2));
                            b(ix_C) =b(ix_C)- dx * dy * dz * (F_CE(3) * obj.CFalpha * obj.s_fn(x, y, z) + F_CE(4) * obj.CFalpha * obj.s_fn(x + dx, y, z));
                            %South flux
                            SA.add(ix_C, ix_S,- dx * dz * F_SC(1));
                            SA.add(ix_C, ix_C,- dx * dz * F_SC(2));
                            %SA.add(ix_C, ix_N) -= 0;
                            b(ix_C) =b(ix_C)+ dy * dx * dz * (F_SC(3) * obj.CFalpha * obj.s_fn(x, y - dy, z) + F_SC(4) * obj.CFalpha * obj.s_fn(x, y, z));
                            %West flux
                            SA.add(ix_C, ix_W,- dy * dz * F_WC(1));
                            SA.add(ix_C, ix_C,- dy * dz * F_WC(2));
                            %SA.add(ix_C, ix_E) -= 0;
                            b(ix_C) =b(ix_C)+ dx * dy * dz * (F_WC(3) * obj.CFalpha * obj.s_fn(x - dx, y, z) + F_WC(4) * obj.CFalpha * obj.s_fn(x, y, z));
                            %Up flux
                            %SA.add(ix_C, ix_DC) += 0;
                            SA.add(ix_C, ix_C , + dx * dy * F_CUC(1));
                            SA.add(ix_C, ix_UC, + dx * dy * F_CUC(2));
                            b(ix_C) =b(ix_C)- dz * dx * dy * (F_CUC(3) * obj.CFalpha * obj.s_fn(x, y, z) + F_CUC(4) * obj.CFalpha * obj.s_fn(x, y, z + dz));
                            %Down flux
                            SA.add(ix_C, ix_DC,- dx * dy * F_DCC(1));
                            SA.add(ix_C, ix_C,- dx * dy * F_DCC(2));
                            %SA.add(ix_C, ix_UC) -= 0;
                            b(ix_C) =b(ix_C)+ dz * dx * dy * (F_DCC(3) * obj.CFalpha * obj.s_fn(x, y, z - dz) + F_DCC(4) * obj.CFalpha * obj.s_fn(x, y, z));
                            %Linear source
                            SA.add(ix_C, ix_C,dx * dy * dz * obj.sl_fn(x, y, z));
                            %Other source
                            b(ix_C) =b(ix_C)+ dx * dy * dz * obj.s_fn(x, y, z);

                            if (obj.CFbeta == 0.0)
                                %If obj.CFbeta==0.0 there is no cross flux, we can skip the rest.
                                continue;
                            end

                            %Cross fluxes:
                            %F_CN (+y)
                            %x_{i,j,k}=x_C, GAMMA
                            %df/dx=(F_CE-F_WC)/dx=(alpha_CE*phi_C+beta_CE*phi_E-alpha_WC*phi_W-beta_WC*phi_C)
                            SA.add(ix_C, ix_C,+ (-obj.CFbeta * dy * dx * dz * F_CN(3)) * F_Ce_fn(1) / dx);
                            SA.add(ix_C, ix_E,+ (-obj.CFbeta * dy * dx * dz * F_CN(3)) * F_Ce_fn(2) / dx);
                            SA.add(ix_C, ix_W,- (-obj.CFbeta * dy * dx * dz * F_CN(3)) * F_WC(1) / dx);
                            SA.add(ix_C, ix_C,- (-obj.CFbeta * dy * dx * dz * F_CN(3)) * F_WC(2) / dx);
                            %df/dz=(F_CUC-F_DCC)/dz=(alpha_CUC*phi_C+beta_CUC*phi_UC-alpha_DCC*phi_DC-beta_DCC*phi_C)
                            SA.add(ix_C, ix_C ,+  (-obj.CFbeta * dy * dx * dz * F_CN(3)) * F_CUC(1) / dz);
                            SA.add(ix_C, ix_UC,+  (-obj.CFbeta * dy * dx * dz * F_CN(3)) * F_CUC(2) / dz);
                            SA.add(ix_C, ix_DC,-  (-obj.CFbeta * dy * dx * dz * F_CN(3)) * F_DCC(1) / dz);
                            SA.add(ix_C, ix_C ,-  (-obj.CFbeta * dy * dx * dz * F_CN(3)) * F_DCC(2) / dz);
                            %x_{i,j+1,k}=x_N, DELTA
                            %df/dx=(F_NNE-F_NWN)/dx
                            SA.add(ix_C, ix_N ,+  (-obj.CFbeta * dy * dx * dz * F_CN(4)) * F_NNe_fn(1) / dx);
                            SA.add(ix_C, ix_NE,+  (-obj.CFbeta * dy * dx * dz * F_CN(4)) * F_NNe_fn(2) / dx);
                            SA.add(ix_C, ix_NW,-  (-obj.CFbeta * dy * dx * dz * F_CN(4)) * F_NWN(1) / dx);
                            SA.add(ix_C, ix_N ,-  (-obj.CFbeta * dy * dx * dz * F_CN(4)) * F_NWN(2) / dx);
                            %df/dz=(F_NUN-F_DNN)/dz
                            SA.add(ix_C, ix_N ,+  (-obj.CFbeta * dy * dx * dz * F_CN(4)) * F_NUN(1) / dz);
                            SA.add(ix_C, ix_UN,+  (-obj.CFbeta * dy * dx * dz * F_CN(4)) * F_NUN(2) / dz);
                            SA.add(ix_C, ix_DN,-  (-obj.CFbeta * dy * dx * dz * F_CN(4)) * F_DNN(1) / dz);
                            SA.add(ix_C, ix_N ,-  (-obj.CFbeta * dy * dx * dz * F_CN(4)) * F_DNN(2) / dz);

                            %F_CE (+x)
                            %x_{i,j,k}=x_C
                            %df/dy=(F_CN-F_SC)/dy
                            SA.add(ix_C, ix_C,+ (-obj.CFbeta * dx * dy * dz * F_Ce_fn(3)) * F_CN(1) / dy);
                            SA.add(ix_C, ix_N,+ (-obj.CFbeta * dx * dy * dz * F_Ce_fn(3)) * F_CN(2) / dy);
                            SA.add(ix_C, ix_S,- (-obj.CFbeta * dx * dy * dz * F_Ce_fn(3)) * F_SC(1) / dy);
                            SA.add(ix_C, ix_C,- (-obj.CFbeta * dx * dy * dz * F_Ce_fn(3)) * F_SC(2) / dy);
                            %df/dz=(F_CUC-F_DCC)/dz
                            SA.add(ix_C, ix_C ,+  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(3)) * F_CUC(1) / dz);
                            SA.add(ix_C, ix_UC,+  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(3)) * F_CUC(2) / dz);
                            SA.add(ix_C, ix_DC,-  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(3)) * F_DCC(1) / dz);
                            SA.add(ix_C, ix_C ,-  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(3)) * F_DCC(2) / dz);
                            %x_{i+1,j,k}=x_E
                            %df/dy=(F_ENE-F_SEE)/dy
                            SA.add(ix_C, ix_E ,+  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(4)) * F_ENe_fn(1) / dy);
                            SA.add(ix_C, ix_NE,+  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(4)) * F_ENe_fn(2) / dy);
                            SA.add(ix_C, ix_SE,-  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(4)) * F_SEe_fn(1) / dy);
                            SA.add(ix_C, ix_E ,-  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(4)) * F_SEe_fn(2) / dy);
                            %df/dz=(F_EUE-F_DEE)/dz
                            SA.add(ix_C, ix_E ,+  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(4)) * F_EUe_fn(1) / dz);
                            SA.add(ix_C, ix_UE,+  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(4)) * F_EUe_fn(2) / dz);
                            SA.add(ix_C, ix_DE,-  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(4)) * F_DEe_fn(1) / dz);
                            SA.add(ix_C, ix_E ,-  (-obj.CFbeta * dx * dy * dz * F_Ce_fn(4)) * F_DEe_fn(2) / dz);

                            %F_SC (-y)
                            %x_{i,j-1,k}=x_S
                            %df/dx=(F_SSE-F_SWS)/dx
                            SA.add(ix_C, ix_S ,+  (+obj.CFbeta * dy * dx * dz * F_SC(3)) * F_SSe_fn(1) / dx);
                            SA.add(ix_C, ix_SE,+  (+obj.CFbeta * dy * dx * dz * F_SC(3)) * F_SSe_fn(2) / dx);
                            SA.add(ix_C, ix_SW,-  (+obj.CFbeta * dy * dx * dz * F_SC(3)) * F_SWs_fn(1) / dx);
                            SA.add(ix_C, ix_S ,-  (+obj.CFbeta * dy * dx * dz * F_SC(3)) * F_SWs_fn(2) / dx);
                            %df/dz=(F_SUS-F_DSS)/dz
                            SA.add(ix_C, ix_S ,+  (+obj.CFbeta * dy * dx * dz * F_SC(3)) * F_SUs_fn(1) / dz);
                            SA.add(ix_C, ix_US,+  (+obj.CFbeta * dy * dx * dz * F_SC(3)) * F_SUs_fn(2) / dz);
                            SA.add(ix_C, ix_DS,-  (+obj.CFbeta * dy * dx * dz * F_SC(3)) * F_DSs_fn(1) / dz);
                            SA.add(ix_C, ix_S ,-  (+obj.CFbeta * dy * dx * dz * F_SC(3)) * F_DSs_fn(2) / dz);
                            %x_{i,j,k}=x_C
                            %df/dx=(F_CE-F_WC)/dx
                            SA.add(ix_C, ix_C,+  (+obj.CFbeta * dy * dx * dz * F_SC(4)) * F_Ce_fn(1) / dx);
                            SA.add(ix_C, ix_E,+  (+obj.CFbeta * dy * dx * dz * F_SC(4)) * F_Ce_fn(2) / dx);
                            SA.add(ix_C, ix_W,-  (+obj.CFbeta * dy * dx * dz * F_SC(4)) * F_WC(1) / dx);
                            SA.add(ix_C, ix_C,-  (+obj.CFbeta * dy * dx * dz * F_SC(4)) * F_WC(2) / dx);
                            %df/dz=(F_CUC-F_DCC)/dz
                            SA.add(ix_C, ix_C ,+  (+obj.CFbeta * dy * dx * dz * F_SC(4)) * F_CUC(1) / dz);
                            SA.add(ix_C, ix_UC,+  (+obj.CFbeta * dy * dx * dz * F_SC(4)) * F_CUC(2) / dz);
                            SA.add(ix_C, ix_DC,-  (+obj.CFbeta * dy * dx * dz * F_SC(4)) * F_DCC(1) / dz);
                            SA.add(ix_C, ix_C ,-  (+obj.CFbeta * dy * dx * dz * F_SC(4)) * F_DCC(2) / dz);

                            %F_WC (-x)
                            %x_{i-1,j,k}=x_W
                            %df/dy=(F_WNW-F_SWW)/dy
                            SA.add(ix_C, ix_W ,+  (+obj.CFbeta * dx * dy * dz * F_WC(3)) * F_WNW(1) / dy);
                            SA.add(ix_C, ix_NW,+  (+obj.CFbeta * dx * dy * dz * F_WC(3)) * F_WNW(2) / dy);
                            SA.add(ix_C, ix_SW,-  (+obj.CFbeta * dx * dy * dz * F_WC(3)) * F_SWW(1) / dy);
                            SA.add(ix_C, ix_W ,-  (+obj.CFbeta * dx * dy * dz * F_WC(3)) * F_SWW(2) / dy);
                            %df/dz=(F_WUW-F_DWW)/dz
                            SA.add(ix_C, ix_W ,+  (+obj.CFbeta * dx * dy * dz * F_WC(3)) * F_WUW(1) / dz);
                            SA.add(ix_C, ix_UW,+  (+obj.CFbeta * dx * dy * dz * F_WC(3)) * F_WUW(2) / dz);
                            SA.add(ix_C, ix_DW,-  (+obj.CFbeta * dx * dy * dz * F_WC(3)) * F_DWW(1) / dz);
                            SA.add(ix_C, ix_W ,-  (+obj.CFbeta * dx * dy * dz * F_WC(3)) * F_DWW(2) / dz);
                            %x_{i,j,k}=x_C
                            %df/dy=(F_CN-F_SC)/dy
                            SA.add(ix_C, ix_C,+  (+obj.CFbeta * dx * dy * dz * F_WC(4)) * F_CN(1) / dy);
                            SA.add(ix_C, ix_N,+  (+obj.CFbeta * dx * dy * dz * F_WC(4)) * F_CN(2) / dy);
                            SA.add(ix_C, ix_S,-  (+obj.CFbeta * dx * dy * dz * F_WC(4)) * F_SC(1) / dy);
                            SA.add(ix_C, ix_C,-  (+obj.CFbeta * dx * dy * dz * F_WC(4)) * F_SC(2) / dy);
                            %df/dz=(F_CUC-F_DCC)/dz
                            SA.add(ix_C, ix_C ,+  (+obj.CFbeta * dx * dy * dz * F_WC(4)) * F_CUC(1) / dz);
                            SA.add(ix_C, ix_UC,+  (+obj.CFbeta * dx * dy * dz * F_WC(4)) * F_CUC(2) / dz);
                            SA.add(ix_C, ix_DC,-  (+obj.CFbeta * dx * dy * dz * F_WC(4)) * F_DCC(1) / dz);
                            SA.add(ix_C, ix_C ,-  (+obj.CFbeta * dx * dy * dz * F_WC(4)) * F_DCC(2) / dz);

                            %F_CUC (+z)
                            %x_{i,j,k}=x_C
                            %df/dx=(F_CE-F_WC)/dx
                            SA.add(ix_C, ix_C,+  (-obj.CFbeta * dx * dy * dz * F_CUC(3)) * F_Ce_fn(1) / dx);
                            SA.add(ix_C, ix_E,+  (-obj.CFbeta * dx * dy * dz * F_CUC(3)) * F_Ce_fn(2) / dx);
                            SA.add(ix_C, ix_W,-  (-obj.CFbeta * dx * dy * dz * F_CUC(3)) * F_WC(1) / dx);
                            SA.add(ix_C, ix_C,-  (-obj.CFbeta * dx * dy * dz * F_CUC(3)) * F_WC(2) / dx);
                            %df/dy=(F_CN-F_SC)/dy
                            SA.add(ix_C, ix_C,+  (-obj.CFbeta * dx * dy * dz * F_CUC(3)) * F_CN(1) / dy);
                            SA.add(ix_C, ix_N,+  (-obj.CFbeta * dx * dy * dz * F_CUC(3)) * F_CN(2) / dy);
                            SA.add(ix_C, ix_S,-  (-obj.CFbeta * dx * dy * dz * F_CUC(3)) * F_SC(1) / dy);
                            SA.add(ix_C, ix_C,-  (-obj.CFbeta * dx * dy * dz * F_CUC(3)) * F_SC(2) / dy);
                            %x_{i,j,k+1}=x_UC
                            %df/dx=(F_UCUE-F_UWUC)/dx
                            SA.add(ix_C, ix_UC,+  (-obj.CFbeta * dx * dy * dz * F_CUC(4)) * F_UCUe_fn(1) / dx);
                            SA.add(ix_C, ix_UE,+  (-obj.CFbeta * dx * dy * dz * F_CUC(4)) * F_UCUe_fn(2) / dx);
                            SA.add(ix_C, ix_UW,-  (-obj.CFbeta * dx * dy * dz * F_CUC(4)) * F_UWUC(1) / dx);
                            SA.add(ix_C, ix_UC,-  (-obj.CFbeta * dx * dy * dz * F_CUC(4)) * F_UWUC(2) / dx);
                            %df/dy=(F_UCUN-F_USUC)/dy
                            SA.add(ix_C, ix_UC+  (-obj.CFbeta * dx * dy * dz * F_CUC(4)) * F_UCUN(1) / dy);
                            SA.add(ix_C, ix_UN+  (-obj.CFbeta * dx * dy * dz * F_CUC(4)) * F_UCUN(2) / dy);
                            SA.add(ix_C, ix_US-  (-obj.CFbeta * dx * dy * dz * F_CUC(4)) * F_USUC(1) / dy);
                            SA.add(ix_C, ix_UC-  (-obj.CFbeta * dx * dy * dz * F_CUC(4)) * F_USUC(2) / dy);

                            %F_DCC (-z)
                            %x_{i,j,k-1}=x_DC
                            %df/dx=(F_DCDE-F_DWDC)/dx
                            SA.add(ix_C, ix_DC,+  (+obj.CFbeta * dx * dy * dz * F_DCC(3)) * F_DCDe_fn(1) / dx);
                            SA.add(ix_C, ix_DE,+  (+obj.CFbeta * dx * dy * dz * F_DCC(3)) * F_DCDe_fn(2) / dx);
                            SA.add(ix_C, ix_DW,-  (+obj.CFbeta * dx * dy * dz * F_DCC(3)) * F_DWDC(1) / dx);
                            SA.add(ix_C, ix_DC,-  (+obj.CFbeta * dx * dy * dz * F_DCC(3)) * F_DWDC(2) / dx);
                            %df/dy=(F_DCDN-F_DSDC)/dy
                            SA.add(ix_C, ix_DC,+  (+obj.CFbeta * dx * dy * dz * F_DCC(3)) * F_DCDN(1) / dy);
                            SA.add(ix_C, ix_DN,+  (+obj.CFbeta * dx * dy * dz * F_DCC(3)) * F_DCDN(2) / dy);
                            SA.add(ix_C, ix_DS,-  (+obj.CFbeta * dx * dy * dz * F_DCC(3)) * F_DSDC(1) / dy);
                            SA.add(ix_C, ix_DC,-  (+obj.CFbeta * dx * dy * dz * F_DCC(3)) * F_DSDC(2) / dy);
                            %x_{i,j,k}=x_C
                            %df/dx=(F_CE-F_WC)/dx
                            SA.add(ix_C, ix_C,+  (+obj.CFbeta * dx * dy * dz * F_DCC(4)) * F_Ce_fn(1) / dx);
                            SA.add(ix_C, ix_E,+  (+obj.CFbeta * dx * dy * dz * F_DCC(4)) * F_Ce_fn(2) / dx);
                            SA.add(ix_C, ix_W,-  (+obj.CFbeta * dx * dy * dz * F_DCC(4)) * F_WC(1) / dx);
                            SA.add(ix_C, ix_C,-  (+obj.CFbeta * dx * dy * dz * F_DCC(4)) * F_WC(2) / dx);
                            %df/dy=(F_CN-F_SC)/dy
                            SA.add(ix_C, ix_C,+  (+obj.CFbeta * dx * dy * dz * F_DCC(4)) * F_CN(1) / dy);
                            SA.add(ix_C, ix_N,+  (+obj.CFbeta * dx * dy * dz * F_DCC(4)) * F_CN(2) / dy);
                            SA.add(ix_C, ix_S,-  (+obj.CFbeta * dx * dy * dz * F_DCC(4)) * F_SC(1) / dy);
                            SA.add(ix_C, ix_C,-  (+obj.CFbeta * dx * dy * dz * F_DCC(4)) * F_SC(2) / dy);

                            %{
					        %TODO: The corners USW,UNW,UNE,USE,DSW,DNW,DNE,DSE seem unused, is this correct?
					        The above part is only a 19 point stencil, what is missing?
                            %}

                        end
                    end
                end
            end
            A=SA.assemble();
        end
        function obj=set_CFbeta(obj,beta)
            obj.CFbeta = beta;
            obj.CFalpha = (1 + obj.CFbeta) / 2;
        end
        function obj=set_crossflux(obj,enabled)
            %Enable or disable the crossflux terms in the CF scheme
            %CF with crossflux is referred to as CFCF.
            if (enabled)
                obj.set_CFbeta(1.0);
            else
                obj.set_CFbeta(0.0);
            end
        end
    end
end
