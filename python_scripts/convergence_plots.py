'''
Copyright (C) 2019-2022 Eindhoven University of Technology.

This code is free software, you can redistribute it and/or modify it under
the terms of the GNU General Public License; either version 3.0 of the
License, or (at your option) any later version. See LICENSE for details.

This code is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
        The Plasimo Team (https://plasimo.phys.tue.nl)
'''
from scipy.io import loadmat
from numpy import size
#from matplotlib.pyplot import plot,figure,xlabel,ylabel,show,clf,xlim,legend
import matplotlib.pyplot as plt
import matplotlib
#x = loadmat("convergence_pe1e-5_da1e-5.fig")
#print(x.keys())
#for i in range(0,len(x["hgS_070000"][0][0])):
  #print(x["hgS_070000"][0][0][i])
#     if i=="BiCGStab":
#         print("ok")
# print(x["hgS_070000"][0][0])

#3 Contains the plot data
#print(x["hgS_070000"][0][0][3])
#for i in range(0,len(x["hgS_070000"][0][0][3])):
  #print(x["hgS_070000"][0][0][3][i])

#print(x["hgS_070000"][0][0][3][0][0][3])
font = {'family': 'normal',
        'weight': 'normal',
        'size': 15}
matplotlib.rc('font', **font)

def plotFig(filename, fignr=1):
	f = plt.figure()
	ax = plt.gca()
	d = loadmat(filename+".fig", squeeze_me=True, struct_as_record=False)
	ax1 = d['hgS_070000'].children
	if size(ax1) > 1:
		legs = ax1[1]
		ax1 = ax1[0]
	else:
		legs = 0
	f=plt.figure(fignr)
	plt.clf()
	#plt.hold(True)
	counter = 0
	for line in ax1.children:
		if line.type == 'graph2d.lineseries':
			# if hasattr(line.properties,'Marker'):
			#     mark = "%s" % line.properties.Marker
			#     mark = mark[0]
			# else:
			#     mark = '.'
			# if hasattr(line.properties,'LineStyle'):
			#     linestyle = "%s" % line.properties.LineStyle
			# else:
			#     linestyle = '-'
			# if hasattr(line.properties,'Color'):
			#     r,g,b =  line.properties.Color
			# else:
			#     r = 0
			#     g = 0
			#     b = 1
			# if hasattr(line.properties,'MarkerSize'):
			#     marker_size = line.properties.MarkerSize
			# else:
			#     marker_size = 1
			x = line.properties.XData
			y = line.properties.YData
			#plt.plot(x,y,marker=mark,linestyle=linestyle,markersize=marker_size)
			plt.plot(x, y)
		elif line.type == 'text':
			if counter < 1:
				plt.xlabel("%s" % line.properties.String)
				counter += 1
			elif counter < 2:
				#plt.ylabel("%s" % line.properties.String)
				plt.ylabel("Relative residual")
				counter += 1
	plt.xlim(ax1.properties.XLim)
	if legs:
		leg_entries = tuple(legs.properties.String)
		py_locs = ['upper center', 'lower center', 'right', 'left',
			'upper right', 'upper left', 'lower right', 'lower left', 'best']
		MAT_locs = ['north', 'south', 'east', 'west', 'northeast',
			'northwest', 'southeast', 'southwest', 'best']
		Mat2py = dict(zip(MAT_locs, py_locs))
		location = legs.properties.Location
		leg_entries=list(leg_entries)
		for leg_ix in range(len(leg_entries)):
			if "Matlab" in leg_entries[leg_ix]:
				leg_entries[leg_ix]=leg_entries[leg_ix].replace("Matlab", "MATLAB's")
		print(leg_entries)
		leg_entries=tuple(leg_entries)
		plt.legend(leg_entries, loc=Mat2py[location],fontsize='small', frameon=False)
	plt.yscale('log')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	plt.autoscale(enable=True, axis='y', tight=False)
	#ax.legend(fontsize='small', frameon=False)
	f.savefig(filename+".pdf", bbox_inches='tight')
	#f.savefig(filename+".png", bbox_inches='tight')
	#hold(False)
	#plt.show()


plotFig("convergence_pe1e5_da1e5")
plotFig("convergence_pe1e5_da1e-5")
plotFig("convergence_pe1e-5_da1e5")
plotFig("convergence_pe1e-5_da1e-5")
