% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
close all

%% Test for the matrix of section 2.4
%{
Note that this matrix can be shown to be positive definite in the sense
that
for every x!=0 x'*A*x>0. Since
x'*A*x=(x'*A*x)'=x'*A'*x
if x'*A*x>0 then also 2*x'*A*x>0
therefore iff 2*x'*A*x=x'*A*x+x'*A'*x=x'*(A'+A)*x>0 then x'*A*x>0.
(A'+A) is symmetric with positive eigenvalues, so it is positive definite.
%}

disp('Test for the example from section 2.4')
A=[1 0 0;-1 1 0; 0 -1 1];
N=length(A);

ix=0;
while ix<1e6
    x=(rand(N,1)-0.5)*10;
    xTAx=x'*A*x;
    if xTAx<=0
        error('Not PD')
    end
    ix=ix+1;
    if mod(ix,1e6)==0
        disp(['ix: ',num2str(ix)])
    end
end

%% Test for the 1D problem

disp('Test for the example for the 1D problem')
for ix2=1:100
N=randi([10,200]);
Pe=rand()*10^((rand()-0.5)*10);
e=1;
Da=rand()*10^((rand()-0.5)*10);
%Set up the problem for the ADR equation
%div(u*phi-e*grad(phi))=s-sl*phi
problem=Input1D;
problem.N_x=N;

%Fixed input
problem.phi_w=1;
problem.phi_e=1;
problem.x_w=0;
problem.x_e=1;

%Variable input
problem.N_x=N;


%Work out the other parameters from the input
dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
u=Pe/dx;
sl=Da*(1/dx^2);
s=0.0;

%Set the flux approximation based on the scheme used
problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

%Advection
problem.u_fn=@(x) u;
%Diffusion
problem.e_fn=@(x) e;
%Constant source
problem.s_fn=@(x) s;
%Linear source
problem.sl_fn=@(x) sl;

[A,b]=problem.discretize();
N=length(A);

ix=0;
max_x=10*max(max(A));

while ix<1e4
    x=(rand(N,1)-0.5)*max_x;
    xTAx=x'*A*x;
    if xTAx<=0
        error('Not PD')
    end
    ix=ix+1;
    if mod(ix,1e3)==0
        disp(['ix: ',num2str(ix)])
    end
end
end

%% Test for the 2D problem

disp('Test for the example for the 2D problem')
for ix2=1:100
N=randi([10,200]);
Pe=rand()*10^((rand()-0.5)*10);
e=1;
Da=rand()*10^((rand()-0.5)*10);
%Set up the problem for the ADR equation
%div(u*phi-e*grad(phi))=s-sl*phi
problem=Input2D;

%Fixed input
problem.phi_w_fn=@(y) 1;
problem.phi_e_fn=@(y) 0;
problem.phi_s_fn=@(x) 0;
problem.phi_n_fn=@(x) 1;
problem.x_w=0;
problem.x_e=1;
problem.y_s=0;
problem.y_n=1;

%Variable input
problem.N_x=N;
problem.N_y=N;


%Work out the other parameters from the input
dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
u=Pe/dx;
sl=Da*(1/dx^2);
s=0.0;

%Set the flux approximation based on the scheme used
problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

%Advection
problem.u_x_fn=@(x,y) u/sqrt(2);
problem.u_y_fn=@(x,y) u/sqrt(2);
%Diffusion
problem.e_fn=@(x,y) e;
%Constant source
problem.s_fn=@(x,y) s;
%Linear source
problem.sl_fn=@(x,y) sl;

[A,b]=problem.discretize();
N=length(A);

ix=0;
max_x=10*max(max(A));
while ix<1e4
    x=(rand(N,1)-0.5)*max_x;
    xTAx=x'*A*x;
    if xTAx<=0
        error('Not PD')
    end
    ix=ix+1;
    if mod(ix,1e3)==0
        disp(['ix: ',num2str(ix)])
    end
end
end

%% Test for the 3D problem
disp('Test for the 3D problem')
for ix2=1:100
N=randi([10,200]);
Pe=rand()*10^((rand()-0.5)*10);
e=1;
Da=rand()*10^((rand()-0.5)*10);
%Set up the problem for the ADR equation
%div(u*phi-e*grad(phi))=s-sl*phi
problem=Input3D;

%Fixed input
problem.phi_w_fn=@(y,z) 1;
problem.phi_e_fn=@(y,z) 0;
problem.phi_s_fn=@(x,z) 0;
problem.phi_n_fn=@(x,z) 1;
problem.phi_u_fn=@(x,y) 0;
problem.phi_d_fn=@(x,y) 1;
problem.x_w=0;
problem.x_e=1;
problem.y_s=0;
problem.y_n=1;
problem.z_d=0;
problem.z_u=1;

%Variable input
problem.N_x=N;
problem.N_y=N;
problem.N_z=N;

%Work out the other parameters from the input
dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
u=Pe/dx;
sl=Da*(1/dx^2);
s=0.0;
%sl=0.0;
%s=Da * (u + e / dx) / dx;

%Set the flux approximation based on the scheme used
problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

%Advection
problem.u_x_fn=@(x,y,z) u/sqrt(3);
problem.u_y_fn=@(x,y,z) u/sqrt(3);
problem.u_z_fn=@(x,y,z) u/sqrt(3);
%Diffusion
problem.e_fn=@(x,y,z) e;
%Constant source
problem.s_fn=@(x,y,z) s;
%Linear source
problem.sl_fn=@(x,y,z) sl;
[A,b]=problem.discretize();
N=length(A);

ix=0;
max_x=10*max(max(A));
while ix<1e4
    x=(rand(N,1)-0.5)*max_x;
    xTAx=x'*A*x;
    if xTAx<=0
        error('Not PD')
    end
    ix=ix+1;
    if mod(ix,1e3)==0
        disp(['ix: ',num2str(ix)])
    end
end
end