% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
%close all

problem=Input1D;
problem.phi_w=1;
problem.phi_e=1;
problem.x_w=0;
problem.x_e=1;
problem.N_x=321;

dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
e=1;
Pe=1;
Da=1e-5;

L=1;
u=Pe/L;
sl=-Da * (u + e / L) / L;


problem.flux_fn=@(u,e,dx) flux.CD(u,e,dx);
problem.u_fn=@(x) u;
problem.e_fn=@(x) e;
problem.s_fn=@(x) 1;
problem.sl_fn=@(x) sl;

[A,b]=problem.discretize();

figure(1)
clf
plot(linspace(problem.x_w,problem.x_e,problem.N_x),A\b);
xlabel('x')
ylabel('phi')

D=eig_toep(A(5,4),A(5,5),A(5,6),length(A));
ev=diag(D);

figure(2)
clf
scatter(real(ev),imag(ev))
xlabel('Re')
ylabel('Im')
