% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
close all
warning('off','all')
clc

%Set the RNG seed for reproducibility
rng(0,'twister')

%Output filename
output_filename='paper32_no_precond_withgmres.log';

%Solvers to use
solver_vec=[
"matlab_lmr_eye",...
"matlab_gmres1_eye","matlab_gmres10_eye","matlab_gmres50_eye",...
"matlab_gmres100_eye","matlab_gmres250_eye",...
"matlab_bicgstab_eye",...
"matlab_idrs_eye",...
"matlab_bicgstabl_eye",...
"matlab_rubicgstablv1_L1_eye","matlab_rubicgstablv2_L1_eye",...
"matlab_rubicgstablv3_L1_eye","matlab_rubicgstablv4_L1_eye",...
"matlab_rubicgstablv5_L1_eye","matlab_rubicgstablv6_L1_eye",...
"matlab_rubicgstablv7_L1_eye","matlab_rubicgstabl_L1_eye",...
];

%Discretization scheme;
%UW=Upwind
%CD=Central Differencing
%HF=Homogeneous Flux (Exponential scheme)
%CF=Complete flux scheme without crossflux
%CFCF=Complete flux+crossflux
scheme_vec=["HF"];

%Range of parameters to test
Pe_vec=logspace(-6,6,32);
Da_vec=logspace(-6,6,32);
e_vec=[1];
N_vec=[101];

%Contains the output
estimated_results=length(N_vec)*length(Pe_vec)*length(Da_vec)*length(scheme_vec)*length(solver_vec);
results=cell(length(N_vec)*length(Pe_vec)*length(Da_vec)*length(scheme_vec)*length(solver_vec),19);

ix_results=1;
for ix_N=1:length(N_vec)
    N=N_vec(ix_N);
    for ix_Pe=1:length(Pe_vec)
        Pe=Pe_vec(ix_Pe);
        for ix_Da=1:length(Da_vec)
            Da=Da_vec(ix_Da);
            for ix_e=1:length(e_vec)
                e=e_vec(ix_e);
                for ix_scheme=1:length(scheme_vec)
                    scheme=scheme_vec(ix_scheme);

                    %Set up and solve the problems
                    posixtime(datetime('now'))
                    %result=problem_1D(N,Pe,e,Da,scheme,solver_vec);
                    %result=problem_2D(N,Pe,e,Da,scheme,solver_vec);
                    result=problem_3D(N,Pe,e,Da,scheme,solver_vec);

                    %Save result into a cell-array
                    for ix_cell_col=ix_results:ix_results+length(solver_vec)-1
                        %"results{ix_results:ix_results+length(solver_vec),:}=result;"
                        for ix_cell_row=1:size(results,2)
                            results{ix_cell_col,ix_cell_row}=result{ix_cell_col-ix_results+1,ix_cell_row};
                        end
                    end
                    ix_results=ix_results+length(solver_vec);
                    disp(['Progress: ',num2str(100*(ix_results-1)/estimated_results),' %'])

                    %Save the intermediate result to file
                    output_file = fopen(output_filename,'w');
                    for ix_col=1:size(results,1)
                        for ix_row=1:size(results,2)
                            fprintf(output_file,'%s\t',results{ix_col,ix_row});
                        end
                        fprintf(output_file,'\n');
                    end
                    fclose(output_file);

                end %ix_scheme=1:length(scheme_vec)
            end %ix_e=1:length(e_vec)
        end %ix_Da=1:length(Da_vec)
    end %ix_Pe=1:length(Pe_vec)
end %ix_N=1:length(N_vec)

%Reset
warning('on','all')
