clear all
close all
multigridLevels=3;

%{
Define the number of points in each dimension, X, Y, and Z, of the finest
grid. The preconditioning method requires that the number of points in this
grid be divisible by 2^multigridLevels. In this case, the number of points
must be divisible by 4, because the number of multigrid levels is 2.
%}

N_inner_points=80;

numPoints.X = N_inner_points;
numPoints.Y = N_inner_points;
numPoints.Z = N_inner_points;

numberOfSmootherSteps = 1;

N=numPoints.X+2;
problem=Input3D;
Pe=1e+5;
Da=1e-5;
disp(["Pe=",num2str(Pe)," Da=",num2str(Da)])

dx=1./(N-1);

L=dx;
u=Pe/L;
e=1.0;
sl=Da*(1/L^2);
s=0.0;

u_y=u/sqrt(3);
u_x=u_y;
u_z=u_y;

problem.phi_w_fn=@(y,z) 1;
problem.phi_e_fn=@(y,z) 0;
problem.phi_s_fn=@(x,z) 0;
problem.phi_n_fn=@(x,z) 1;
problem.phi_u_fn=@(x,y) 0;
problem.phi_d_fn=@(x,y) 1;
problem.x_w=0;
problem.x_e=1;
problem.y_s=0;
problem.y_n=1;
problem.z_d=0;
problem.z_u=1;

problem.N_x=N;
problem.N_y=N;
problem.N_z=N;

problem.u_x_fn=@(x,y,z) u_x;
problem.u_y_fn=@(x,y,z) u_y;
problem.u_z_fn=@(x,y,z) u_z;
problem.e_fn=@(x,y,z) e;
problem.s_fn=@(x,y,z) s;
problem.sl_fn=@(x,y,z) sl;

problem.flux_fn=@(u,e,ds) flux.HF(u,e,ds);

tic
disp("discretizing")
[A,b]=problem.discretize();
toc
tic
disp("Setting up multigrid hierarchy")
multigridData = discretizeADREquation(numPoints,multigridLevels,numberOfSmootherSteps,problem,A);
toc
tic
disp("Setting up the preconditioner itself")
preconditioner = setupPreconditioner(multigridData);
toc

tol = 1e-12;
maxit = 2000;
%x = pcg(A,b,tol,maxit,preconditioner);
tic
disp("Solving with multigrid preconditioner")
x_mg = bicgstab(A,b,tol,maxit,preconditioner);
toc
tic
tic
disp("Solving without preconditioner")
x_none = bicgstab(A,b,tol,maxit);
toc
norm(x_mg-x_none)/norm(x_none)

function x = multigridPreconditioner(mgData,r,level)

if(level < mgData(level).MaxLevel)
    x = zeros(size(r),'like',r);

    % Presmooth using Gauss-Seidel
    for i=1:mgData(level).NumberOfSmootherSteps
        x = mgData(level).Matrices.LD \ (-mgData(level).Matrices.U*x + r);
        x = mgData(level).Matrices.DU \ (-mgData(level).Matrices.L*x + r);
    end

    % Compute residual on a coarser level
    Axf = mgData(level).Matrices.A*x;
    rc = r(mgData(level).Fine2CoarseOperator)- Axf(mgData(level).Fine2CoarseOperator);

    % Recursive call until coarsest level is reached
    xc = multigridPreconditioner(mgData, rc, level+1);

    % Update solution with prolonged coarse grid solution
    x(mgData(level).Fine2CoarseOperator) = x(mgData(level).Fine2CoarseOperator)+xc;

    % Postsmooth using Gauss-Seidel
    for i = 1:mgData(level).NumberOfSmootherSteps
        x = mgData(level).Matrices.LD \ (-mgData(level).Matrices.U*x + r);
        x = mgData(level).Matrices.DU \ (-mgData(level).Matrices.L*x + r);
    end
else
    % Obtain exact solution on the coarsest level
    x = mgData(level).Matrices.A \ r;
end

end

function preconditioner = setupPreconditioner(multigridData)

if ~isempty(multigridData)
    preconditioner = @(x,varargin) multigridPreconditioner(multigridData,x,1);
else
    preconditioner = [];
end

end





















