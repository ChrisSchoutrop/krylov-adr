% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
close all

problem=Input3D;
problem.phi_w_fn=@(y,z) 0;
problem.phi_e_fn=@(y,z) 1;
problem.phi_s_fn=@(x,z) 0;
problem.phi_n_fn=@(x,z) 1;
problem.phi_u_fn=@(x,y) 0;
problem.phi_d_fn=@(x,y) 1;

problem.x_w=0;
problem.x_e=1;
problem.y_s=0;
problem.y_n=1;
problem.z_d=0;
problem.z_u=1;
problem.N_x=80;
problem.N_y=80;
problem.N_z=80;

problem.flux_fn=@(u,e,ds) flux.CF(u,e,ds);
problem.u_x_fn=@(x,y,z) 1.0;
problem.u_y_fn=@(x,y,z) 1.0;
problem.u_z_fn=@(x,y,z) 1.0;
problem.e_fn=@(x,y,z) 1.0;
problem.s_fn=@(x,y,z) 1.0;
problem.sl_fn=@(x,y,z) 1.0;

tic
[A,b]=problem.discretize();
toc

tic
A\b;
toc

tic
bicgstab(A,b,1e-12,2000);
toc
