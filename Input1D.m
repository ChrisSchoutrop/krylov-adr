% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef Input1D
    %{
        +X direction: west -> east
        +Y direction: south-> north
        +Z direction: down -> up
        See Fig. 1 in "Discretization and Parallel Iterative Schemes for Advection-Diffusion-Reaction Problems"
    %}
    properties
        %Boundary condition values
        phi_w;  %West
        phi_e;   %East

        %Are the boundary conditions Dirichlet? If they're not, they're
        %Neumann
        phi_w_dirichlet = true;
        phi_e_dirichlet = true;

        %Coordinates of the boundaries
        x_w;
        x_e;

        %Number of gridpoints in the x-direction
        N_x

        %Flux approximation function
        flux_fn %@(u,e,ds) flux.CF(u,e,ds);

        %x-Component advection(x):
        u_fn; %u(x)

        %Diffusion (x)
        e_fn; %e(x)

        %Source (x)
        s_fn; %s(x)

        %Linear source(x)
        sl_fn; %sl(x)
    end
    methods
        function [A,b]=discretize(obj)
            %{
            Discretize the ADR equation
            %}
            dx=(obj.x_e - obj.x_w) / (obj.N_x - 1);

            %Prepare A and b
            SA=SparseA;
            b=zeros(obj.N_x-2,1);

            if(obj.phi_w_dirichlet || obj.phi_e_dirichlet)==false
                error('Neumann boundary conditions not implemented, use discretize_ones instead')
            end

            %Construct A and b
            for i=1:obj.N_x-2 %Non-boundary cells
                %Coordinate of current center cell
                x=obj.x_w+(i)*dx;

                %Evaluate fluxes at the edges of the cell
                F_right = obj.flux_fn(obj.u_fn(x + 0.5 * dx), obj.e_fn(x + 0.5 * dx), dx);
                F_left = obj.flux_fn(obj.u_fn(x - 0.5 * dx), obj.e_fn(x - 0.5 * dx), dx);

                %Conservation law: F_right-F_left=dx*(s-sl*phi)
                %F_left
                if i~=1
                    SA.add(i,i-1,-F_left(1));    %alpha
                end
                SA.add(i,i,-F_left(2));    %beta
                %SA.add(i,i+1)=SA.add(i,i+1)-0;

                %F_right
                %SA.add(i,i-1)=SA.add(i,i-1)+0;
                SA.add(i,i,F_right(1)); %alpha
                if i~=obj.N_x-2
                    SA.add(i,i+1,+F_right(2)); %beta
                end

                %Linear source
                SA.add(i,i,dx*obj.sl_fn(x));

                %F_left
                b(i)=b(i)-dx*(-F_left(3) * obj.s_fn(x - dx) + -F_left(4) * obj.s_fn(x));
                %F_right
                b(i)=b(i)-dx*(F_right(3) * obj.s_fn(x) + F_right(4) * obj.s_fn(x + dx));
                %Source
                b(i)=b(i)+dx*obj.s_fn(x);

                if i==1
                    b(i)=b(i)--F_left(1)*obj.phi_w;
                elseif i==obj.N_x-2
                    b(i)=b(i)-+F_right(2)*obj.phi_e;
                end

            end
            A=SA.assemble();
        end
        function phi=assemble_phi(obj,phi_in)
            %{
            Assembles the full vector phi that contains the boundary
            conditions as well, given the solution obtained from A\b
            obtained from discretize().
            Note that discretize_ones() yields the solution vector
            directly, but has different matrix properties, and the
            enforcement of boundary conditions depends on the accuracy of
            the linear solver.
            %}
            phi=[obj.phi_w;phi_in;obj.phi_e];
        end
        function [A,b]=discretize_ones(obj)
            %{
            Same discretization, but resolves the boundary conditions by
            adding a row which contains a 1. Solving A\b yields the entire
            phi-vector as solution, no need to add the boundaries in later.
            %}
            dx=(obj.x_e - obj.x_w) / (obj.N_x - 1);

            %Prepare A and b
            SA=SparseA;
            b=zeros(obj.N_x,1);

            %West boundary condition
            if(obj.phi_w_dirichlet)
                SA.add(1,1,+1);
                b(1)=obj.phi_w; %phi(x=x_w)
            else
                %{
                Estimate Neumann BC using finite difference
                Note this is first order, but is exact for homogeneous Neumann
                Implementing ghost points, CD estimate for BC seems like a
                nightmare to implement at this stage.
                Q: Would it be possible to just use a higher order
                forward/backward differences?
                %}
                SA.add(1,1,-1);
                SA.add(1,2,+1);
                b(1) = dx * obj.phi_w;
            end

            %East boundary condition
            if(obj.phi_e_dirichlet)
                SA.add(obj.N_x,obj.N_x,+1);
                b(obj.N_x)=obj.phi_e; %phi(x=x_e)
            else
                SA.add(obj.N_x, obj.N_x,+1);
                SA.add(obj.N_x, obj.N_x-1,-1);
                b(obj.N_x) = dx * obj.phi_e;
            end

            %Construct A and b
            for i=2:obj.N_x-1 %Non-boundary cells
                %Coordinate of current center cell
                x=obj.x_w+(i-1)*dx;

                %Evaluate fluxes at the edges of the cell
                F_right = obj.flux_fn(obj.u_fn(x + 0.5 * dx), obj.e_fn(x + 0.5 * dx), dx);
                F_left = obj.flux_fn(obj.u_fn(x - 0.5 * dx), obj.e_fn(x - 0.5 * dx), dx);

                %Conservation law: F_right-F_left=dx*(s-sl*phi)
                %F_left
                SA.add(i,i-1,-F_left(1));    %alpha
                SA.add(i,i,-F_left(2));    %beta
                %SA.add(i,i+1)=SA.add(i,i+1)-0;

                %F_right
                %SA.add(i,i-1)=SA.add(i,i-1)+0;
                SA.add(i,i,F_right(1)); %alpha
                SA.add(i,i+1,+F_right(2)); %beta

                %Linear source
                SA.add(i,i,dx*obj.sl_fn(x));

                %F_left
                b(i)=b(i)-dx*(-F_left(3) * obj.s_fn(x - dx) + -F_left(4) * obj.s_fn(x));
                %F_right
                b(i)=b(i)-dx*(F_right(3) * obj.s_fn(x) + F_right(4) * obj.s_fn(x + dx));
                %Source
                b(i)=b(i)+dx*obj.s_fn(x);
            end
            A=SA.assemble();
        end

    end
end



























