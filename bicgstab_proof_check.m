% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)

clear all
close all
clc
%{
Observations:
tridiag(-1,1,0), b=e1 leads to a NaN result
tridiag(-1,1,eps), b=e1 does lead to extremely small rho, however the
system does eventually converge. With random rtilde this takes 11
iterations, with rtilde=r0 this takes 18 for M=10. After the 18th iteration
rtilde'*rhat(:,1): "    "6.5843e-108".
Note that for such a small value the Eigen-implmentation would have chosen
a new rtilde.

This suggests that indeed the boundary between converging and
not-converging is very sharp. However, for matrices close to bidiagonal
choosing random rtilde does seem beneficial for the number of iterations
required.

-> More than just "small rtilde" seems to be required to completely
stagnate. However the number of iterations exceeds the dimension of the
matrix, at which point one might as well use LU.

tridiag(-1,1,-11*eps) converges, k=22
tridiag(-1,1,-10*eps) converges, k=17
tridiag(-1,1,-9*eps)  converges, k=20
tridiag(-1,1,-8*eps)  stagnates
tridiag(-1,1,-7*eps)  converges, k=20
tridiag(-1,1,-6*eps)  converges, k=18
tridiag(-1,1,-5*eps)  converges, k=19
tridiag(-1,1,-4*eps)  stagnates
tridiag(-1,1,-3*eps)  converges, k=17
tridiag(-1,1,-2*eps)  stagnates
tridiag(-1,1,-1*eps)  converges, k=20
tridiag(-1,1,0*eps)   stagnates
tridiag(-1,1,1*eps)   converges, k=18
tridiag(-1,1,2*eps)   stagnates
tridiag(-1,1,3*eps)   stagnates
tridiag(-1,1,4*eps)   stagnates
tridiag(-1,1,5*eps)   stagnates
tridiag(-1,1,6*eps)   converges, k=19
tridiag(-1,1,7*eps)   converges, k=20
tridiag(-1,1,8*eps)   stagnates
tridiag(-1,1,9*eps)   stagnates
tridiag(-1,1,10*eps)  converges, k=19
tridiag(-1,1,11*eps)  converges, k=19
%}

%Size of the linear system
M=10;

%A=tridiag(a,b,c)
a=-1;   %Subdiagonal
b=1;    %Diagonal
c=0;    %Superdiagonal

%Matrix
A = diag(b*ones(1,M)) + diag(c*ones(1,M-1),1) + diag(a*ones(1,M-1),-1);
%RHS
b=zeros(M,1);
b(1)=1;

vbicgstab(A,b,zeros(size(b)))

function [x,r]=vbicgstab(A,b,x)
%{
Vanilla implementation of BiCGStab
%}
k=0
M=length(b);
rho0=1;
alpha=1;
omega=1;
rhat=zeros(M,2);
uhat=zeros(M,2);
rhat(:,1)=b-A*x;
rtilde=rhat(:,1);
%rtilde=rand(size(rhat(:,1)));
xp=x;
x=zeros(size(x));
zeta=norm(rhat(:,1));

tol=1e-12;
while zeta>tol*norm(b)
    rho0=-rho0*omega;
    rho1=rtilde'*rhat(:,1);
    beta=alpha*(rho1/rho0);
    rho0=rho1;
    uhat(:,1)=rhat(:,1)-beta*uhat(:,1);
    uhat(:,2)=A*uhat(:,1);
    alpha=rho1/(rtilde'*uhat(:,2));
    rhat(:,1)=rhat(:,1)-alpha*uhat(:,2);
    rhat(:,2)=A*rhat(:,1);
    x=x+alpha*uhat(:,1);
    omega=(rhat(:,2)'*rhat(:,1))/(rhat(:,2)'*rhat(:,2));
    x=x+omega*rhat(:,1);
    rhat(:,1)=rhat(:,1)-omega*rhat(:,2);
    uhat(:,1)=uhat(:,1)-omega*uhat(:,2);
    zeta=norm(rhat(:,1));
    k=k+1
end
x=x+xp;
disp(['Iters: ',num2str(k)," rtilde'*rhat(:,1): ",num2str(rtilde'*rhat(:,1))])
disp(['estimated residual: ',num2str(zeta/norm(b)),' true residual: ',num2str(norm(b-A*x)/norm(b))])

end
