% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function [x,flag,relres,iter,resvec]=lmr(A,b,tol,maxit,M1,M2,x0)
%{
"Local Minimal Residual" LMR aims to solve the linear system Ax=b. It uses
the current residual as a search direction, and makes a step such that the
next residual is locally minimal.

Note that this is equivalent to gmres(1), or the "polynomial"/"minimal
residual" step of BiCGStab.
This can also be seen as Richardson iteration with variable step size.
%}

N=length(b);

% Assign default values to unspecified parameters
if nargin < 3 || isempty(tol)
    tol = 1e-6;
end
if tol < eps
    warning('tooSmallTolerance');
    tol = eps;
elseif tol >= 1
    warning('tooBigTolerance');
    tol = 1-eps;
end
if nargin < 4 || isempty(maxit)
    maxit = min(N,20);
end
maxit = max(maxit, 0);

%Check if a preconditioner was defined
if ((nargin >= 5) && ~isempty(M1))
    existM1 = 1;
else
    existM1 = 0;
end
if ((nargin >= 6) && ~isempty(M2))
    existM2 = 1;
else
    existM2 = 0;
end

%Check if an initial guess was given
if ((nargin >= 7) && ~isempty(x0))==false
    x0=zeros(size(b));
end

%m=20000;
resvec=[];
r = b-A*x0;
x=zeros(size(x0)); %This will contain the updates to x0

normb=norm(b);
normr=norm(r);
resvec(end+1)=normr;
iter=0;

while normr>normb*tol && iter<maxit

    %Ar=A*r;
    if existM1 && existM2
        Ar = A*(M2\(M1\r));
    elseif existM1
        Ar = A*(M1\r);
    elseif existM2
        Ar = A*(M2\r);
    else
        Ar = A*r;
    end
    alpha=Ar'*r/(Ar'*Ar);
    x=x+alpha*r;
    r=r-alpha*Ar;
    normr=norm(r);
    resvec(end+1)=normr;

    iter=iter+1;
end

if existM1 && existM2
    x = (M2\(M1\x));
elseif existM1
    x = (M1\x);
elseif existM2
    x = (M2\x);
else
    x = x;
end

x=x0+x;

relres=norm(b-A*x)/normb;

if normr<normb*tol
    flag=0;
else
    flag=1;
end

end
