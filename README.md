# krylov adr

Supplementary Matlab code used to obtain the results present in
Assessment of Krylov subspace implementations for the advection-diffusion-reaction equation

main.m is the entry point for this code. Numerical experiments were conducted in Matlab 2019a.
