% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
close all
warning('off','all')
clc

%Set the RNG seed for reproducibility
rng(0,'twister')
diary('compare_pc.log')
%{
Setup
%}
N_inner_points=256

%plot_cells{1} =["none","jacobi","ilu0"];
plot_cells{1} =["none","jacobi","ilu0","multigrid4"];
%plot_cells{1}=["none","jacobi","row_sum","col_sum","tridiag"];
%plot_cells{2}=["multigrid1","multigrid2","multigrid3","multigrid4"];
%plot_cells{3}=["lower_triangular","upper_triangular","ilu0"];

Pe_vec=[1e-5,1,1e5];
Da_vec=[1e-5,1,1e5];

%Pe_vec=[1e5];
%Da_vec=[1e-5];

for ix_Pe=1:length(Pe_vec)
    for ix_Da=1:length(Da_vec)
        close all
        Pe=Pe_vec(ix_Pe);
        Da=Da_vec(ix_Da);
        disp(["Pe=",num2str(Pe)," Da=",num2str(Da)])

        numPoints.X = N_inner_points;
        numPoints.Y = N_inner_points;
        numPoints.Z = N_inner_points;
        numberOfSmootherSteps = 1;

        N=numPoints.X+2;

        %Set up the problem for the ADR equation
        %div(u*phi-e*grad(phi))=s-sl*phi
        problem=Input3D;

        %Fixed input
        problem.phi_w_fn=@(y,z) 1;
        problem.phi_e_fn=@(y,z) 0;
        problem.phi_s_fn=@(x,z) 0;
        problem.phi_n_fn=@(x,z) 1;
        problem.phi_u_fn=@(x,y) 0;
        problem.phi_d_fn=@(x,y) 1;
        problem.x_w=0;
        problem.x_e=1;
        problem.y_s=0;
        problem.y_n=1;
        problem.z_d=0;
        problem.z_u=1;

        %Variable input
        problem.N_x=N;
        problem.N_y=N;
        problem.N_z=N;

        %Work out the other parameters from the input
        dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
        e=1;
        u=Pe/dx;
        sl=Da*(1/dx^2);
        s=0.0;

        %Advection
        problem.u_x_fn=@(x,y,z) u/sqrt(3);
        problem.u_y_fn=@(x,y,z) u/sqrt(3);
        problem.u_z_fn=@(x,y,z) u/sqrt(3);
        %Diffusion
        problem.e_fn=@(x,y,z) e;
        %Constant source
        problem.s_fn=@(x,y,z) s;
        %Linear source
        problem.sl_fn=@(x,y,z) sl;

        %Set up the discretization matrix
        problem.flux_fn=@(u,e,ds) flux.HF(u,e,ds);
        [A,b]=problem.discretize();

        for plot_id=1:length(plot_cells)
            pc_vec=plot_cells{plot_id};
            for i=1:length(pc_vec)
                start_setup_time=posixtime(datetime('now'));
                pc=pc_vec(i);
                if strcmp(pc,"none")
                    M1=[];
                    M2=[];
                elseif strcmp(pc,"jacobi")
                    M1=spdiags(A,0);
                    M1=spdiags(M1,0,length(A),length(A));
                    M2=[];
                elseif strcmp(pc,"col_sum")
                    M1=spdiags(sum(abs(A),1)',0,length(A),length(A));
                    M2=[];
                elseif strcmp(pc,"row_sum")
                    M1=spdiags(sum(abs(A),2),0,length(A),length(A));
                    M2=[];
                elseif strcmp(pc,"both_sum")
                    error("Not implemented")
                elseif strcmp(pc,"tridiag")
                    M1=spdiags(A,[-1,0,1]);
                    M1=spdiags(M1,[-1,0,1],length(A),length(A));
                    M2=[];
                elseif strcmp(pc,"ilu0")
                    options.type='nofill';
                    [M1,M2]=ilu(A,options);
                elseif strcmp(pc,"iluc")
                    options.type='crout';
                    [M1,M2]=ilu(A,options);
                elseif strcmp(pc,"ilutp")
                    options.type='ilutp';
                    [M1,M2]=ilu(A,options);
                elseif strcmp(pc,"gauss_seidel_lower")
                    M1=tril(A);
                    M2=[];
                elseif strcmp(pc,"gauss_seidel_upper")
                    M1=triu(A);
                    M2=[];
                elseif contains(pc,"multigrid")
                    %Implementation based on:
                    %https://nl.mathworks.com/help/parallel-computing/solve-differential-equation-using-multigrid-preconditioner-on-distributed-discretization.html
                    if strcmp(pc,"multigrid")
                        multigridLevels=2;
                    elseif strcmp(pc,"multigrid1")
                        multigridLevels=1;
                    elseif strcmp(pc,"multigrid2")
                        multigridLevels=2;
                    elseif strcmp(pc,"multigrid3")
                        multigridLevels=3;
                    elseif strcmp(pc,"multigrid4")
                        multigridLevels=4;
                    end
                    multigridData = discretizeADREquation(numPoints,multigridLevels,numberOfSmootherSteps,problem,A);
                    M1 = setupPreconditioner(multigridData);
                    M2 = [];
                elseif strcmp(pc,"lu")
                    [M1,M2]=lu(A);
                end
                end_setup_time=posixtime(datetime('now'));

                %Initial guess
                x0=zeros(size(b));

                %Other settings
                tol=1e-12;
                maxit=2000;

                plt=figure(plot_id);
                filename=strjoin(["Pe=",num2str(Pe),"Da=",num2str(Da)," Figure: ",num2str(plot_id)]);
                %title(filename)
                normb=norm(b);
                start_time_solver=posixtime(datetime('now'));
                [x,flag,relres,iter,resvec] = bicgstab(A,b,tol,maxit,M1,M2,x0);
                %[x,flag,relres,iter,resvec] = idrs(A,b,4,tol,maxit,M1,M2,x0);
%                 opt.reliable_update=false;
%                 opt.random_rtilde=false;
%                 opt.keep_best=false;
%                 L=1;
%                 [x,flag,relres,iter,resvec]=rubicgstabl(A,b,tol,maxit/(2*L),M1,M2,x0,opt);
%                 len_relres=length(relres);

                end_time_solver=posixtime(datetime('now'));
                res=norm(b-A*x)/normb;
                disp(['Relres BiCGStab: ',num2str(res)])
                semilogy([1:1:length(resvec)],resvec/normb,'displayname',pc,'linewidth',1.5)
                hold on
                disp(['Setup time',pc,num2str(end_setup_time-start_setup_time)])
                disp(['Solve time',pc,num2str(end_time_solver-start_time_solver)])
                disp(['Wall  time',pc,num2str((end_time_solver-start_time_solver)+(end_setup_time-start_setup_time))])
                legend('Interpreter', 'none')
                xlabel("MV")
                ylabel("Relative residual")
                set(handleToAxes, 'XLimSpec', 'Tight');
                savefig(plt,filename)
                saveas(plt,[filename,".png"])
            end
        end
        disp("========================================================")
    end
end

function x = multigridPreconditioner(mgData,r,level)

if(level < mgData(level).MaxLevel)
    x = zeros(size(r),'like',r);

    % Presmooth using Gauss-Seidel
    for i=1:mgData(level).NumberOfSmootherSteps
        x = mgData(level).Matrices.LD \ (-mgData(level).Matrices.U*x + r);
        x = mgData(level).Matrices.DU \ (-mgData(level).Matrices.L*x + r);
    end

    % Compute residual on a coarser level
    Axf = mgData(level).Matrices.A*x;
    rc = r(mgData(level).Fine2CoarseOperator)- Axf(mgData(level).Fine2CoarseOperator);

    % Recursive call until coarsest level is reached
    xc = multigridPreconditioner(mgData, rc, level+1);

    % Update solution with prolonged coarse grid solution
    x(mgData(level).Fine2CoarseOperator) = x(mgData(level).Fine2CoarseOperator)+xc;

    % Postsmooth using Gauss-Seidel
    for i = 1:mgData(level).NumberOfSmootherSteps
        x = mgData(level).Matrices.LD \ (-mgData(level).Matrices.U*x + r);
        x = mgData(level).Matrices.DU \ (-mgData(level).Matrices.L*x + r);
    end
else
    % Obtain exact solution on the coarsest level
    x = mgData(level).Matrices.A \ r;
end

end

function preconditioner = setupPreconditioner(multigridData)

if ~isempty(multigridData)
    preconditioner = @(x,varargin) multigridPreconditioner(multigridData,x,1);
else
    preconditioner = [];
end

end