'''
Copyright (C) 2019-2022 Eindhoven University of Technology.

This code is free software, you can redistribute it and/or modify it under
the terms of the GNU General Public License; either version 3.0 of the
License, or (at your option) any later version. See LICENSE for details.

This code is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
        The Plasimo Team (https://plasimo.phys.tue.nl)
'''
'''
Plotscript for the acos_2019 results
00 result.algorithm << "\t" <<
01 result.true_residual << "\t" <<
02 result.estimated_residual << "\t" <<
03 result.iterations << "\t" <<
04 result.return_flag << "\t" <<
05 result.u << "\t" <<
06 result.beta << "\t" <<
07 result.e << "\t" <<
08 result.time << "\t" <<
09 result.N << "\t" <<
10 result.max_iterations << "\t" <<
11 result.tolerance << "\t" <<
12 result.const_source << "\t" <<
13 result.boundary_value << "\t" <<
14 result.length << "\t" <<
15 result.S << "\t" <<
16 result.L << "\t" <<
17 result.timestamp << "\t" <<
'''
#TODO: Clean this in some nice way

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import math
import scipy.interpolate
import plotter
from multiprocessing import Pool, cpu_count

plt.close()
font = {'family': 'normal',
        'weight': 'normal',
        'size': 15}
matplotlib.rc('font', **font)
'''
Available options:
	Algorithms (string):
		-amgcl
		-amgcl_eigen_bicgstab_solver
		-amgcl_bicgstabl_solver
		-amgcl_idrs_solver
		-BiCGSTAB
		-BiCGSTABL
		-IDRStab
	N (int):
		-5
		-11
		-21
		-41
		-81
		-161
	x,y (string)
		-u
		-e
		-beta
		-Advective Damkohler
		-Diffusive Damkohler
		-Harmonic Damkohler
		-Peclet
		-Grid Advective Damkohler
		-Grid Diffusive Damkohler
		-Grid Harmonic Damkohler
		-Grid Peclet
	z (string)
		-true_residual
		-estimated_residual
		-iterations
		-time
	scheme (string)
		-UW
		-CD
		-HF
		-CF
		-CFCF
'''
plot_jobs = []
for N in [101]:
	for scheme in ["HF"]:
		# job = {
		# 	"algorithm": "matlab_bicgstabl_eye",
		# 	"scheme": scheme,
		# 	"y": "Grid Diffusive Damkohler",
		# 	"x":"Grid Peclet",
		# 	"z": "cond",
		# 	"N": 101,
		# 	"e": 1,
		# 	"L":2,
		# 	"S":1,
		# }
		# plot_jobs.append(job)
		for solver in ["matlab_bicgstab_eye","matlab_idrs_eye","matlab_bicgstabl_eye","matlab_rubicgstablv1_L1_eye","matlab_rubicgstablv2_L1_eye","matlab_rubicgstablv3_L1_eye","matlab_rubicgstablv4_L1_eye","matlab_rubicgstablv5_L1_eye","matlab_rubicgstablv6_L1_eye","matlab_rubicgstablv7_L1_eye","matlab_rubicgstabl_L1_eye","matlab_lmr_eye","matlab_gmres1_eye","matlab_gmres10_eye","matlab_gmres50_eye","matlab_gmres100_eye","matlab_gmres250_eye"]:
			L=1
			S=1
			if solver=="matlab_idrs_eye":
				S=4
				L=2
			elif solver=="matlab_bicgstabl_eye":
				L=2
			elif solver=="matlab_rubicgstabl_eye":
				L=2
			if "_rubicgstabl" in solver:
				L=2
			if "_L1_" in solver:
				L=1
			if "_gmres" in solver:
				L=2
			if "_lmr_" in solver:
				L=2
			job = {
				"algorithm": solver,
				"scheme": scheme,
				"y": "Grid Diffusive Damkohler",
				"x":"Grid Peclet",
				"z": "true_residual",
				"N": N,
				"e": 1,
				"L":L,
				"S":S,
			}
			plot_jobs.append(job)
			job = {
				"algorithm": solver,
				"scheme": scheme,
				"y": "Grid Diffusive Damkohler",
				"x":"Grid Peclet",
				"z": "estimated_residual",
				"N": N,
				"e": 1,
				"L":L,
				"S":S,
			}
			plot_jobs.append(job)
			job = {
				"algorithm": solver,
				"scheme": scheme,
				"y": "Grid Diffusive Damkohler",
				"x":"Grid Peclet",
				"z": "time",
				"N": N,
				"e": 1,
				"L":L,
				"S":S,
			}
			plot_jobs.append(job)

#Load in data
data = np.loadtxt('paper32_no_precond_withgmres.log', delimiter='\t', dtype=object)
data[:, 1:-5] = data[:, 1:-5].astype(float)
print(data)

#Execute plot jobs
for job in plot_jobs:
	print(job)
	filename=""
	#Convert to a useful format
	if "N" in job:
		N = job["N"]
	else:
		N = None
	if isinstance(job["algorithm"],list):
		filename = filename+'_'+str('-'.join(job["algorithm"]))
	else:
		filename = filename+job["algorithm"]
	if isinstance(job["scheme"],list):
		filename = filename+'_'+str('-'.join(job["scheme"]))
	else:
		filename=filename+'_'+str(job["scheme"])
	filename = filename+'_'+str(N)
	filename = filename+'_x'+str(job["x"])
	filename = filename+'_y'+str(job["y"])
	if "z" in job:
		filename = filename+'_z'+str(job["z"])
		z_type = job["z"]
	else:
		z_type = None
	if "u" in job:
		u = job["u"]
	else:
		u = None
	if "e" in job:
		e = job["e"]
	else:
		u = None
	if "beta" in job:
		beta = job["beta"]
	else:
		beta = None
	L = job["L"]
	S = job["S"]
	#try:
	plotter.plot(filename, data, z_type, job["scheme"],
				job["algorithm"], job["x"], job["y"], L, S, N, u, e, beta)
	#except:
	#	continue
