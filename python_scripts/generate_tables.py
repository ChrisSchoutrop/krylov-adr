'''
Copyright (C) 2019-2022 Eindhoven University of Technology.

This code is free software, you can redistribute it and/or modify it under
the terms of the GNU General Public License; either version 3.0 of the
License, or (at your option) any later version. See LICENSE for details.

This code is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
        The Plasimo Team (https://plasimo.phys.tue.nl)
'''
import secrets
import string

input_filename="256_results.txt"
algorithm="Matlab BiCGStab"
preconditioners=["none","jacobi","ilu0","multigrid4"]
data=["solve","wall"]
Pe_list=[1e-5,1,1e5]
Da_list=[1e5,1,1e-5]

def generate_table(results,preconditioner,timetype):
	alphabet = string.ascii_letters + string.digits
	#table_label = ''.join(secrets.choice(alphabet) for i in range(8))
	table_label=timetype+preconditioner
	table="\\begin{table}[h]\n"
	table=table+"\t\\centering\n"
	if preconditioner=="none":
		fancy_preconditioner_name="unprecondtioned"
	elif preconditioner=="jacobi":
		fancy_preconditioner_name="Jacobi preconditioner"
	elif preconditioner=="ilu0":
		fancy_preconditioner_name="ILU(0) preconditioner"
	elif preconditioner=="multigrid4":
		fancy_preconditioner_name="multigrid preconditioner"
	table=table+"\t\\caption{"+timetype.capitalize()+" time (s) "+algorithm+", "+fancy_preconditioner_name+"}\n"
	table=table+"\t\\label{tab:"+table_label+"}\n"
	table=table+"\t\\begin{tabular}{l|lll}\n"
	table=table+"& $\\text{Pe}=10^{-5}$ & $\\text{Pe}=1$ & $\\text{Pe}=10^{5}$ \\\\ \hline\n"

	#Fill in the first row using results
	r1=[None,None,None]
	for Da in Da_list:
		for experiment in results:
			if experiment["Da"]==Da:
				if experiment["Pe"]==1e-5:
					r1[0]=experiment[preconditioner][timetype]
				elif experiment["Pe"]==1:
					r1[1]=experiment[preconditioner][timetype]
				elif experiment["Pe"]==1e5:
					r1[2]=experiment[preconditioner][timetype]
		if Da==1e5:
			table=table+"$\\text{Da}=10^{5}$"+f"&{r1[0]:.1f}&{r1[1]:.1f}&{r1[2]:.1f}\\\\\n"
		elif Da==1:
			table=table+"$\\text{Da}=1$"+f"&{r1[0]:.1f}&{r1[1]:.1f}&{r1[2]:.1f}\\\\\n"
		elif Da==1e-5:
			table=table+"$\\text{Da}=10^{-5}$"+f"&{r1[0]:.1f}&{r1[1]:.1f}&{r1[2]:.1f}\\\\\n"

	table=table+"\\end{tabular}\n"
	table=table+"\\end{table}"
	print(table)
	pass

def process_section(lines):
	results={}
	idx=0
	for ix,line in enumerate(lines):
		idx=ix
		line=line.lower()
		line=line.replace("\"","")
		r1=line.split()
		if line.startswith("==="):
			#End of section
			break
		if line.startswith("level "):
			continue
		if line.startswith("relres"):
			continue
		if r1[0]=="pe=":
			results["Pe"]=float(r1[1])
			results["Da"]=float(r1[3])
			continue
		print(r1)
		#['setup', 'time', 'none', '0.001606']
		try:
			results[r1[2]][r1[0]]=float(r1[3])
		except:
			results[r1[2]]={}
			results[r1[2]][r1[0]]=float(r1[3])
		print(idx)
	return results,idx

with open(input_filename) as file:
	lines=file.readlines()
	lines=[line.rstrip() for line in lines]

'''
First two lines contain
N_inner_points =
   256
'''
N=int(lines[1])
lines=lines[2:]

results=[]
while lines:
	new_result,ix=process_section(lines)
	results.append(new_result)
	del lines[0:ix+1]

print(results)
for d in data:
	for p in preconditioners:
		generate_table(results,p,d)