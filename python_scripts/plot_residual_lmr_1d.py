'''
Copyright (C) 2019-2022 Eindhoven University of Technology.

This code is free software, you can redistribute it and/or modify it under
the terms of the GNU General Public License; either version 3.0 of the
License, or (at your option) any later version. See LICENSE for details.

This code is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
        The Plasimo Team (https://plasimo.phys.tue.nl)
'''
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import math
import scipy.interpolate
import scipy.io
import copy

plt.close()
font = {'family': 'normal',
        'weight': 'normal',
        'size': 15}
matplotlib.rc('font', **font)

scenario="omega0.5"
#scenario="extended"

if scenario=="omega0.5":
    '''
    Data for the case in which omega=0.5 is valid
    '''
    r_exact=scipy.io.mmread("r_exact.mtx")
    r_lmr=scipy.io.mmread("r_lmr.mtx")
    t_axis=scipy.io.mmread("tspan.mtx")
elif scenario=="extended":
    '''
    Data for the case where LMR is still performing forward Euler
    but omega!=0.5.
    Note for maxiter>M, we can no longer guarantee that omega=0.5 and there do
    indeed exist iterations for which omega>2. For omega>2 the forward Euler
    discretization scheme is in principle unstable and negative values exist in the
    residual. However, also note that omega is chosen such that norm(r) is
    minimized and thus the residual will remain bounded in size as it can never
    exceed the initial residual. Nevertheless, it is interesting to see
    omega>0.5 as this implies the system evolves faster.

    Interesting things happen when varying Lambda as well. It always takes at
    least M iterations for the residual to traverse the entire domain. However,
    note that larger lambda lead to smaller initial omega. It is conjectured
    that Lambda*initial omega=constant. After a certain critical time, omega is
    allows to increase. It is conjectured that this happens right when the wave
    has passed through the domain; Lambda can be seen as the advection
    velocity of sorts (if we were to use upwind discretization?), the length of
    the domain is 1, so it takes 1/lambda seconds for a packet of fluid to pass
    through the domain.

    In the 2D and 3D experiments this does not seem to be the case, as it takes 2M and 3M
    iterations for the plateau region of the convergence.
    In 3D the velocity is u/sqrt(3) in each of the directions where we have a coupling.
    1D might actually be exceptional in that both the non-zero front and the wave have to
    travel across the domain in the same direction. 9-point or 27 point coupling in 2D/3D
    may be a good check. For the 7-point coupling in 3D omega does not seem to strongly
    increase at any point.
    '''
    r_exact=scipy.io.mmread("r_exact_extended.mtx")
    r_lmr=scipy.io.mmread("r_lmr_extended.mtx")
    t_axis=scipy.io.mmread("tspan_extended.mtx")


print(t_axis)
#Check that this is monotonically increasing
for ix in range(0,len(t_axis)-1):
    if (t_axis[ix+1]>t_axis[ix])==False:
        print("t_axis not monotonically increasing!")

x_axis=[]
for i in range(1,101):
    x_axis.append(i)

print(x_axis)

print(r_exact)
print(r_lmr)

'''
Plot for the solution of the exact equation
'''
#Set up the plot
f = plt.figure()
ax = plt.gca()

#Set up the plot grid
xi, yi = np.meshgrid(x_axis, t_axis)
zi=np.transpose(r_exact)

pcm = plt.pcolormesh(xi, yi, zi, cmap='jet', norm=matplotlib.colors.LogNorm(
    vmin=1e-50, vmax=1), edgecolors='none')
cbar = plt.colorbar(extend='both')
plt.xlabel('x (arb.u.)')
plt.ylabel('t (arb.u.)')
#cbar.ax.set_ylabel('Iterations', usetex=False)

f.savefig("r_exact.png", bbox_inches='tight')
plt.close()
'''
Plot for the solution of LMR
'''
#Set up the plot
f = plt.figure()
ax = plt.gca()

#Set up the plot grid
if scenario=='omega0.5':
    xi, yi = np.meshgrid(x_axis, [i for i in range(1,len(t_axis)+1)])
else:
    xi, yi = np.meshgrid(x_axis, t_axis)
zi=np.transpose(r_lmr)

if scenario=='omega0.5':
    pcm = plt.pcolormesh(xi, yi, zi, cmap='jet', norm=matplotlib.colors.LogNorm(vmin=1e-50, vmax=1), edgecolors='none')
elif scenario=="extended":
    pcm = plt.pcolormesh(xi, yi, np.abs(zi), cmap='jet', norm=matplotlib.colors.LogNorm(vmin=1e-50, vmax=1), edgecolors='none')
cbar = plt.colorbar(extend='both')
plt.xlabel('Index')
if scenario=='omega0.5':
    plt.ylabel('Iteration')
elif scenario=="extended":
    plt.ylabel('Sum(omega)')
#cbar.ax.set_ylabel('Iterations', usetex=False)

f.savefig("r_lmr.png", bbox_inches='tight')
plt.close()
print(zi)

'''
Plot of omega as function of iteration count
'''
#Set up the plot
f = plt.figure()
ax = plt.gca()

print(np.diff(t_axis[:,0]))
plt.plot(np.diff(t_axis[:,0]),label="omega")
plt.plot(np.log10(np.linalg.norm(r_lmr,axis=0)),label="log10(norm(r))")
plt.xlabel('Iteration')
plt.ylabel('')
plt.legend()
f.savefig("omega_iteration.png", bbox_inches='tight')
plt.close()

#Set up the plot
f = plt.figure()
ax = plt.gca()
plt.plot(t_axis[:-1,0],np.diff(t_axis[:,0]),label="omega")
plt.plot(t_axis,np.log10(np.linalg.norm(r_lmr,axis=0)),label="log10(norm(r))")
plt.xlabel('t (arb.u.)')
plt.ylabel('')
plt.legend()
f.savefig("omega_time.png", bbox_inches='tight')
plt.close()
