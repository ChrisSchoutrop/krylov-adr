% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
%close all
clc

rng(0,'twister');

plot_id=0;

%{
Setup
%}
problem=Input3D;
N=101;

Pe=1e5
Da=1e-5

dx=1./(N-1);

L=dx;
u=Pe/L;
e=1.0;
sl=Da*(1/L^2);
s=0.0;

u_y=u/sqrt(3);
u_x=u_y;
u_z=u_y;

problem.phi_w_fn=@(y,z) 1;
problem.phi_e_fn=@(y,z) 0;
problem.phi_s_fn=@(x,z) 0;
problem.phi_n_fn=@(x,z) 1;
problem.phi_u_fn=@(x,y) 0;
problem.phi_d_fn=@(x,y) 1;
problem.x_w=0;
problem.x_e=1;
problem.y_s=0;
problem.y_n=1;
problem.z_d=0;
problem.z_u=1;

problem.N_x=N;
problem.N_y=N;
problem.N_z=N;

problem.u_x_fn=@(x,y,z) u_x;
problem.u_y_fn=@(x,y,z) u_y;
problem.u_z_fn=@(x,y,z) u_z;
problem.e_fn=@(x,y,z) e;
problem.s_fn=@(x,y,z) s;
problem.sl_fn=@(x,y,z) sl;

%{
Solve the problem
%}
%     problem.flux_fn=@(u,e,ds) flux.UW(u,e,ds);
%     [A_UW,b_UW]=problem.discretize();
%     tic
%     disp(['condest(A_UW): ',num2str(condest(A_UW),3)])
%     toc
%     tic
%     disp(['condest(A_UW): ',num2str(condest_krylov(A_UW),3)])
%     toc
%     problem.flux_fn=@(u,e,ds) flux.CD(u,e,ds);
%     [A_CD,b_CD]=problem.discretize();
%     tic
%     %disp(['condest(A_CD): ',num2str(condest(A_CD),3)])
%     toc
%     tic
%     disp(['condest(A_CD): ',num2str(condest_krylov(A_CD),3)])
%     toc
    problem.flux_fn=@(u,e,ds) flux.HF(u,e,ds);
    [A_HF,b_HF]=problem.discretize();
%     tic
%     %disp(['condest(A_HF): ',num2str(condest(A_HF),3)])
%     toc
%     tic
%     disp(['condest(A_HF): ',num2str(condest_krylov(A_HF),3)])
%     toc
%     phi_ones=A_HF\b_HF;


    r1=diag(A_HF);
    r1(r1==0)=1;
    M1=spdiags(r1,0,length(A_HF),length(A_HF));
    x0=zeros(size(b_HF));
    [phi,flag,relres,iter,resvec] = rubicgstabl(A_HF,b_HF,1e-12,2000,M1,[],x0);
    res=norm(b_HF-A_HF*phi)/norm(b_HF);
    disp(['Relres RU-BiCGStab(2): ',num2str(res)])

     %phi=A_HF\b_HF;
     phi=problem.assemble_phi(phi);

%     condest_krylov(A_HF)
%     condest_krylov(A_HF/M1)
%
phi_plot=zeros(problem.N_x,problem.N_y);
x=linspace(problem.x_w,problem.x_e,problem.N_x);
y=linspace(problem.x_w,problem.x_e,problem.N_y);
[X,Y]=meshgrid(x,y);

k=ceil(problem.N_z/4);

for i=0:problem.N_x-1
    for j=0:problem.N_y-1
        ix_C   = (i + 0) + problem.N_x * (j + 0) + problem.N_x * problem.N_y * (k + 0) + 1;
        phi_plot(i+1,j+1)=phi(ix_C);
    end
end
plot_id=plot_id+1;
figure(plot_id)
clf
contourf(X,Y,phi_plot');
colorbar
colormap jet
xlabel('x')
ylabel('y')
zlabel('phi')
title('Nz/4')

k=ceil(problem.N_z/2);

for i=0:problem.N_x-1
    for j=0:problem.N_y-1
        ix_C   = (i + 0) + problem.N_x * (j + 0) + problem.N_x * problem.N_y * (k + 0) + 1;
        phi_plot(i+1,j+1)=phi(ix_C);
    end
end
plot_id=plot_id+1;
figure(plot_id)
clf
contourf(X,Y,phi_plot');
colorbar
colormap jet
xlabel('x')
ylabel('y')
zlabel('phi')
title('Nz/2')

k=ceil(3*problem.N_z/4);

for i=0:problem.N_x-1
    for j=0:problem.N_y-1
        ix_C   = (i + 0) + problem.N_x * (j + 0) + problem.N_x * problem.N_y * (k + 0) + 1;
        phi_plot(i+1,j+1)=phi(ix_C);
    end
end
plot_id=plot_id+1;
figure(plot_id)
clf
contourf(X,Y,phi_plot');
colorbar
colormap jet
xlabel('x')
ylabel('y')
zlabel('phi')
title('3Nz/4')

%{
Solve using Krylov methods
%}
schemes=["HF"];
for schemes_ix=1:length(schemes)
    disp(['Residual for scheme: ',schemes(schemes_ix)])
    switch schemes(schemes_ix)
        case "UW"
            A=A_UW;
            b=b_UW;
        case "CD"
            A=A_CD;
            b=b_CD;
        case "HF"
            A=A_HF;
            b=b_HF;
    end

    %No preconditioner
    M1=[];
    M2=[];

    %Initial guess
    x0=zeros(size(b));

    %Other settings
    tol=1e-12;
    maxit=2000;

    plot_id=plot_id+1;
    figure(plot_id)
    clf
    hold on
    normb=norm(b);
    [x,flag,relres,iter,resvec] = bicgstab(A,b,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres BiCGStab: ',num2str(res)])
    plot([1:1:length(resvec)],resvec/normb,'displayname','Matlab BiCGStab','linewidth',1.5)

    [x,flag,relres,iter,resvec] = bicgstabl(A,b,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres BiCGStab(2): ',num2str(res)])
    plot([1:1:length(resvec)],resvec/normb,'displayname','Matlab BiCGStab(2)','linewidth',1.5)

    [x,flag,relres,iter,resvec] = idrs(A,b,4,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres IDR(4): ',num2str(res)])
    plot([1:1:length(resvec)],resvec/normb,'displayname','IDR(4)','linewidth',1.5)

%     [x,flag,relres,iter,resvec] = gmres(A,b,[],tol,500,M1,M2,x0);
%     res=norm(b-A*x)/normb;
%     disp(['Relres GMRES(Inf): ',num2str(res)])
%     plot([1:length(resvec)],resvec/normb,'displayname','GMRES(Inf)','linewidth',1.5)
%
%     [x,flag,relres,iter,resvec] = gmres(A,b,10,tol,maxit,M1,M2,x0);
%     res=norm(b-A*x)/normb;
%     disp(['Relres GMRES(10): ',num2str(res)])
%     plot([1:length(resvec)],resvec/normb,'displayname','GMRES(10)','linewidth',1.5)

    [x,flag,relres,iter,resvec] = lmr(A,b,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres LMR: ',num2str(res)])
    plot([1:length(resvec)],resvec/normb,'displayname','LMR','linewidth',1.5)

    [x,flag,relres,iter,resvec] = rubicgstabl(A,b,tol,maxit,M1,M2,x0);
    res=norm(b-A*x)/normb;
    disp(['Relres RU-BiCGStab(2): ',num2str(res)])
    plot([1:1:length(resvec)],resvec/normb,'displayname',"Modified BiCGStab",'linewidth',1.5)

    set(gca, 'YScale', 'log')
    set(gca, 'Linewidth', 1.5)
    box on
    xlabel('MV')
    ylabel('Residual')
    title(schemes(schemes_ix))
    legend()
    axis tight

end
