% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
%close all
%{
Script to show that in some cases the residual can be seen as the solution
to something similar to a time-dependent ADR problem.

Interesting parameters;

%Wave-like solution:
dimension=2;
N=101;
Pe=1e5;
e=1;
Da=1e-5;

%Diffusion-like solution:
dimension=2;
N=101;
Pe=1e-5;
e=1;
Da=1e-5;
%}

rng(0,'twister')
clc
plot_id=1;

dimension=2;

N=78;
Pe=1e5;
e=1;
Da=1e-5;

maxit_vec=[5,25,50,75,100,125,150,175,200,225];
for iter_maxit=1:length(maxit_vec)

if dimension==3
    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input3D;

    %Fixed input
    problem.phi_w_fn=@(y,z) 1;
    problem.phi_e_fn=@(y,z) 0;
    problem.phi_s_fn=@(x,z) 0;
    problem.phi_n_fn=@(x,z) 1;
    problem.phi_u_fn=@(x,y) 0;
    problem.phi_d_fn=@(x,y) 1;
    problem.x_w=0;
    problem.x_e=1;
    problem.y_s=0;
    problem.y_n=1;
    problem.z_d=0;
    problem.z_u=1;

    %Variable input
    problem.N_x=N;
    problem.N_y=N;
    problem.N_z=N;


    %Work out the other parameters from the input
    dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
    u=Pe/dx;
    sl=Da*(1/dx^2);
    s=0.0;
    %sl=0.0;
    %s=Da * (u + e / dx) / dx;

    %Set the flux approximation based on the scheme used
    problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

    %Advection
    problem.u_x_fn=@(x,y,z) u/sqrt(3);
    problem.u_y_fn=@(x,y,z) u/sqrt(3);
    problem.u_z_fn=@(x,y,z) u/sqrt(3);
    %Diffusion
    problem.e_fn=@(x,y,z) e;
    %Constant source
    problem.s_fn=@(x,y,z) s;
    %Linear source
    problem.sl_fn=@(x,y,z) sl;
elseif dimension==2
    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input2D;

    %Fixed input
    problem.phi_w_fn=@(y) 1;
    problem.phi_e_fn=@(y) 0;
    problem.phi_s_fn=@(x) 0;
    problem.phi_n_fn=@(x) 1;
    problem.x_w=0;
    problem.x_e=1;
    problem.y_s=0;
    problem.y_n=1;

    %Variable input
    problem.N_x=N;
    problem.N_y=N;


    %Work out the other parameters from the input
    dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
    u=Pe/dx;
    sl=Da*(1/dx^2);
    s=0.0;
    %sl=0.0;
    %s=Da * (u + e / dx) / dx;

    %Set the flux approximation based on the scheme used
    problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

    %Advection
    problem.u_x_fn=@(x,y) u/sqrt(2);
    problem.u_y_fn=@(x,y) u/sqrt(2);
    %Diffusion
    problem.e_fn=@(x,y) e;
    %Constant source
    problem.s_fn=@(x,y) s;
    %Linear source
    problem.sl_fn=@(x,y) sl;
elseif dimension==1
    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input1D;

    %Fixed input
    problem.phi_w= 1;
    problem.phi_e= 0;
    problem.x_w=0;
    problem.x_e=1;

    %Variable input
    problem.N_x=N;


    %Work out the other parameters from the input
    dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
    u=Pe/dx;
    sl=Da*(1/dx^2);
    s=0.0;
    %sl=0.0;
    %s=Da * (u + e / dx) / dx;

    %Set the flux approximation based on the scheme used
    problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

    %Advection
    problem.u_fn=@(x) u/sqrt(3);
    %Diffusion
    problem.e_fn=@(x) e;
    %Constant source
    problem.s_fn=@(x) s;
    %Linear source
    problem.sl_fn=@(x) sl;
else
    error('Dimension not supported')
end

disp('Discretizing')
[A,b]=problem.discretize();

%No preconditioner
M1=[];
M2=[];

%Multigrid preconditioner
numPoints.X = N-2;
numPoints.Y = N-2;
numPoints.Z = N-2;
numberOfSmootherSteps = 1;
multigridLevels=2;
multigridData = discretizeADREquation2D(numPoints,multigridLevels,numberOfSmootherSteps,problem,A);
M1 = setupPreconditioner(multigridData);
M2 = [];

%Solver settings
maxit=maxit_vec(iter_maxit);
tol=1e-12;
options.L=1;
options.reliable_update=false;
rng(5)
options.random_rtilde=true;
options.keep_best=false;
options.norm=2;

x0=zeros(size(b));

[phi_solver,~,~,~,resvec]=rubicgstabl(A,b,tol,maxit,M1,M2,x0,options);
%[phi_solver,~,~,~,resvec]=lmr(A,b,1e-12,maxit,M1,M2,x0);
%[phi_solver,~,~,~,resvec]=gmres(A,b,1,tol,maxit,M1,M2,x0,options);
%[phi_solver,~,~,~,resvec]=bicgstab(A,b,tol,maxit,M1,M2,x0,options);

relresvec=resvec/norm(b);

%Assemble the solution for plotting
phi=problem.assemble_phi(phi_solver);
phi_plot=zeros(problem.N_x,problem.N_y);
x=linspace(problem.x_w,problem.x_e,problem.N_x);
y=linspace(problem.x_w,problem.x_e,problem.N_y);
[X,Y]=meshgrid(x,y);

if dimension==3
    k=ceil(problem.N_z/2);
    for i=0:problem.N_x-1
        for j=0:problem.N_y-1
            ix_C   = (i + 0) + problem.N_x * (j + 0) + problem.N_x * problem.N_y * (k + 0) + 1;
            phi_plot(i+1,j+1)=phi(ix_C);
        end
    end
else
    for i=0:problem.N_x-1
        for j=0:problem.N_y-1
            ix_C =  (i + 0) + problem.N_x * (j + 0)+1;
            phi_plot(i+1,j+1)=phi(ix_C);
        end
    end
end
plot_id=plot_id+1;
figure(plot_id)
clf
contourf(X,Y,phi_plot');
colorbar
colormap jet
xlabel('x')
ylabel('y')
zlabel('phi')
title('Solution')

%Assemble the residual for plotting
%The residual is 0 on the boundaries (which are not solved)
if dimension==3
    problem.phi_w_fn=@(y,z) 0;
    problem.phi_e_fn=@(y,z) 0;
    problem.phi_s_fn=@(x,z) 0;
    problem.phi_n_fn=@(x,z) 0;
    problem.phi_u_fn=@(x,y) 0;
    problem.phi_d_fn=@(x,y) 0;
else
    problem.phi_w_fn=@(y) 0;
    problem.phi_e_fn=@(y) 0;
    problem.phi_s_fn=@(x) 0;
    problem.phi_n_fn=@(x) 0;
end
phi=problem.assemble_phi(b-A*phi_solver);
phi_plot=zeros(problem.N_x,problem.N_y);
x=linspace(problem.x_w,problem.x_e,problem.N_x);
y=linspace(problem.x_w,problem.x_e,problem.N_y);
[X,Y]=meshgrid(x,y);

if dimension==3
    k=ceil(problem.N_z/2);
    for i=0:problem.N_x-1
        for j=0:problem.N_y-1
            ix_C   = (i + 0) + problem.N_x * (j + 0) + problem.N_x * problem.N_y * (k + 0) + 1;
            phi_plot(i+1,j+1)=phi(ix_C);
        end
    end
else
    for i=0:problem.N_x-1
        for j=0:problem.N_y-1
            ix_C =  (i + 0) + problem.N_x * (j + 0)+1;
            phi_plot(i+1,j+1)=phi(ix_C);
        end
    end
end
plot_id=plot_id+1;
figure(plot_id)
clf
contourf(X,Y,log10(abs(phi_plot')));
colorbar
colormap jet
xlabel('x')
ylabel('y')
zlabel('phi')
title(['Residual, True relres:',num2str(norm(b-A*phi_solver)/norm(b))])

mmwrite("relres_moving_X.mtx",X)
mmwrite("relres_moving_Y.mtx",Y)
mmwrite(join(["relres_moving_rubicgstab_",num2str(maxit_vec(iter_maxit)),".mtx"],""),phi_plot'./norm(b))

plot_id=plot_id+1;
figure(plot_id)
clf
semilogy(relresvec,'linewidth',1.5)
axis tight
xlabel('MV')
ylabel('Residual')
mmwrite('resvec_moving_rubicgstab.mtx',relresvec)

end

function x = multigridPreconditioner(mgData,r,level)

if(level < mgData(level).MaxLevel)
    x = zeros(size(r),'like',r);

    % Presmooth using Gauss-Seidel
    for i=1:mgData(level).NumberOfSmootherSteps
        x = mgData(level).Matrices.LD \ (-mgData(level).Matrices.U*x + r);
        x = mgData(level).Matrices.DU \ (-mgData(level).Matrices.L*x + r);
    end

    % Compute residual on a coarser level
    Axf = mgData(level).Matrices.A*x;
    rc = r(mgData(level).Fine2CoarseOperator)- Axf(mgData(level).Fine2CoarseOperator);

    % Recursive call until coarsest level is reached
    xc = multigridPreconditioner(mgData, rc, level+1);

    % Update solution with prolonged coarse grid solution
    x(mgData(level).Fine2CoarseOperator) = x(mgData(level).Fine2CoarseOperator)+xc;

    % Postsmooth using Gauss-Seidel
    for i = 1:mgData(level).NumberOfSmootherSteps
        x = mgData(level).Matrices.LD \ (-mgData(level).Matrices.U*x + r);
        x = mgData(level).Matrices.DU \ (-mgData(level).Matrices.L*x + r);
    end
else
    % Obtain exact solution on the coarsest level
    x = mgData(level).Matrices.A \ r;
end

end

function preconditioner = setupPreconditioner(multigridData)

if ~isempty(multigridData)
    preconditioner = @(x,varargin) multigridPreconditioner(multigridData,x,1);
else
    preconditioner = [];
end

end
















