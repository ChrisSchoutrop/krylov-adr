% Copyright (C) 2019-2022 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
clear all
close all
clc
%{
Test to see if the system matrix A can be constructed using "Kronecker
sums".
%}

N=10;

dx=1/(N-1);

Pe=1;
e=1;
Da=1;

sl=Da*(1/dx^2);

%{
Test to see what happens if Ax=Ay=Az
%}
disp('Ax=Ay=Az')
[A1D,b1D]=generate_problem(1,N,Pe,e,Da);
[A2D,b2D]=generate_problem(2,N,Pe,e,Da);
[A3D,b3D]=generate_problem(3,N,Pe,e,Da);

%In the paper the source is not in A1D, but gets added later on.
A1D=A1D-sl*dx*eye((N-2)^1);

A2D_test=kron_sum(A1D,A1D)*dx+sl*dx^2*eye((N-2)^2);
A3D_test1=kron_sum(kron_sum(A1D,A1D),A1D)*dx*dx+sl*dx^3*eye((N-2)^3);
A3D_test2=kron_sum(A1D,kron_sum(A1D,A1D))*dx*dx+sl*dx^3*eye((N-2)^3);

%These should be near-zero.
normest(A2D-A2D_test)./normest(A2D)
normest(A3D-A3D_test1)./normest(A3D)
normest(A3D-A3D_test2)./normest(A3D)

%Check of the eigenvalues:
L_A1D=sort(eig(full(A1D)));
L_A2D=sort(eig(full(A2D)));
L_A3D=sort(eig(full(A3D)));

L_A2D_formula=[];
for i=1:N-2
    for j=1:N-2
        L_A2D_formula(end+1,1)=(L_A1D(i)+L_A1D(j)).*dx+sl*dx^2;
    end
end
L_A3D_formula=[];
for i=1:N-2
    for j=1:N-2
        for k=1:N-2
            L_A3D_formula(end+1,1)=(L_A1D(i)+L_A1D(j)+L_A1D(k)).*dx^2+sl*dx^3;
        end
    end
end
L_A2D_formula=sort(L_A2D_formula);
L_A3D_formula=sort(L_A3D_formula);
L_A2D(1)
L_A2D_formula(1)
disp('Eigenvalues test 2D')
norm(L_A2D_formula-L_A2D,1)./norm(L_A2D,1)
disp('Eigenvalues test 3D')
norm(L_A3D_formula-L_A3D,1)./norm(L_A3D,1)

%Check if A is normal, if it is we could use eigenvalues to properly look into
%the convergence of gmres. Non-normal becomes a bit of a mess.
disp('Test if A3D is normal')
t=A3D'*A3D-A3D*A3D';
normest(t)

%{
Test to see what happens if Ax!=Ay!=Az
%}
disp('Ax!=Ay!=Az')
[A1Dx,b1Dx]=generate_problem(1,N,Pe,e,Da);
[A1Dy,b1Dy]=generate_problem(1,N,2.7183*Pe,e,Da);
[A1Dz,b1Dz]=generate_problem(1,N,-3.1416*Pe,e,Da);

[A3D,b3D]=generate_problem(3,N,Pe,e,Da,2.7183,-3.1416);

%In the paper the source is not in A1D, but gets added later on.
A1Dx=A1Dx-sl*dx*eye((N-2)^1);
A1Dy=A1Dy-sl*dx*eye((N-2)^1);
A1Dz=A1Dz-sl*dx*eye((N-2)^1);

A3D_test1=kron_sum(kron_sum(A1Dx,A1Dy),A1Dz)*dx*dx+sl*dx^3*speye((N-2)^3);
A3D_test2=kron_sum(A1Dx,kron_sum(A1Dy,A1Dz))*dx*dx+sl*dx^3*speye((N-2)^3);

%Should be near-zero
normest(A3D-A3D_test1)./normest(A3D)
normest(A3D-A3D_test2)./normest(A3D)

%{
Test to check if the ordering is lexicographic or reversed lexicographic.
This is done by computing the solution to a known problem. (Only diffusion
with some boundaries set to 1 should give decreasing parabolas).
%}
disp('Ordering test')
[A,b]=generate_problem_xyz_3D('x',N);
phi=A\b;
figure(1)
clf
plot(phi);
xlabel('Index')
ylabel('phi')
title('x')

[A,b]=generate_problem_xyz_3D('y',N);
phi=A\b;
figure(2)
clf
plot(phi);
xlabel('Index')
ylabel('phi')
title('y')

[A,b]=generate_problem_xyz_3D('z',N);
phi=A\b;
figure(3)
clf
plot(phi);
xlabel('Index')
ylabel('phi')
title('z')

[A,b]=generate_problem_xyz_2D('x',N);
phi=A\b;
figure(4)
clf
plot(phi);
xlabel('Index')
ylabel('phi')
title('x')

[A,b]=generate_problem_xyz_2D('y',N);
phi=A\b;
figure(5)
clf
plot(phi);
xlabel('Index')
ylabel('phi')
title('y')

%{
Randomized tests of the Kronecker sum construction to make sure it is not a
fluke.
%}
for i=1:100
    disp(['Random test: ',num2str(i)])
    N=randi([5,100]);

    dx=1/(N-1);

    Pe=rand()*10^(randi([-6,6]));
    e=1;
    Da=rand()*10^(randi([-6,6]));

    sl=Da*(1/dx^2);

    %Test for Ax=Ay=Az:
    [A1D,b1D]=generate_problem(1,N,Pe,e,Da);
    [A2D,b2D]=generate_problem(2,N,Pe,e,Da);
    [A3D,b3D]=generate_problem(3,N,Pe,e,Da);

    %In the paper the source is not in A1D, but gets added later on.
    A1D=A1D-sl*dx*eye((N-2)^1);

    A2D_test=kron_sum(A1D,A1D)*dx+sl*dx^2*speye((N-2)^2);
    A3D_test1=kron_sum(kron_sum(A1D,A1D),A1D)*dx*dx+sl*dx^3*speye((N-2)^3);
    A3D_test2=kron_sum(A1D,kron_sum(A1D,A1D))*dx*dx+sl*dx^3*speye((N-2)^3);

    e1=normest(A2D-A2D_test)./normest(A2D)
    n3D=normest(A3D);
    e2=normest(A3D-A3D_test1)./n3D
    e3=normest(A3D-A3D_test2)./n3D
    assert(e1<1e-12)
    assert(e2<1e-12)
    assert(e3<1e-12)


    u_y=(rand()-0.5)*1e5;
    u_z=(rand()-0.5)*1e5;

    [A1Dx,b1Dx]=generate_problem(1,N,Pe,e,Da);
    [A1Dy,b1Dy]=generate_problem(1,N,u_y*Pe,e,Da);
    [A1Dz,b1Dz]=generate_problem(1,N,u_z*Pe,e,Da);

    %[A2D,b2D]=generate_problem(2,N,Pe,e,Da);
    [A3D,b3D]=generate_problem(3,N,Pe,e,Da,u_y,u_z);

    %In the paper the source is not in A1D, but gets added later on.
    A1Dx=A1Dx-sl*dx*eye((N-2)^1);
    A1Dy=A1Dy-sl*dx*eye((N-2)^1);
    A1Dz=A1Dz-sl*dx*eye((N-2)^1);

    A3D_test1=kron_sum(kron_sum(A1Dx,A1Dy),A1Dz)*dx*dx+sl*dx^3*speye((N-2)^3);
    A3D_test2=kron_sum(A1Dx,kron_sum(A1Dy,A1Dz))*dx*dx+sl*dx^3*speye((N-2)^3);

    %normest(A2D-A2D_test)./normest(A2D)
    n3D=normest(A3D);
    e1=normest(A3D-A3D_test1)./n3D;
    e2=normest(A3D-A3D_test2)./n3D;

    assert(e1<1e-12)
    assert(e2<1e-12)

end



function [E]=kron_sum(C,D)

n=length(D);
m=length(C);

E=kron(speye(n),C)+kron(D,speye(m));
%E=kron(C,speye(n))+kron(speye(m),D);
end


function [A,b]=generate_problem_xyz_2D(direction,N)
    e=1;
    Da=0;
    Pe=0;
    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input2D;

 %Fixed input
    problem.phi_w_fn=@(y) 1;
    problem.phi_e_fn=@(y) 1;
    problem.phi_s_fn=@(x) 1;
    problem.phi_n_fn=@(x) 1;
    problem.x_w=0;
    problem.x_e=1;
    problem.y_s=0;
    problem.y_n=1;

    %Variable input
    problem.N_x=N;
    problem.N_y=N;

    %Work out the other parameters from the input
    dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
    u=Pe/dx;
    sl=Da*(1/dx^2);
    s=0.0;
    %sl=0.0;
    %s=Da * (u + e / dx) / dx;

    %Set the flux approximation based on the scheme used
    problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

    %Advection
    problem.u_x_fn=@(x,y) 0;
    problem.u_y_fn=@(x,y) 0;
    %Diffusion
    problem.e_fn=@(x,y) e;
    %Constant source
    problem.s_fn=@(x,y) s;
    %Linear source
    problem.sl_fn=@(x,y) sl;

if strcmp(direction,'x')
    problem.phi_e_fn=@(y) 0;
    problem.u_x_fn=@(x,y) u;
elseif strcmp(direction,'y')
    problem.phi_n_fn=@(x) 0;
    problem.u_y_fn=@(x,y) u;
end

disp('Discretizing')
[A,b]=problem.discretize();

end

function [A,b]=generate_problem_xyz_3D(direction,N)
    e=1;
    Da=0;
    Pe=0;
    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input3D;

    %Fixed input
    problem.phi_w_fn=@(y,z) 1;
    problem.phi_e_fn=@(y,z) 1;
    problem.phi_s_fn=@(x,z) 1;
    problem.phi_n_fn=@(x,z) 1;
    problem.phi_u_fn=@(x,y) 1;
    problem.phi_d_fn=@(x,y) 1;
    problem.x_w=0;
    problem.x_e=1;
    problem.y_s=0;
    problem.y_n=1;
    problem.z_d=0;
    problem.z_u=1;

    %Variable input
    problem.N_x=N;
    problem.N_y=N;
    problem.N_z=N;


    %Work out the other parameters from the input
    dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
    u=Pe/dx;
    sl=Da*(1/dx^2);
    s=0.0;
    %sl=0.0;
    %s=Da * (u + e / dx) / dx;

    %Set the flux approximation based on the scheme used
    problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

    %Advection
    problem.u_x_fn=@(x,y,z) 0;
    problem.u_y_fn=@(x,y,z) 0;
    problem.u_z_fn=@(x,y,z) 0;
    %Diffusion
    problem.e_fn=@(x,y,z) e;
    %Constant source
    problem.s_fn=@(x,y,z) s;
    %Linear source
    problem.sl_fn=@(x,y,z) sl;

if strcmp(direction,'x')
    problem.phi_e_fn=@(y,z) 0;
    problem.u_x_fn=@(x,y,z) u;
elseif strcmp(direction,'y')
    problem.phi_n_fn=@(x,z) 0;
    problem.u_y_fn=@(x,y,z) u;
elseif strcmp(direction,'z')
    problem.phi_u_fn=@(x,y) 0;
    problem.u_z_fn=@(x,y,z) u;
end

disp('Discretizing')
[A,b]=problem.discretize();

end

function [A,b]=generate_problem(dimension,N,Pe,e,Da,Pe_y_f,Pe_z_f)

if nargin==5
    Pe_y_f=1;
    Pe_z_f=1;
end

if dimension==3
    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input3D;

    %Fixed input
    problem.phi_w_fn=@(y,z) 1;
    problem.phi_e_fn=@(y,z) 0;
    problem.phi_s_fn=@(x,z) 0;
    problem.phi_n_fn=@(x,z) 1;
    problem.phi_u_fn=@(x,y) 0;
    problem.phi_d_fn=@(x,y) 1;
    problem.x_w=0;
    problem.x_e=1;
    problem.y_s=0;
    problem.y_n=1;
    problem.z_d=0;
    problem.z_u=1;

    %Variable input
    problem.N_x=N;
    problem.N_y=N;
    problem.N_z=N;


    %Work out the other parameters from the input
    dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
    u=Pe/dx;
    sl=Da*(1/dx^2);
    s=0.0;
    %sl=0.0;
    %s=Da * (u + e / dx) / dx;

    %Set the flux approximation based on the scheme used
    problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

    %Advection
    problem.u_x_fn=@(x,y,z) u/sqrt(3);
    problem.u_y_fn=@(x,y,z) Pe_y_f*u/sqrt(3);
    problem.u_z_fn=@(x,y,z) Pe_z_f*u/sqrt(3);
    %Diffusion
    problem.e_fn=@(x,y,z) e;
    %Constant source
    problem.s_fn=@(x,y,z) s;
    %Linear source
    problem.sl_fn=@(x,y,z) sl;
elseif dimension==2
    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input2D;

    %Fixed input
    problem.phi_w_fn=@(y) 1;
    problem.phi_e_fn=@(y) 0;
    problem.phi_s_fn=@(x) 0;
    problem.phi_n_fn=@(x) 1;
    problem.x_w=0;
    problem.x_e=1;
    problem.y_s=0;
    problem.y_n=1;

    %Variable input
    problem.N_x=N;
    problem.N_y=N;


    %Work out the other parameters from the input
    dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
    u=Pe/dx;
    sl=Da*(1/dx^2);
    s=0.0;
    %sl=0.0;
    %s=Da * (u + e / dx) / dx;

    %Set the flux approximation based on the scheme used
    problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

    %Advection
    problem.u_x_fn=@(x,y) u/sqrt(3);
    problem.u_y_fn=@(x,y) u/sqrt(3);
    %Diffusion
    problem.e_fn=@(x,y) e;
    %Constant source
    problem.s_fn=@(x,y) s;
    %Linear source
    problem.sl_fn=@(x,y) sl;
elseif dimension==1
    %Set up the problem for the ADR equation
    %div(u*phi-e*grad(phi))=s-sl*phi
    problem=Input1D;

    %Fixed input
    problem.phi_w= 1;
    problem.phi_e= 0;
    problem.x_w=0;
    problem.x_e=1;

    %Variable input
    problem.N_x=N;


    %Work out the other parameters from the input
    dx=(problem.x_e-problem.x_w)/(problem.N_x-1);
    u=Pe/dx;
    sl=Da*(1/dx^2);
    s=0.0;
    %sl=0.0;
    %s=Da * (u + e / dx) / dx;

    %Set the flux approximation based on the scheme used
    problem.flux_fn=@(u,e,dx) flux.HF(u,e,dx);

    %Advection
    problem.u_fn=@(x) u/sqrt(3);
    %Diffusion
    problem.e_fn=@(x) e;
    %Constant source
    problem.s_fn=@(x) s;
    %Linear source
    problem.sl_fn=@(x) sl;
else
    error('Dimension not supported')
end

disp('Discretizing')
[A,b]=problem.discretize();

end
